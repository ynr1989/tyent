<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="customStyle.css">

</head>

<body id="page-top">
<?php include("phpIncludes/header.php") ?>

	<!-- /navbar ends -->

	
	<section id="content ">

        <div class="container headMargin paddingBottom  fadeInUp animated">
			
            <div class="row mainProdMenu fadeInUp animated"> 
				<div class="heading-block center marginTop10px  fadeInUp animated">
					<h3 class="poppins letterSpacing2px fontWeight100px">Products</h3>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="card1 shadow" id="nmp">
						<img src="cImages/prod/NMP_thumb.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<h2 class="card1-title">NMP Series</h2>
							<p class="card1-text">Above Counter Water Ionizer Series</p>
						</div>
						
					</div>
				</div>
				<div class="col-md-3 col-sm-6" >
					<div class="card1 shadow" id="uce">
						<img src="cImages/prod/UCE_thumb.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<h2 class="card1-title">UCE Series</h2>
							<p class="card1-text">Under Counter Water Ionizer Series</p>
						</div>
						
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="card1 shadow" id="ESWG" data-loc="electric_water_generator">
						<img src="cImages/prod/eswg_thumb.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<h2 class="card1-title">Sterilized</h2>
							<p class="card1-text">Water Generator</p>
						</div>
						
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="card1 shadow" id="TMX" data-loc="tyentsoap">
						<img src="cImages/prod/TMX_thumb.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<h2 class="card1-title">TM-X </h2>
							<p class="card1-text">Beauty Soap</p>
						</div>
					</div>
				</div>		
				<div class="col-md-3 col-sm-6">
					<a href="product/h2Hybrid">
						<div class="card1 shadow" id="h2" data-loc="h2Hybrid">
							<img src="../images/combo_01.jpg" class="card1-img-top" alt="NMP Series">
							<div class="card1-body">
								<h2 class="card1-title">H2 Hybrid </h2>
								<p class="card1-text">Above Counter Water Ionizer Series</p>
							</div>
						</div>
					</a>
				</div>	
			</div>

			<div class="prodSecDiv">
				
			<div class="row  dNone prodSec fadeInUp animated"  id="nmpS"> 
				<div class="heading-block center">
					<h3 class="poppins letterSpacing2px fontWeight100px">NMP Series</h3>
				</div>
				<div class="col-md-3 col-sm-6" data-toggle="modal" data-target="#previewNMP5">
					<div class="card1 shadow" >
						<img src="cImages/prod/NMP_thumb.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<h2 class="card1-title">NMP 5 Plate</h2>
							<p class="card1-text">	<span>Price:</span>₹ 1,51,000</p>
							<p class="card1-text">	<span> Negative ORP:</span>-500 to -700</p>
							<p class="card1-text">	<span>Jog Dial:</span>No</p>
						</div>	
					</div>
				</div>
				<div class="col-md-3 col-sm-6" data-toggle="modal" data-target="#previewNMP7">
					<div class="card1 shadow">
						<img src="cImages/prod/NMP_thumb.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<h2 class="card1-title">NMP 7 Plate</h2>
							<p class="card1-text">	<span>Price:</span>₹ 1,80,000</p>
							<p class="card1-text">	<span> Negative ORP:</span>-600 to -800</p>
							<p class="card1-text">	<span>Jog Dial:</span>No</p>
						</div>	
					</div>
				</div>
				<div class="col-md-3 col-sm-6" data-toggle="modal" data-target="#previewNMP9">
					<div class="card1 shadow">
						<img src="cImages/prod/NMP_thumb1.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<h2 class="card1-title">NMP  9 Plate</h2>
							<p class="card1-text">	<span>Price:</span>₹ 2,10,000</p>
							<p class="card1-text">	<span> Negative ORP:</span>-600 to -1000</p>
							<p class="card1-text">	<span>Jog Dial:</span>Yes</p>
						</div>	
					</div>
				</div>
				<div class="col-md-3 col-sm-6" data-toggle="modal" data-target="#previewNMP11">
					<div class="card1 shadow" >
						<img src="cImages/prod/NMP_thumb1.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<h2 class="card1-title">NMP 11 Plate</h2>
							<p class="card1-text">	<span>Price:</span>₹ 2,40,000</p>
							<p class="card1-text">	<span> Negative ORP:</span>-600 to -1200</p>
							<p class="card1-text">	<span>Jog Dial:</span>Yes</p>
						</div>	
					</div>
				</div>
				<div class="col-md-3 col-sm-6 goBack" >
					<div class="card1 shadow" >
						<img src="cImages/previous.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<div class="center">Back to all products</div>
						</div>	
					</div>
				</div>				
			</div>
			
			<div class="row  dNone prodSec fadeInUp animated"  id="uceS"> 
				<div class="heading-block center">
					<h3 class="poppins letterSpacing2px fontWeight100px">UCE Series</h3>
				</div>
				<div class="col-md-3 col-sm-6" data-toggle="modal" data-target="#previewUCE9">
					<div class="card1 shadow" >
						<img src="cImages/prod/UCE_thumb.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<h2 class="card1-title">UCE 9 Plate</h2>
							<p class="card1-text">	<span>Price:</span>₹ 2,49,000</p>
							<p class="card1-text">	<span> Negative ORP:</span>-600 to -1000</p>
						</div>	
					</div>
				</div>
				<div class="col-md-3 col-sm-6" data-toggle="modal" data-target="#previewUCE11">
					<div class="card1 shadow">
						<img src="cImages/prod/UCE_thumb.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<h2 class="card1-title">UCE 11 Plate</h2>
							<p class="card1-text">	<span>Price:</span>₹ 2,70,000</p>
							<p class="card1-text">	<span> Negative ORP:</span>	-600 to -1200</p>
						</div>	
					</div>
				</div>
				<div class="col-md-3 col-sm-6 goBack">
					<div class="card1 shadow" >
						<img src="cImages/previous.png" class="card1-img-top" alt="NMP Series">
						<div class="card1-body">
							<div class="center">Back to all products</div>
						</div>	
					</div>
				</div>				
			</div>
			</div>
		</div>
		<div class="modalHolderDiv">
				
			<div class="modal fade bs-example-modal-lg" id="previewNMP5" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-body">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title poppins" id="myModalLabel">NMP 5 Plate</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body poppins">
								<div class="col-md-12">
									<div class="col-md-4 col-sm-4">
										<img src="cImages/prod/NMP_thumb.png" alt="Electrolyzed sterilized water generators" class="auto">
									</div>
									<div class="col-md-8 col-sm-8 modalTextData">
										<table class="table modalTable">
											<thead><tr><td>Model Name :</td><td> NMP-5</td></tr></thead>
											<tr><td>Total number of plates :</td><td>5</td></tr>
											<tr><td>pH Level :</td><td> 11.5 - 2.5<i>*</i></td></tr>
											<tr><td>ORP Level :</td><td>-500 to -700<i>*</i> </td></tr>
											<tr><td>Generates :  </td><td>3 Alkaline, 1 Neutral, 2 Acidic, Turbo</td></tr>
											<tr><td>Water outflow :</td><td>2 Liter - 2.5 Liter / min</td></tr>
											<tr><td>Power consumption :</td><td> Max 220W</td></tr>
											<tr><td>Display :</td><td>Custom LCD</td></tr>
											<tr><td>Jog Dial :</td><td>No</td></tr>
											<tr><td>Dimension :</td><td>300MM X 350MM X 135MM</td></tr>
											<tr><td>Price :</td><td>₹ 1,51,000</td></tr>
											<tr><td>Warranty :</td><td>3 years on ionizer & 15years on Plates</td></tr>
										</table>
									</div>
								</div>
								<div class="clearfix"></div>
								<p class="prodtnc"><span>*</span>Depends upon Input water quality water and input water pressure.</p><p class="nobottommargin textCenter">
									<div>
										<a href="/product/nmmp-5-plates-water-ionizer" class="btn btn-primary btn-lg btn-block">View detailed information</a>
									</div>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			
			<div class="modal fade bs-example-modal-lg" id="previewNMP7" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-body">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title poppins" id="myModalLabel">NMP 7 Plate</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body poppins">
								<div class="col-md-12">
									<div class="col-md-4 col-sm-4">
										<img src="cImages/prod/NMP_thumb.png" alt="Electrolyzed sterilized water generators" class="auto">
									</div>
									<div class="col-md-8 col-sm-8 modalTextData">
										<table class="table modalTable">
											<thead><tr><td>Model Name :</td><td> NMP-7</td></tr></thead>
											<tr><td>Total number of plates :</td><td>7</td></tr>
											<tr><td>pH Level :</td><td> 11.5 - 2.5<i>*</i></td></tr>
											<tr><td>ORP Level :</td><td>-600 to -800<i>*</i> </td></tr>
											<tr><td>Generates :  </td><td>3 Alkaline, 1 Neutral, 2 Acidic, Turbo</td></tr>
											<tr><td>Water outflow :</td><td>2 Liter - 2.5 Liter / min</td></tr>
											<tr><td>Power consumption :</td><td> Max 220W</td></tr>
											<tr><td>Display :</td><td>Custom LCD</td></tr>
											<tr><td>Jog Dial :</td><td>No</td></tr>
											<tr><td>Dimension(WHD) :</td><td>300MM X 350MM X 135MM</td></tr>
											<tr><td>Price :</td><td>₹ 1,80,000.</td></tr>
											<tr><td>Warranty :</td><td>3 years on ionizer & 15years on Plates</td></tr>
										</table>
									</div>
								</div>
								<div class="clearfix"></div>
								<p class="prodtnc"><span>*</span>Depends upon Input water quality water and input water pressure.</p><p class="nobottommargin textCenter">
									<div>
										<a href="/product/nmmp-7-plates-water-ionizer" class="btn btn-primary btn-lg btn-block">View detailed information</a>
									</div>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade bs-example-modal-lg" id="previewNMP9" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-body">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title poppins" id="myModalLabel">NMP 9 Plate</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body poppins">
								<div class="col-md-12">
									<div class="col-md-4 col-sm-4">
										<img src="cImages/prod/NMP_thumb1.png" alt="Electrolyzed sterilized water generators" class="auto">
									</div>
									<div class="col-md-8 col-sm-8 modalTextData">
										<table class="table modalTable">
											<thead><tr><td>Model Name :</td><td> NMP-9</td></tr></thead>
											<tr><td>Total number of plates :</td><td>9</td></tr>
											<tr><td>pH Level :</td><td> 11.5 - 2.5<i>*</i></td></tr>
											<tr><td>ORP Level :</td><td>-600 to -1000<i>*</i> </td></tr>
											<tr><td>Generates :  </td><td>3 Alkaline, 1 Neutral, 2 Acidic, Turbo</td></tr>
											<tr><td>Water outflow :</td><td>2 Liter - 2.5 Liter / min</td></tr>
											<tr><td>Power consumption :</td><td> Max 220W</td></tr>
											<tr><td>Display :</td><td>Custom LCD</td></tr>
											<tr><td>Jog Dial :</td><td>Yes</td></tr>
											<tr><td>Dimension(WHD) :</td><td>300MM X 350MM X 135MM</td></tr>
											<tr><td>Price :</td><td>₹ 2,10,000.</td></tr>
											<tr><td>Warranty :</td><td>3 years on ionizer & 15years on Plates</td></tr>
										</table>
									</div>
								</div>
								<div class="clearfix"></div>
								<p class="prodtnc"><span>*</span>Depends upon Input water quality water and input water pressure.</p><p class="nobottommargin textCenter">
									<div>
										<a href="/product/nmmp-9-plates-water-ionizer" class="btn btn-primary btn-lg btn-block">View detailed information</a>
									</div>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade bs-example-modal-lg" id="previewNMP11" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-body">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title poppins" id="myModalLabel">NMP 11 Plate</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body poppins">
								<div class="col-md-12">
									<div class="col-md-4 col-sm-4">
										<img src="cImages/prod/NMP_thumb1.png" alt="Electrolyzed sterilized water generators" class="auto">
									</div>
									<div class="col-md-8 col-sm-8 modalTextData">
										<table class="table modalTable">
											<thead><tr><td>Model Name :</td><td> NMP-11</td></tr></thead>
											<tr><td>Total number of plates :</td><td>11</td></tr>
											<tr><td>pH Level :</td><td> 11.5 - 2.5<i>*</i></td></tr>
											<tr><td>ORP Level :</td><td>-600 to -1200<i>*</i> </td></tr>
											<tr><td>Generates :  </td><td>3 Alkaline, 1 Neutral, 2 Acidic, Turbo</td></tr>
											<tr><td>Water outflow :</td><td>2 Liter - 2.5 Liter / min</td></tr>
											<tr><td>Power consumption :</td><td> Max 220W</td></tr>
											<tr><td>Display :</td><td>Custom LCD</td></tr>
											<tr><td>Jog Dial :</td><td>Yes</td></tr>
											<tr><td>Dimension(WHD) :</td><td>300MM X 350MM X 135MM</td></tr>
											<tr><td>Price :</td><td>₹ 2,40,000.</td></tr>
											<tr><td>Warranty :</td><td>3 years on ionizer & 15years on Plates</td></tr>
										</table>
									</div>
								</div>
								<div class="clearfix"></div>
								<p class="prodtnc"><span>*</span>Depends upon Input water quality water and input water pressure.</p><p class="nobottommargin textCenter">
									<div>
										<a href="/product/nmmp-11-plates-water-ionizer" class="btn btn-primary btn-lg btn-block">View detailed information</a>
									</div>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade bs-example-modal-lg" id="previewUCE9" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-body">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title poppins" id="myModalLabel">UCE 9 Plate</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body poppins">
								<div class="col-md-12">
									<div class="col-md-4 col-sm-4">
										<img src="cImages/prod/UCE_thumb.png" alt="Electrolyzed sterilized water generators" class="auto">
									</div>
									<div class="col-md-8 col-sm-8 modalTextData">
										<table class="table modalTable">
											<thead><tr><td>Model Name :</td><td> UCE-9</td></tr></thead>
											<tr><td>Total number of plates :</td><td>9</td></tr>
											<tr><td>pH Level :</td><td> 11.5 - 2.5<i>*</i></td></tr>
											<tr><td>ORP Level :</td><td>-600 to -1000<i>*</i> </td></tr>
											<tr><td>Generates :  </td><td>3 Alkaline, 1 Neutral, 2 Acidic, Turbo</td></tr>
											<tr><td>Water outflow :</td><td>2 Liter - 2.5 Liter / min</td></tr>
											<tr><td>Power consumption :</td><td> Max 220W</td></tr>
											<tr><td>Display :</td><td>Custom LCD</td></tr>
											<tr><td class="textCenter strongText" colspan="2">Dimension(WHD)</td></tr>
											<tr><td>Water Ionizer  :</td><td>360(W) x 132(D) x 355(H)</td></tr>
											<tr><td>Faucet  :</td><td>50(W) x 343(D)</td></tr>
											<tr><td>Price :</td><td>₹ 2,49,000.</td></tr>
											<tr><td>Warranty :</td><td>3 years on ionizer & 15years on Plates</td></tr>
										</table>
									</div>
								</div>
								<div class="clearfix"></div>
								<p class="prodtnc"><span>*</span>Depends upon Input water quality water and input water pressure.</p><p class="nobottommargin textCenter">
									<div>
										<a href="/product/tyent-uce-9-water-ionizer" class="btn btn-primary btn-lg btn-block">View detailed information</a>
									</div>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade bs-example-modal-lg" id="previewUCE11" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-body">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title poppins" id="myModalLabel">UCE 11 Plate</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body poppins">
								<div class="col-md-12">
									<div class="col-md-4 col-sm-4">
										<img src="cImages/prod/UCE_thumb.png" alt="Electrolyzed sterilized water generators" class="auto">
									</div>
									<div class="col-md-8 col-sm-8 modalTextData">
										<table class="table modalTable">
											<thead><tr><td>Model Name :</td><td> UCE-11</td></tr></thead>
											<tr><td>Total number of plates :</td><td>11</td></tr>
											<tr><td>pH Level :</td><td> 11.5 - 2.5<i>*</i></td></tr>
											<tr><td>ORP Level :</td><td>-600 to -1200<i>*</i> </td></tr>
											<tr><td>Generates :  </td><td>3 Alkaline, 1 Neutral, 2 Acidic, Turbo</td></tr>
											<tr><td>Water outflow :</td><td>2 Liter - 2.5 Liter / min</td></tr>
											<tr><td>Power consumption :</td><td> Max 220W</td></tr>
											<tr><td>Display :</td><td>Custom LCD</td></tr>
											<tr><td class="textCenter strongText" colspan="2">Dimension(WHD)</td></tr>
											<tr><td>Water Ionizer  :</td><td>360(W) x 132(D) x 355(H)</td></tr>
											<tr><td>Faucet  :</td><td>50(W) x 343(D)</td></tr>
											<tr><td>Price :</td><td>₹ 2,70,000.</td></tr>
											<tr><td>Warranty :</td><td>3 years on ionizer & 15years on Plates</td></tr>
										</table>
									</div>
								</div>
								<div class="clearfix"></div>
								<p class="prodtnc"><span>*</span>Depends upon Input water quality water and input water pressure.</p><p class="nobottommargin textCenter">
									<div>
										<a href="/product/tyent-uce-11-water-ionizer" class="btn btn-primary btn-lg btn-block">View detailed information</a>
									</div>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade bs-example-modal-lg" id="previewESWG" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-body">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title poppins" id="myModalLabel">Electrolyzed sterilizing Water Generator
								</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body poppins">
								<div class="col-md-12">
									<div class="col-md-4 col-sm-4">
										<img src="cImages/prod/eswg_thumb.png" alt="Electrolyzed sterilized water generators" class="auto">
									</div>
									<div class="col-md-8 col-sm-8 modalTextData">
									<div class="">
										
										<table class="table modalTable">
											<tbody><tr>
												<td >Product</td>
												<td>Sterilizing water generator</td>
											</tr>
											<tr>
												<td >Model</td>
												<td>TIE-N15WR</td>
											</tr>
											<tr>
												<td >Size(mm)</td>
												<td>545x225x470</td>
											</tr>
											<tr valign="top">
												<td >Purpose of use</td>
												<td>disinfection and sterilization of food,<br>utensils, and other apparatus</td>
											</tr>
											<tr valign="top">
												<td >Places to use</td>
												<td>School, military, hospital, hotel restaurant,<br>cafe</td>
											</tr>
											
										</tbody></table>
										
									</div>
									</div>
									<div class="col-md-12 modalTextData textCenter">
									<div class="box2">
											<ul style="font-weight: bold">
												<li>100% Natural ingredients</li>
												<li>99.9% Sterilization power</li>
												<li>Sterilization Ingredients of FDA GRAS Class
													<i style="font-style: normal">*GRAS(Generally Recognized As Safe, CITE: 21CFR178.1010)</i></li>
												<li>KFDA Approval sterilization ingredients for food</li>
												<li>Registered in China safety standard (GB28233)</li>
												<li>Novel <span style="color: #2393bb;">Coronavirus(COVID-19) disinfectant</span> suggested by WHO</li>
											</ul>
									</div>
									</div>
									
								</div>
								<div class="clearfix"></div>
								<p class="nobottommargin textCenter">
									<div>
										<a href="/product/electrolyzed-sterilizing-water-generator" class="btn btn-primary btn-lg btn-block">View detailed information</a>
									</div>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade bs-example-modal-lg" id="previewTMX" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-body">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title poppins" id="myModalLabel">Electrolyzed sterilizing Water Generator
								</h4>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body poppins">
								<div class="col-md-12">
									<div class="col-md-4 col-sm-4">
										<img src="cImages/tyentsoap/tm-x-beauty-soap.jpg" alt="Electrolyzed sterilized water generators" class="auto">
									</div>
									<div class="col-md-8 col-sm-8 modalTextData">
										<div class="text- pro-infor marginTop10px">

											<div class="pro-infor-title poppins">Tyent TM-X Beauty Soap</div>
											<p>Tyent TM-X Beauty Soap is made best ingredients which can maintain the delicate balance
												of oil and moisture that is required for healthy skin. This is made possible through a 600-hours 
												TM fermentation method, Resulting in a premium cleansing soap that is truly natural.</p>
					
											<table class="pro-inforTable">
					
												<colgroup>
													<col width="135">
													<col width="">
												</colgroup>
					
												<tbody>
													<tr>
														<td class="bold1">Weight</td>
														<td>80g</td>
													</tr>
					
													<tr>
														<td colspan="2" height="13"></td>
													</tr>
					
													<tr>
														<td colspan="2" height="13"></td>
													</tr>
													<tr>
														<td class="bold1">Price</td>
														<td>₹ 900</td>
													</tr>
					
					
												</tbody>
											</table>
					
										</div>
									</div>
								</div>
								<div class="clearfix"></div>
									<div>
										<a href="/product/tyentsoap" class="btn btn-primary btn-lg btn-block">View detailed information</a>
									</div>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		
		</div> 

		<?php include("phpIncludes/footer.php") ?>

	</section>
	<div id="remClick"></div>
	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="header/js/jquery.min.js"></script>
	<script>
		if($('.isMobile').css("display")=="block"){
			$('#productLi').click(function(e){
			  e.preventDefault();
			  window.location.href="/products";
		  });
		}
		 
		  
	</script>
	<script src="header/js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/functions.js"></script>

  
	<!-- Main Js -->
    <script src="header/js/main.js"></script>
	<script src="js/customScript.js"></script>
	<script>
		
		  $( ".goBack" ).click(function() {
			  $('html, body').animate({scrollTop: $("#"+$('#remClick').attr('loc')).offset().top - 110}, 'slow',"linear",function() {
				$('.prodSec').addClass('dNone');
  				});
		  });

		  $( ".card1" ).click(function() {
			if($(this).parents().hasClass('mainProdMenu')){
				$('.prodSec').addClass('dNone');
				$('#remClick').attr('loc',$(this).attr('id'))
			}
			var a = $(this).attr("id");
			if( $(this).attr("data-loc")==undefined){
				$('#'+a+'S').removeClass('dNone');
				$('html, body').animate({scrollTop: $('.prodSecDiv').offset().top - 110}, 'slow');
			}
			else{
				$('#preview'+a).modal('show');
			}
			
		  });

		 
	</script>

</body>

</html>