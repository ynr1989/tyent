<!DOCTYPE html>

<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php  include('seoTags.php');echo ${basename(__FILE__, '.php')};?>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link href="header/css/bootstrap.css" rel="stylesheet" >
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" >
	<link rel="stylesheet" href="style.css"  />
	<link rel="stylesheet" href="css/dark.css"  />
	<link rel="stylesheet" href="css/animate.css"  />
	<link rel="stylesheet" href="css/responsive.css"  />
	<link rel="stylesheet" href="css/font-icons.css"  />
	<link rel="stylesheet" href="demos/gym/gym.css"  />
	<link rel="stylesheet" href="css/magnific-popup.css"  />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet"  href="include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet"  href="include/rs-plugin/css/layers.css">
	<link rel="stylesheet"  href="include/rs-plugin/css/navigation.css">
	<link rel="stylesheet"  href="customStyle.css">

</head>

<body id="page-top">
<?php include("phpIncludes/header.php") ?>

<section id="slider" class="slider-element revslider-wrap slider-parallax clearfix headMargin">
		<div class="slider-parallax-inner ">
			<div id="rev_slider_579_1_wrapper" class="rev_slider_wrapper fullscreen-container"
				data-alias="default-slider" style="padding:0px;">
				<!-- START REVOLUTION SLIDER 5.1.4 fullscreen mode -->
				<div id="rev_slider_579_1" class="rev_slider fullscreenbanner" style="display:none;"
					data-version="5.1.4">
					<ul>
					
						<li data-transition="slideleft" data-slotamount="1"  data-masterspeed="1500" data-delay="5000"
							data-saveperformance="off">
							<!-- MAIN IMAGE -->
							
							<img class="displayDesktop" src="cImages/slider/design1a.jpg" alt="kenburns6" data-bgposition="center center"
								data-kenburns="off" data-duration="10000" data-ease="Linear.easeNone"	>
							
							<div class="rvSlide1" nextslideatend="true" alt="kenburns6" data-bgposition="left top"
								data-kenburns="off" data-duration="10000" data-ease="Linear.easeNone"></div>
								<div class="tp-caption   tp-resizeme"
									 id="slide-167-layer-1"
									 data-x="['center','center','center','center']"  data-hoffset="['0','0','30','0']"
									 data-y="['center','center','center','top']" data-voffset="['-50','-50','-50','30']"
									data-fontsize="['50','50','50','50']"
									data-lineheight="['50','50','60','60']"
									data-letterspacing="['1','1','1','1']"
									data-width="900"
									data-height="none"
									data-whitespace="normal"
									data-type="text"
									data-responsive_offset="on"
									data-textAlign="center"
									data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
									data-margintop="[0,0,0,0]"
									data-marginright="[0,0,0,0]"
									data-marginbottom="[30,31,26,26]"
									data-marginleft="[0,0,0,0]"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"

									style="z-index: 8; white-space: normal; font-weight: 100; letter-spacing: 5px; display: block;font-family: 'raleway',system-ui;">Tyent <span class="fontColorBlue">water</span> ionizer</div>

								<div class="tp-caption   tp-resizeme"
									 id="slide-167-layer-4"
									 data-x="['center','center','center','center']" data-hoffset="['200','200','180','0']"
									 data-y="['center','center','center','top']" data-voffset="['0','0','-20','100']"
									data-fontsize="['40','40','40','60']"
									data-width="750"
									data-height="none"
									data-whitespace="normal"

									data-type="text"
									data-responsive_offset="on"

									data-frames='[{"delay":1500,"speed":300,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

									data-margintop="[0,0,40,40]"
									data-marginright="[0,0,0,0]"
									data-marginbottom="[40,40,40,40]"
									data-marginleft="[0,0,0,0]"
									data-textAlign="center"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"

									style="z-index: 14; min-width: 100%px; max-width: 100%px; white-space: normal; font-size: 15px; line-height: 30px; font-weight: 700; letter-spacing: 3px; display: block;font-family:'raleway',sans-serif;color:#039dda">Doctors</div>

								<div class="tp-caption   tp-resizeme"
									 id="slide-167-layer-4"
									 data-x="['center','center','center','center']" data-hoffset="['40','40','40','0']"
									 data-y="['center','center','center','top']" data-voffset="['50','50','20','190']"
									 data-fontsize="['130','130','80','100']"
									data-width="750"
									data-height="none"
									data-whitespace="normal"

									data-type="text"
									data-responsive_offset="on"

									data-frames='[{"delay":2000,"speed":300,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'

									data-margintop="[0,0,40,40]"
									data-marginright="[0,0,0,0]"
									data-marginbottom="[40,40,40,40]"
									data-marginleft="[0,0,0,0]"
									data-textAlign="center"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"

									style="z-index: 14; min-width: 100%px; max-width: 100%px; white-space: normal; font-size: 15px; line-height: 30px; font-weight: 900; color: #afcb20; display: block;font-family:'raleway',sans-serif;">Choice</div>

								
						</li>
						
						<li data-transition="slideleft" data-slotamount="1" data-masterspeed="1500" data-delay="5000"
							data-saveperformance="off">
							<!-- MAIN IMAGE -->
							<img class="displayDesktop" src="cImages/slider/design2.jpg" alt="kenburns6" data-bgposition="center center"
								data-kenburns="off" data-duration="10000" data-ease="Linear.easeNone" data-visibility="['on', 'on', 'on', 'off']">
							
								<!-- LAYER NR. 1 -->
								<div class="tp-caption  "
									 id="slide-5-layer-1"
									 data-x="['right','right','right','center']" data-hoffset="['20','20','20','0']"
									 data-y="['center','center','center','top']" data-voffset="['-90','-90','-90','20']"
									data-fontsize="['45','45','35','45']"
									data-lineheight="['50','45','35','40']"
									data-width="400"
									data-height="none"
									data-whitespace="normal"
									data-transform_idle="o:1;"
									data-textAlign="center"
									 data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeOut;"
									 data-transform_out="opacity:0;s:300;"
									data-start="500"
									data-splitin="none"
									data-splitout="none"
									data-frames='[{"delay":500,"speed":300,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
									style="z-index: 5; min-width: 600px; max-width: 600px; white-space: normal; font-size: 60px; line-height: 60px; font-weight: 200;font-family:'poppins';">Discover the true </div>
								
								<div class="tp-caption  "
									id="slide-5-layer-1"
									data-x="['right','right','right','center']"  data-hoffset="['30','30','40','0']"
									data-y="['center','center','center','top']" data-voffset="['-30','-30','-30','100']"
								   data-fontsize="['45','45','35','45']"
								   data-lineheight="['50','45','35','40']"
								   data-width="500"
								   data-height="none"
								   data-whitespace="normal"
								   data-transform_idle="o:1;"
								   data-textAlign="['right','right','right','center']"
								   data-frames='[{"delay":1500,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'

									data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeOut;"
									data-transform_out="opacity:0;s:300;"
								   data-start="500"
								   data-splitin="none"
								   data-splitout="none"
								   style="z-index: 5; color:#0088c6;white-space: normal; font-size: 60px; line-height: 60px; font-weight: 700; ;font-family:'raleway',sans-serif;">Health & Happiness </div>

							<div class="tp-caption  "
								   id="slide-5-layer-1"
								   data-x="['right','right','right','center']" data-hoffset="['20','20','20','0']"
								   data-y="['center','center','center','top']" data-voffset="['50','50','40','190']"
								  data-fontsize="['45','45','35','45']"
								  data-lineheight="['50','45','35','70']"
								  data-width="400"
								  data-height="none"
								  data-whitespace="normal"
								  data-transform_idle="o:1;"
								  data-textAlign="center"
								   data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeOut;"
								   data-transform_out="opacity:0;s:300;"
								  data-start="500"
								  data-splitin="none"
								  data-splitout="none"
								  data-frames='[{"delay":2000,"speed":300,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
								  style="z-index: 5; min-width: 600px; max-width: 600px; white-space: normal; font-size: 60px; line-height: 60px; font-weight: 200;font-family:'poppins';">with Tyent <br> Water Ionizers </div>
							  
							   
						</li>
						<li data-transition="slideleft" data-slotamount="1" data-masterspeed="1500" data-delay="5000"
							data-saveperformance="off">
							<!-- MAIN IMAGE -->
						
							<img class="displayDesktop" src="cImages/slider/design3.jpg" alt="kenburns6" data-bgposition="center center"
								data-kenburns="off" data-duration="10000" data-ease="Linear.easeNone">
							
							<div class="tp-caption  "
								   id="slide-5-layer-1"
								   data-x="['right','right','right','center']" data-hoffset="['20','20','20','0']"
								   data-y="['bottom','bottom','bottom','top']" data-voffset="['150','150','130','20']"
								  data-fontsize="['45','45','25','40']"
								  data-lineheight="['50','45','35','40']"
								  data-width="['800','800','800','550']"
								  data-height="none"
								  data-whitespace="normal"
								  data-transform_idle="o:1;"
								  data-textAlign="['right','right','right','center']"
								  data-letterspacing="['10','10','3']"
								   data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeOut;"
								   data-transform_out="opacity:0;s:300;"
								  data-start="500"
								  data-splitin="none"
								  data-splitout="none"
								  data-frames='[{"delay":1000,"speed":1500,"frame":"0","from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
								  style="z-index: 5; min-width: 600px; max-width: 600px; white-space: normal; line-height: 60px; font-weight: 100;font-family: 'raleway',system-ui;color:#05a6da;">Water ionizer with</div>
							  
							<div class="tp-caption  "
								  id="slide-5-layer-1"
								  data-x="['right','right','right','center']" data-hoffset="['20','20','20','0']"
								  data-y="['bottom','bottom','bottom','top']" data-voffset="['90','90','100','80']"
								 data-fontsize="['60','45','30','40']"
								 data-lineheight="['50','45','35','40']"
								 data-width="['800','800','800','550']"
								 data-height="none"
								 data-whitespace="normal"
								 data-transform_idle="o:1;"
								 data-textAlign="['right','right','right','center']"
								  data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeOut;"
								  data-transform_out="opacity:0;s:300;"
								 data-start="500"
								 data-splitin="none"
								 data-splitout="none"
								 data-frames='[{"delay":2000,"speed":300,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
								 style="z-index: 5; min-width: 600px; max-width: 600px; white-space: normal; line-height: 60px; font-weight: 500;font-family: 'raleway',system-ui;color:#05a6da;">Advanced technology</div>
							 
							<div class="tp-caption  "
								 id="slide-5-layer-1"
								 data-x="['right','right','right','center']" data-hoffset="['20','20','20','0']"
								 data-y="['bottom','bottom','bottom','top']" data-voffset="['30','30','60','120']"
								data-fontsize="['25','25','20','15']"
								data-width="['800','800','800','550']"
								data-letterspacing="['10','10','3']"
								data-height="none"
								data-whitespace="normal"
								data-transform_idle="o:1;"
								data-textAlign="['right','right','right','center']"
								 data-transform_in="y:50px;opacity:0;s:1500;e:Power4.easeOut;"
								 data-transform_out="opacity:0;s:300;"
								data-start="500"
								data-splitin="none"
								data-splitout="none"
								data-frames='[{"delay":2000,"speed":2000,"frame":"0","from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
								style="z-index: 5; min-width: 600px; max-width: 600px; white-space: normal;  line-height: 60px; font-weight: 100;font-family: 'raleway',system-ui;color:#fff;">Next Generation Technology</div>
							
						</li>
						<li data-transition="slideleft" data-slotamount="1" data-masterspeed="1500" data-delay="5000"
							data-saveperformance="off">
							<!-- MAIN IMAGE -->
							<img class="displayDesktop" src="cImages/slider/design4.jpg" alt="kenburns6" data-bgposition="center center"
								data-kenburns="off" data-duration="10000" data-ease="Linear.easeNone" data-visibility="['on', 'on', 'on', 'off']">
								<div class="tp-caption   tp-resizeme"
									 id="slide-167-layer-1"
									 data-x="['center','center','right','center']"  data-hoffset="['100','100','0','0']"
									 data-y="['center','center','center','top']" data-voffset="['-100','-100','-100','20']"
									data-fontsize="['80','70','60','50']"
									data-lineheight="['50','50','60','60']"
									data-letterspacing="['1','1','1','1']"
									data-width="['600','600','400','400']"
									data-height="none"
									data-whitespace="normal"
									data-type="text"
									data-responsive_offset="on"
									data-textAlign="['left','left','left','center']"
									data-frames='[{"delay":1000,"speed":300,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
									data-margintop="[0,0,0,0]"
									data-marginright="[0,0,0,0]"
									data-marginbottom="[30,31,26,26]"
									data-marginleft="[0,0,0,0]"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"

									style="z-index: 8; white-space: normal; color:#f07d1d;font-weight: 700; letter-spacing: 5px; display: block;font-family: 'raleway',sans-serif;">Tyent water</div>

							<div class="tp-caption   tp-resizeme"
									 id="slide-167-layer-1"
									 data-x="['center','center','right','center']"  data-hoffset="['100','100','0','0']"
									 data-y="['center','center','center','top']" data-voffset="['-30','-40','-40','90']"
									data-fontsize="['70','50','50','50']"
									data-lineheight="['50','50','60','60']"
									data-letterspacing="['1','1','1','1']"
									data-width="['600','600','400','400']"
									data-height="none"
									data-whitespace="normal"
									data-type="text"
									data-responsive_offset="on"
									data-textAlign="['left','left','left','center']"
									data-frames='[{"delay":1500,"speed":300,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
									data-margintop="[0,0,0,0]"
									data-marginright="[0,0,0,0]"
									data-marginbottom="[30,31,26,26]"
									data-marginleft="[0,0,0,0]"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"

									style="z-index: 8; white-space: normal; color:#18a9d6;font-weight: 100; letter-spacing: 5px; display: block;font-family: 'raleway',system-ui;">Improves energy</div>
									<div class="tp-caption   tp-resizeme"
									 id="slide-167-layer-1"
									 data-x="['center','center','right','center']"  data-hoffset="['100','100','0','0']"
									 data-y="['center','center','center','top']" data-voffset="['50','20','20','160']"
									data-fontsize="['70','50','50','50']"
									data-lineheight="['50','50','60','60']"
									data-letterspacing="['1','1','1','1']"
									data-width="['600','600','400','400']"
									data-height="none"
									data-whitespace="normal"
									data-type="text"
									data-responsive_offset="on"
									data-textAlign="['left','left','left','center']"
									data-frames='[{"delay":2000,"speed":300,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
									data-margintop="[0,0,0,0]"
									data-marginright="[0,0,0,0]"
									data-marginbottom="[30,31,26,26]"
									data-marginleft="[0,0,0,0]"
									data-paddingtop="[0,0,0,0]"
									data-paddingright="[0,0,0,0]"
									data-paddingbottom="[0,0,0,0]"
									data-paddingleft="[0,0,0,0]"

									style="z-index: 8; white-space: normal; color:#18a9d6;font-weight: 100; letter-spacing: 5px; display: block;font-family: 'raleway',system-ui;">& Metabolism</div>
						</li>
						<li data-transition="slideleft" data-slotamount="1" data-masterspeed="1500" data-delay="5000"
								data-saveperformance="off">
								<!-- MAIN IMAGE -->
								<img class="displayDesktop" src="cImages/slider/design5.jpg" alt="kenburns6" data-bgposition="center center"
									data-kenburns="off" data-duration="10000" data-ease="Linear.easeNone" data-visibility="['on', 'on', 'on', 'off']">
									<div class="tp-caption   tp-resizeme"
										id="slide-167-layer-1"
										data-x="['left','left','left','center']"  data-hoffset="['30','30','30','0']"
										data-y="['center','center','center','top']" data-voffset="['-100','-100','-100','20']"
										data-fontsize="['50','70','45','40']"
										data-lineheight="['50','50','60','60']"
										data-letterspacing="['1','1','1','1']"
										data-width="600"
										data-height="none"
										data-whitespace="normal"
										data-type="text"
										data-responsive_offset="on"
										data-textAlign="['left','left','left','center']"
										data-frames='[{"delay":1000,"speed":300,"frame":"0","from":"y:top;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
										data-margintop="[0,0,0,0]"
										data-marginright="[0,0,0,0]"
										data-marginbottom="[30,31,26,26]"
										data-marginleft="[0,0,0,0]"
										data-paddingtop="[0,0,0,0]"
										data-paddingright="[0,0,0,0]"
										data-paddingbottom="[0,0,0,0]"
										data-paddingleft="[0,0,0,0]"

										style="z-index: 8; white-space: normal; color:#0f4c75;font-weight: 700; letter-spacing: 5px; display: block;font-family: 'raleway',sans-serif;">Drinking Tyent water</div>

								<div class="tp-caption   tp-resizeme"
										id="slide-167-layer-1"
										data-x="['left','left','left','center']"  data-hoffset="['30','30','30','0']"
										data-y="['center','center','center','top']" data-voffset="['-50','-40','-40','80']"
										data-fontsize="['50','50','40','40']"
										data-lineheight="['50','50','60','60']"
										data-letterspacing="['1','1','1','1']"
										data-width="600"
										data-height="none"
										data-whitespace="normal"
										data-type="text"
										data-responsive_offset="on"
										data-textAlign="['left','left','left','center']"
										data-frames='[{"delay":1500,"speed":300,"frame":"0","from":"x:right;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
										data-margintop="[0,0,0,0]"
										data-marginright="[0,0,0,0]"
										data-marginbottom="[30,31,26,26]"
										data-marginleft="[0,0,0,0]"
										data-paddingtop="[0,0,0,0]"
										data-paddingright="[0,0,0,0]"
										data-paddingbottom="[0,0,0,0]"
										data-paddingleft="[0,0,0,0]"

										style="z-index: 8; white-space: normal; color:#18a9d6;font-weight: 100; letter-spacing: 5px; display: block;font-family: 'raleway',system-ui;">can improve your</div>
									<div class="tp-caption   tp-resizeme"
										id="slide-167-layer-1"
										data-x="['left','left','left','center']"  data-hoffset="['30','30','30','0']"
										data-y="['center','center','center','top']" data-voffset="['10','20','20','140']"
										data-fontsize="['50','50','40','40']"
										data-lineheight="['50','50','60','60']"
										data-letterspacing="['1','1','1','1']"
										data-width="600"
										data-height="none"
										data-whitespace="normal"
										data-type="text"
										data-responsive_offset="on"
										data-textAlign="['left','left','left','center']"
										data-frames='[{"delay":2000,"speed":300,"frame":"0","from":"x:left;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
										data-margintop="[0,0,0,0]"
										data-marginright="[0,0,0,0]"
										data-marginbottom="[30,31,26,26]"
										data-marginleft="[0,0,0,0]"
										data-paddingtop="[0,0,0,0]"
										data-paddingright="[0,0,0,0]"
										data-paddingbottom="[0,0,0,0]"
										data-paddingleft="[0,0,0,0]"

										style="z-index: 8; white-space: normal; color:#18a9d6;font-weight: 100; letter-spacing: 5px; display: block;font-family: 'raleway',system-ui;">health in many ways</div>
						</li>
						<!-- SLIDE  -->
					
					</ul>
					
        <!-- Example Progress Bar, with a height and background color added to its style attribute -->
        <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: rgba(0, 0, 0, 0.25);"></div>
 
				</div>
			</div><!-- END REVOLUTION SLIDER -->
		</div>
	</section>
	
	<section id="content">
		<div class="container clearfix paddingTopBottom">

			<div class="divcenter center clearfix homeHead" style="max-width: 900px;">
				<h1 class="poppins uppercase homeHeading">Improving <span>health</span> and preserving
					<span>nature</span></h1>
				<h2 class="poppins "><span>Medilight healthcare</span> collaborated with <span> TAEYOUNG E&T Co. Ltd.</span> a parent company of Tyent Water Ionizers and successfully launched world's No.1 water Ionizers brand Tyent in India. Tyent is a Korean company with Japanese technology.</h2>
			</div>
			<div class="row indexAbout">
				<div class="col-md-6 ">
					<h3 class="poppins lineHeight1 fontColorBlue">Worlds most trusted:</h3>
					<p class="poppins lineHeight1 fontSize15px">
						Tyent is a leading water ionizer brand in USA, Canada, Europe, Australia and in 26 more countries across the globe.  People all around the world keep trust in this brand to keep up their health and for their loving family members also.
					</p>
				</div>
				<div class="col-md-6">
					<h3 class="poppins lineHeight1 fontColorBlue">Patented & updated technology:</h3>
					<p class="poppins lineHeight1 fontSize15px">
						World's only water ionizer to have the patented Solid hybrid mesh plate (Solid plate strength + Mesh plate more participating surface area in electrolysis with both advantages) & patented SMPS PLUS for more amount of anti-oxidants.
					</p>
				</div>
			</div>
		</div>

		<div class="clear"></div>
		<div class="banner viewport-section in-viewport ">
			<div class="paddingTopBottom " id="bg-stretch1598770616023928">
				<div class="container ">
					<div class="dark textCenter">
						<h3 class="poppins letterSpacing2px fontWeight100px">What is Kangen Water?</h3>
						<p class="bannerP">
							Kangen is a Water Ionizer which is manufactured by Enagic & Co. Which is nothing special than other water ionizers except that it is little popular because of its “Referal Marketing”(similar to MLM) business model. The Enagic company’s Kangen machine is sold at “Double the price” compared to other water ionizers in the market as nearly 40%-45% of the machine cost will be distributed as commission to almost 30 middle men through their MLM structure on each sale. The biggest set back in considering a kangen water machine for domestic use in India is lack of integrated after sales customer support or door step service in major cities across the country.
						</p>
						<h3 class="poppins letterSpacing2px fontWeight100px">Why Tyent?</h3>

						<p class="bannerP">
						At Medilight healthcare, We believe in Traditional marketing method to ensure  product delivery occurs from the Manufacturing unit to the Customer without middlemen. Tyent water ionizers are designed to provide pure, healthy hydration for the modern family with advanced Japanese technology where the Ionization chamber has been manufactured by Permelec Company, Japan.  Tyent water ionizers made available at an affordable cost with more transparency to the customers. 
						</p>
						<p class="bannerP">
						Do u really want to know how Tyent is more superior and more advanced 
						</p>
						<a href="why_tyent" class="button button-rounded button-reveal button-large button-white button-light tright"><i class="icon-line-arrow-right"></i><span>Click Here</span></a>
					</div>
				</div>
			</div>
		</div>
		<div class="container paddingTopBottom">
			<div class="heading-block center ">
				<h3 class="poppins letterSpacing2px fontWeight100px">Comparison</h3>
			</div>
			<div class="textCenter">
				<img src="cImages/Differences.jpg">
			</div>
		</div>
		<a href="technology" class="button button-full button-purple center tright header-stick">
			<div class="container clearfix">
				Know more about the technology &amp; Options. <strong>Check Out</strong> <i class="icon-caret-right" style="top:4px;"></i>
			</div>
		</a>
		<div class="row dark align-items-stretch clearfix">
					<div class="col-sm-6 homeLeftVideo">
						<div class="d-flex align-items-start flex-column compVideo" style="height: 400px;">
							<div class="mb-auto heading-block nobottomborder">
								<h3>Tyent  Vs Kangen Water Ionizer Comparison</h3>
								<span></span>
							</div>
							<p class="t300 mb-2">Want to see how the award-winning Tyent Water Ionizers are superior than Enagic Kangen water Ionizers. Check the below video.</p>
							<a class="button button-border-icon" href="https://www.youtube.com/watch?v=JAHUlOASNrI" data-lightbox="iframe"><i class="icon-play"></i><span>Play Video</span></a>
						</div>
					</div>
					<div class="col-sm-6 homeRightVideo">
						<div class="d-flex align-items-start flex-column compVideo" style="height: 400px;">
							<div class="mb-auto heading-block nobottomborder">
								<h3>TYENT WATER IONIZER CELL LEADS THE WATER IONIZER INDUSTRY</h3>
							</div>
							<p class="t300 mb-2">Our Tyent ionizer plates with advanced Japanese technology were designed for maximum surface area for Greater electrolysis for more health benefits with more anti-oxidant levels(-ORP). Check the below video.</p>
							<a class="button button-border-icon" href="https://www.youtube.com/watch?v=xC8kSojrPkA" data-lightbox="iframe"><i class="icon-play"></i><span>Play Video</span></a>
						</div>
					</div>
				</div>
		
		<div class="clear"></div>
		<div class="paddingTopBottom">
			<div class="heading-block center nomargin">
				<h3>Everyone talking about Tyent</h3>
			</div>
			<div id="oc-clients" class="owl-carousel image-carousel carousel-widget" data-autoplay="2000"
				data-loop="true" data-margin="100" data-nav="false" data-pagi="false" data-items-xs="2"
				data-items-sm="3" data-items-md="4" data-items-lg="5" data-items-xl="6">

				<div class="oc-item"><img src="cImages/clients/1.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/2.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/3.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/4.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/5.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/6.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/7.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/8.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/9.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/10.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/11.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/12.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/13.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/14.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/15.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/16.png" alt="Clients"></div>
				<div class="oc-item"><img src="cImages/clients/amazon.jpg" alt="Amazon"></div>
				<div class="oc-item"><img src="cImages/clients/aom.jpg" alt="AOM"></div>
				<div class="oc-item"><img src="cImages/clients/bbb.jpg" alt="AOM"></div>
				<div class="oc-item"><img src="cImages/clients/bornrich.jpg" alt="Born Rich"></div>
				<div class="oc-item"><img src="cImages/clients/cnet.jpg" alt="c|net"></div>
				<div class="oc-item"><img src="cImages/clients/cool-things.jpg" alt="Cool Things"></div>
				<div class="oc-item"><img src="cImages/clients/discovery.jpg" alt="Discovery Channel"></div>
				<div class="oc-item"><img src="cImages/clients/dvice.jpg" alt="DVICE"></div>
				<div class="oc-item"><img src="cImages/clients/electronic-house.jpg" alt="Electronic House"></div>
				<div class="oc-item"><img src="cImages/clients/e-network.jpg" alt="E Network"></div>
				<div class="oc-item"><img src="cImages/clients/facebook.jpg" alt="E Network"></div>
				<div class="oc-item"><img src="cImages/clients/geeky-gadgets.jpg" alt="Geeky Gadgets"></div>
				<div class="oc-item"><img src="cImages/clients/gizmodo.jpg" alt="Gizmodo"></div>
				<div class="oc-item"><img src="cImages/clients/google.jpg" alt="Google 5 stars"></div>
				<div class="oc-item"><img src="cImages/clients/health-news-digest.jpg" alt="Health News Digest"></div>
				<div class="oc-item"><img src="cImages/clients/home-DIT.jpg" alt="home dit"></div>
				<div class="oc-item"><img src="cImages/clients/luxury-launches.jpg" alt="Luxury launches"></div>
				<div class="oc-item"><img src="cImages/clients/oh-gizmo.jpg" alt="oh gizmo!"></div>
				<div class="oc-item"><img src="cImages/clients/pc-world.jpg" alt="PCWorld"></div>
				<div class="oc-item"><img src="cImages/clients/reseller-ratings.jpg" alt="PCWorld"></div>
				<div class="oc-item"><img src="cImages/clients/sand-trap.jpg" alt="the Sand Trap"></div>
				<div class="oc-item"><img src="cImages/clients/Sky-Mall.jpg" alt="Sky Mall"></div>
				<div class="oc-item"><img src="cImages/clients/splash-magazine.jpg" alt="Splash magazine"></div>
				<div class="oc-item"><img src="cImages/clients/springwise.jpg" alt="Spring Wise"></div>
				<div class="oc-item"><img src="cImages/clients/star-pulse.jpg" alt="Star Pulse"></div>
				<div class="oc-item"><img src="cImages/clients/tech-investor-news.jpg" alt="Tech Investor News"></div>
				<div class="oc-item"><img src="cImages/clients/traditional-home.jpg" alt="Traditional Home Magazine"></div>
			</div>
		</div>
		<div class="clear"></div>
		<div class="banner viewport-section in-viewport ">
			<div class="bg-stretch section2BG">
				<div class="content-holder">
					<div class="frame">
					</div>
				</div>
			</div>
		</div>
		<div class="v1-certi-old paddingTop">
			<div class="container">
				<div class="row ourCertificate col">
					<div class="col-md-5 textCenter four columns alpha certificateDetail textCenter"> <img
							src="cImages/tick.png" alt="Checkmark">
						<h2 class="lineHeight1">Our Certifications<br> <small>and</small><br> Accreditation's</h2>
						<p>Tyent Water Ionizers are tested to the highest standards</p>
						<a href="certifications" class="button button-large button-rounded">View All</a>
					</div>
					<div class="col-md-7 eleven columns omega offset-by-one">
						<ul class="certificationsUL clients-grid grid-6 nobottommargin clearfix ">
							<li><img src="cImages/certifications/1.png" alt="Clients"></li>
							<li><img src="cImages/certifications/2.png" alt="Clients"></li>
							<li><img src="cImages/certifications/3.png" alt="Clients"></li>
							<div class="clear"></div>
							<li><img src="cImages/certifications/4.png" alt="Clients"></li>
							<li><img src="cImages/certifications/5.png" alt="Clients"></li>
							<li><img src="cImages/certifications/PEL.jpg" alt="Clients"></li>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="clear"></div>

		<?php include('form.php')?>
		<!-- Location Section
			============================================= -->
		<div class="paddingTopBottom nobg">
			<div class="container clearfix">
				<div class="heading-block center mt-0 divcenter nobottomborder clearfix" style="max-width: 700px">
					<h2>We are present around the globe.</h2>
				</div>
				<div class="row">
					<div class=" ">
						<div class="heading-block mt-md-3 mt-0">
							<h3 class="ftSize18 nott ls0 poppins">Tyent is present in 32 countries around the globe.</h3>
						</div>
						<div class="row">
								<div class="col-md-3 col-xs-6">
									<ul class="iconlist ml-0">
										<li><img src="demos/hosting/images/flags/us.png" alt="">USA</li>
										<li><img src="demos/hosting/images/flags/in.png" alt="">India</li>
										<li><img src="demos/hosting/images/flags/au.png" alt="">Australia</li>
										<li><img src="demos/hosting/images/flags/ca.png" alt="">Canada</li>
										<li><img src="cImages/002-south-korea.png" alt="">South Korea</li>
										<li><img src="demos/hosting/images/flags/sa.png" alt="">South Africa</li>
										<li><img src="cImages/007-spain.png" alt="">Spain</li>
										<li><img src="cImages/006-poland.png" alt="">Poland</li>
										</ul>
								</div>
								<div class="col-md-3 col-xs-6">
									<ul class="iconlist ml-0">
										<li><img src="cImages/003-malaysia.png" alt="">Malaysia</li>
										<li><img src="demos/hosting/images/flags/uk.png" alt="">United Kingdom</li>
										<li><img src="demos/hosting/images/flags/ja.png" alt="">Japan</li>
										<li><img src="cImages/004-united-arab-emirates.png" alt="">UAE</li>
										<li><img src="demos/hosting/images/flags/br.png" alt="">Brazil</li>
										<li><img src="cImages/008-austria.png" alt="">Austria</li>
										<li><img src="cImages/009-kuwait.png" alt="">Kuwait</li>
										<li><img src="cImages/iceland.png" alt="">Iceland</li>
									</ul>
								</div>
								<div class="col-md-3 col-xs-6">
									<ul class="iconlist ml-0">
										<li><img src="cImages/005-germany.png" alt="">Germany</li>
										<li><img src="cImages/006-switzerland.png" alt="">Switzerland</li>
										<li><img src="cImages/007-sweden.png" alt="">Sweden</li>
										<li><img src="cImages/008-france.png" alt="">France</li>
										<li><img src="cImages/009-italy.png" alt="">Italy</li>
										<li><img src="cImages/010-norway.png" alt="">Norway</li>
										<li><img src="cImages/003-mexico.png" alt="">Mexico</li>
									</ul>
								</div>
								<div class="col-md-3 col-xs-6">
									<ul class="iconlist ml-0">
										<li><img src="cImages/010-indonesia.png" alt="">Indonesia</li>
										<li><img src="cImages/001-taiwan.png" alt="">Taiwan</li>
										<li><img src="demos/hosting/images/flags/si.png" alt="">Singapore<li><img src="cImages/010-indonesia.png" alt="">Portugal</li>
										<li><img src="cImages/002-thailand.png" alt="">Thailand</li>
										<li><img src="cImages/004-belgium.png" alt="">Belgium</li>
										<li><img src="cImages/005-new-zealand.png" alt="">New Zealand</li>
									</ul>
								</div>							
						</div>
					</div>
				</div>
			</div>
		</div>


	</section>
<?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="header/js/jquery.min.js"></script>
	<script src="header/js/bootstrap.min.js"></script>

	<script src="js/plugins.js"></script>
	<script src="js/functions.js"></script>

	<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
	<script src="include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>	
	<script>
	
		var tpj = jQuery;

		var revapi202;
		tpj(document).ready(function() {
			if (tpj("#rev_slider_579_1").revolution == undefined) {
				revslider_showDoubleJqueryError("#rev_slider_579_1");
			} else {
				var heightLarge = Math.min($( window ).height() - (20*$( window ).height()/100),'600');
				var heightMedium =360;
				var heightMobile = 730;
				revapi202 = tpj("#rev_slider_579_1").show().revolution({
					sliderType: "standard",
					jsFileLocation: "include/rs-plugin/js/",
					dottedOverlay: "none",
					delay: 9000,
					responsiveLevels: [1140, 1024, 778, 480],
					visibilityLevels: [1140, 1024, 778, 480],
					gridwidth: [1140, 1024, 778, 480],
					gridheight: [heightLarge,heightMedium,heightMedium,heightMobile],
					lazyType: "none",
					shadow: 0,
					spinner: "off",
					shuffle: "off",
					autoHeight: "off",
					fullScreenAutoWidth: "off",
					fullScreenAlignForce: "off",
					fullScreenOffsetContainer: "",
					fullScreenOffset: "",
					hideThumbsOnMobile: "off",
					hideSliderAtLimit: 0,
					hideCaptionAtLimit: 0,
					hideAllCaptionAtLilmit: 0,
					debugMode: false,
					fallbacks: {
						simplifyAll:"off",
						disableFocusListener:false,
					},
					parallax: {
						type:"mouse",
						origo:"slidercenter",
						speed:300,
						levels:[2,4,6,8,10,12,14,16,18,20,22,24,49,50,51,55],
					},
					navigation: {
								bullets: {
									enable: true,
									style: 'hermes',
									h_align: 'center',
									v_align: 'bottom',
									h_offset: 0,
									v_offset: 20,
									space: 5,
								},
								keyboardNavigation: "off",
								keyboard_direction: "horizontal",
								mouseScrollNavigation: "off",
								onHoverStop: "on",
								touch: {
									touchenabled: "on",
									swipe_threshold: 75,
									swipe_min_touches: 1,
									swipe_direction: "horizontal",
									drag_block_vertical: false
								},
								arrows: {
									enable: true,
									style: 'uranus',
								}
							},
			});
		revapi202.bind("revolution.slide.onloaded",function (e) {
			setTimeout( function(){ SEMICOLON.slider.sliderParallaxDimensions(); }, 200 );
			function isMobile(){
            if($('.isMobile').css('display')=="none") return false;
            else return true;
          }
		if(isMobile()) {
			console.log("in");
			$( ".tp-bgimg" ).each(function( index ) {
				var newImg = $(this).css('background-image').slice(0,-6)+"Mob"+$(this).css('background-image').slice(-6);
				var newImg1 = $(this).css('background-image').slice(5,-6)+"Mob"+$(this).css('background-image').slice(-6,-2);
				$(this).css('background-image',newImg);
				$(this).attr('src',newImg1);
			});
		}
		});

		revapi202.bind("revolution.slide.onchange",function (e,data) {
			SEMICOLON.slider.revolutionSliderMenu();
		});

		
	}	}); 
	</script>
	<!-- Main Js -->
	<script src="header/js/main.js"></script>
	<script src="js/customScript.js"></script>
	<script>
		 $(".flagsHidden").hide();
		$( ".seeAll" ).click(function() {
			   $(".flagsHidden").toggle();
			   $('.seeAll').html($('.seeAll').text() == 'see all' ? 'see less' : 'see all');
          });
		  $('.nav-left li:first-child').addClass('active')
		
	
	</script>

</body>

</html>