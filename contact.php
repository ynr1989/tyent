<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="customStyle.css">

</head>

<body id="page-top">
<?php include("phpIncludes/header.php") ?>

	<!-- /navbar ends -->

	
	<section id="content ">
        <div class="page headMargin poppins" id="top">
            
            
            <!-- Navigation panel -->
            
            <!-- End Navigation panel -->
            
            
            <!-- Head Section -->
            <section class="contactUsHead" data-background="cImages/contactUs.jpg" >
                
            </section>
            <!-- End Head Section -->
            
            
            <!-- Contact Section -->
            <section class="paddingTopBottom" id="contact">
                <div class="container relative">
                    
                    <div class="row mb-80 mb-xs-50">
                        <div class="col-md-8 col-md-offset-2 align-center">
                            
                            <!-- Section Titles -->
                            <h1 class="section-title mb-10 poppins">
                                Have a questions?
                            </h1>
                            
                            <!-- End Section Titles -->
                            
                            <div class="section-line mb-50 mb-xs-30"></div>
                            
                            <!-- Section Text -->
                            <div class="section-text poppins">
                               Get in touch with us
                            </div>
                            <!-- End Section Text -->
                            
                        </div>
                    </div>
                    
                    
                    <div></div>
                    
                    
                    <!-- Contact Information -->
                    <div class="row multi-columns-row alt-features-grid-1">
                        <div style="display: none;" hidden>
                        
<svg xmlns="http://www.w3.org/2000/svg" style="display: none;"><symbol id="phone" viewBox="0 0 513.64 513.64"><title>phone</title><path d="M499.66,376.96l-71.68-71.68c-25.6-25.6-69.12-15.359-79.36,17.92c-7.68,23.041-33.28,35.841-56.32,30.72 c-51.2-12.8-120.32-79.36-133.12-133.12c-7.68-23.041,7.68-48.641,30.72-56.32c33.28-10.24,43.52-53.76,17.92-79.36l-71.68-71.68 c-20.48-17.92-51.2-17.92-69.12,0l-48.64,48.64c-48.64,51.2,5.12,186.88,125.44,307.2c120.32,120.32,256,176.641,307.2,125.44 l48.64-48.64C517.581,425.6,517.581,394.88,499.66,376.96z"/></symbol><symbol id="email" viewBox="0 0 512 512"><title>email</title><g> <g> <path d="M467,61H45c-6.927,0-13.412,1.703-19.279,4.51L255,294.789l51.389-49.387c0,0,0.004-0.005,0.005-0.007 c0.001-0.002,0.005-0.004,0.005-0.004L486.286,65.514C480.418,62.705,473.929,61,467,61z"/> </g> </g><g> <g> <path d="M507.496,86.728L338.213,256.002L507.49,425.279c2.807-5.867,4.51-12.352,4.51-19.279V106 C512,99.077,510.301,92.593,507.496,86.728z"/> </g> </g><g> <g> <path d="M4.51,86.721C1.703,92.588,0,99.073,0,106v300c0,6.923,1.701,13.409,4.506,19.274L173.789,256L4.51,86.721z"/> </g> </g><g> <g> <path d="M317.002,277.213l-51.396,49.393c-2.93,2.93-6.768,4.395-10.605,4.395s-7.676-1.465-10.605-4.395L195,277.211 L25.714,446.486C31.582,449.295,38.071,451,45,451h422c6.927,0,13.412-1.703,19.279-4.51L317.002,277.213z"/> </g> </g></symbol><symbol id="home" viewBox="0 0 512 512"><title>home</title><path d="m256 4c-108.075 0-196 87.925-196 196 0 52.5 31.807 119.92 94.537 200.378a1065.816 1065.816 0 0 0 93.169 104.294 12 12 0 0 0 16.588 0 1065.816 1065.816 0 0 0 93.169-104.294c62.73-80.458 94.537-147.878 94.537-200.378 0-108.075-87.925-196-196-196zm0 336c-77.2 0-140-62.8-140-140s62.8-140 140-140 140 62.8 140 140-62.8 140-140 140z"/><path d="m352.072 183.121-88-80a12 12 0 0 0 -16.144 0l-88 80a12.006 12.006 0 0 0 -2.23 15.039 12.331 12.331 0 0 0 10.66 5.84h11.642v76a12 12 0 0 0 12 12h28a12 12 0 0 0 12-12v-44a12 12 0 0 1 12-12h24a12 12 0 0 1 12 12v44a12 12 0 0 0 12 12h28a12 12 0 0 0 12-12v-76h11.642a12.331 12.331 0 0 0 10.66-5.84 12.006 12.006 0 0 0 -2.23-15.039z"/></symbol></svg>
                        </div>
                        <!-- Contact Item-->
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="alt-features-item-1 align-center">
                                <div class="alt-features-icon-1">
                                    <svg class="icon svgIcon">
                                        <use xlink:href="#phone" />
                                    </svg>
                                </div>
                                <h3 class="alt-features-title-1 poppins">Call us </h3>
                                <div class="alt-features-descr-1 poppins">
                                <a class="" href="tel:+9191824 14209">+91 91824 14209</a> 
                                </div>
                            </div>
                        </div>
                        <!-- End Contact Item -->
                        
                        <!-- Contact Item-->
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="alt-features-item-1 align-center">
                                <div class="alt-features-icon-1">
                                    <svg class="icon svgIcon">
                                        <use xlink:href="#email" />
                                    </svg>                                </div>
                                <h3 class="alt-features-title-1 poppins">We are located at</h3>
                                <div class="alt-features-descr-1 poppins">
									H.No. 8-2-277/23, Near TV9 Office,
									RoadNo.3, Banjara Hills, Hyderabad-34
                                </div>
                            </div>
                        </div>
                        <!-- End Contact Item -->
                        
                        <!-- Contact Item-->
                        <div class="col-sm-4 col-md-4 col-lg-4">
                            <div class="alt-features-item-1 align-center">
                                <div class="alt-features-icon-1">
                                    <svg class="icon svgIcon">
                                        <use xlink:href="#home" />
                                    </svg>                                </div>
                                <h3 class="alt-features-title-1 poppins">Email Us</h3>
                                <div class="alt-features-descr-1 email-link poppins">
                                    <a href="mailto:support@bestlooker.pro">info@tyent.co.in</a>
                                </div>
                            </div>
                        </div>
                        <!-- End Contact Item -->
                    
                    </div>
                    <!-- End Contact Information -->
                    
                    
                </div>
            </section>
            <!-- End Contact Section -->
            
            
            <!-- Google Map -->
            <div class="google-map">
                
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.720617745976!2d78.43457251424174!3d17.425190306301715!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb97362d755fed%3A0xdb0a99fb1e59f387!2sMedilight%20Pvt%20Ltd!5e0!3m2!1sen!2sin!4v1600264737728!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                
                
            </div>
            <!-- End Google Map -->
            
            
            <!-- Feedback Form Section -->
            <?php include('form.php')?>

            <!-- End Feedback Form Section -->
        </div>

	</section>
    <?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="header/js/jquery.min.js"></script>
	<script src="header/js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/functions.js"></script>

  
	<!-- Main Js -->
    <script src="header/js/main.js"></script>

	
	<script src="js/customScript.js"></script>


</body>

</html>