<?php 

$travel = "cImages";
if(isset($dir)){
	$travel = "../cImages";
}
	
?>
<div class="contactHeader">
	<div class="container">
	<div class="col-md-6 col-sm-6 left">
			<a href="#"  data-toggle="modal" data-target="#contactHeaderModal"><i class="icon-time"></i>Book a free demo </a>
	</div>
	<div class="col-md-6 col-sm-6">
						<i class="icon-envelope2"></i> <a class="tel" href="mailto:info@medilightindia.com">info@medilightindia.com</a> <span class="middot"></span> 
						<i class="icon-phone3"></i> <a class="tel" href="tel:+919182414181">+91 91824 14181</a> 
	</div>
	</div>
</div>
<nav class="navbar navbar-custom navbar-fixed-top">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button id="mobileButton" type="button" class="navbar-toggle collapsed" data-toggle="collapse"
					data-target="#navbar-brand-centered">
					<img class="mobileBars" src="<?php echo $travel?>/menu.png">
					<img class="mobileClose" src="<?php echo $travel?>/close.png">
				</button>
				<div class="navbar-brand navbar-brand-centered page-scroll">
					<a href="/index">
						<!-- logo  -->
						<img src="<?php echo $travel?>/logo.png" class="img-responsive" alt="">
					</a>
				</div>
			</div>
			<!--/navbar-header -->
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="navbar-brand-centered">
				<ul class="nav navbar-nav nav-left">
					<li ><a href="/index">Home</a></li>
					<li class="dropdown" id="productLi">
						<a class="dropdown-toggle" aria-expanded="false"  aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" href="#" id="productsMenu">Products<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li class="dropdown" >
								<a class="dropdown-toggle" aria-expanded="false" data-toggle="dropdown" href="#" id="nmpSeriesMenu">NMP Series<b class="icon-caret-right"></b></a>
								<ul class="dropdown-menu dropdown-menuRight">
									<li><a href="/product/nmmp-5-plates-water-ionizer">NMP 5 Plate</a></li>
									<li><a href="/product/nmmp-7-plates-water-ionizer">NMP 7 Plate</a></li>
									<li><a href="/product/nmmp-9-plates-water-ionizer">NMP 9 Plate</a></li>
									<li><a href="/product/nmmp-11-plates-water-ionizer">NMP 11 Plate</a></li>
								</ul>
							</li>
							<li class="dropdown" >
								<a class="dropdown-toggle" aria-expanded="false" data-toggle="dropdown" id="uceSeriesMenu" href="#">UCE Series<b class="icon-caret-right"></b></a>
								<ul class="dropdown-menu dropdown-menuRight">
									<li><a href="/product/tyent-uce-9-water-ionizer">UCE 9 Plate</a></li>
									<li><a href="/product/tyent-uce-11-water-ionizer">UCE 11 Plate</a></li>
								</ul>
							</li>
							<li><a href="/product/electrolyzed-sterilizing-water-generator">Electric Water Generator</a></li>
							<li><a href="/product/tyentsoap">Tyent TM-X Soap</a></li>
							<li><a href="/product/h2Hybrid">Tyent H2 Hybrid</a></li>
						</ul>
					</li>
					<li ><a href="/technology">Technology</a></li>
					<li class="dropdown" id="aboutLi">
						<a class="dropdown-toggle" aria-expanded="false" data-toggle="dropdown" href="#">Why Tyent?<b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="/why_tyent">Why Tyent?</a></li>
						   <li><a href="/aboutus">About Tyent</a></li>
						   <li><a href="/applications">Applications</a></li>
						   <li><a href="/certifications">Certifications</a></li>
						   <li><a href="/testimonials">Testimonials</a></li>
						</ul>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					
					<li ><a href="/whyWaterIonizer">Why Water Ionizer?</a></li>
					<li><a href="/benefits">Benefits</a></li>
					<li><a href="/contact">Contact</a></li>
					<div class="contactHeader contactHeaderMobile">
					<div class="col-md-6 col-sm-6 left">
								<a href="#"  data-toggle="modal" data-target="#contactHeaderModal"><i class="icon-time"></i>Book a free demo </a>
						</div>
						<div class="col-md-6">
							<div>
								<i class="icon-envelope2"></i> <a class="tel" href="mailto:info@medilightindia.com">info@medilightindia.com</a> 
							</div>
							<div>
								<i class="icon-phone3"></i> <a class="tel" href="tel:+919182414181">+91 91824 14181</a> 	
							</div>			
						</div>
					</div>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container -->
	</nav>