<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Page title -->
    <?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?>
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
    <!-- Bootstrap Core CSS -->
    <link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
        rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />

    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="customStyle.css">

</head>

<body id="page-top">
    <?php include("phpIncludes/header.php") ?>

    <!-- /navbar ends -->


    <section id="content">
        <div class="container headMargin">
            <div class="col-md-12 paddingMobileLR5px">
                <div class="col-md-4 productImages">
                    <img class="xzoom" id="main_image" src="images/npm.jpg" xoriginal="images/npm.jpg"
                        style="width: 340px;">

                    <div class="xzoom-thumbs marginTop50px">
                        <a href="images/npm.jpg">
                            <img class="xzoom-gallery" width="60" src="images/npm.jpg">
                        </a>
                        <a href="images/nmpg1.png">
                            <img class="xzoom-gallery " width="60" src="images/nmpg1.png">
                        </a>
                        <a href="images/nmpg2.png">
                            <img class="xzoom-gallery" width="60" src="images/nmpg2.png">
                        </a>
                        <a href="images/nmp3.png">
                            <img class="xzoom-gallery" width="60" src="images/nmp3.png">
                        </a>
                    </div>
                </div>
                <div class="col-md-6 paddingMobileLR5px">
                    <div class="text- pro-infor marginTop10px">

                        <div class="pro-infor-title poppins">NMP 5 Plate</div>

                        <table class="pro-inforTable">

                            <colgroup>
                                <col width="135">
                                <col width="">
                            </colgroup>

                            <tbody>
                                <tr>
                                    <td class="bold1">Product name</td>
                                    <td>Water ionizer</td>
                                </tr>

                                <tr>
                                    <td colspan="2" height="13"></td>
                                </tr>

                                <tr>
                                    <td class="bold1">Dimension(mm)</td>
                                    <td>300(W) x 135(D) x 355(H)</td>
                                </tr>

                                <tr>
                                    <td colspan="2" height="13"></td>
                                </tr>

                                <tr>
                                    <td class="bold1">Weight(kg)</td>
                                    <td>5.2</td>
                                </tr>

                                <tr>
                                    <td colspan="2" height="13"></td>
                                </tr>
                                <tr>
                                    <td class="bold1">Plates</td>
                                    <td>5</td>
                                </tr>

                                <tr>
                                    <td colspan="2" height="13"></td>
                                </tr>
                                <tr>
                                    <td class="bold1">Price</td>
                                    <td>₹ 1,51,000</td>
                                </tr>


                            </tbody>
                        </table>

                    </div>

                </div>
            </div>

        </div>



    </section>
    <section id="content">
        <div class="container ">
            <div class="tab-con-box nmpFeatures">

                <div class="tab-box-title">
                    <div class="tab-title">Features</div>
                </div>
                <div class="row marginTop50px">
                    <div class="col-md-4 " style="text-align:center">
                        <img src="cImages/pro11_11_1_img03.png" alt="ONE-TOUCH system">
                        <div class="c36 fpx14 fw400 marginTop10px">ONE-TOUCH system</div>
                    </div>
                    <div class="col-md-4" style="text-align:center">
                        <img src="cImages/pro11_11_1_img04.png" alt="ONE-TOUCH system">
                        <div class="c36 fpx14 fw400 " style="margin-top:10px; ">Chrome
                            decorative pointModern and stylish design</div>
                    </div>
                    <div class="col-md-4" style="text-align:center">
                        <img src="cImages/pro11_11_1_img05.png" alt="ONE-TOUCH system">
                        <div class="c36 fpx14 fw400" style="margin-top:10px">46cm
                            flexible water outlet tube</div>
                    </div>
                </div>
                <div class="row marginTop50px">
                    <div class="col-md-3" style="text-align:center">
                        <img src="cImages/pro11_11_1_img06.png" alt="ONE-TOUCH system">
                        <div class="c36 fpx14 fw400" style="margin-top:5px">Fixed
                            quantity discharge function</div>
                    </div>

                    <div class="col-md-3" style="text-align:center">
                        <img src="cImages/pro11_11_1_img08.png" alt="ONE-TOUCH system">
                        <div class="c36 fpx14 fw400" style="margin-top:5px">Simple
                            Filter Replacement</div>
                    </div>
                    <div class="col-md-3" style="text-align:center">
                        <img src="cImages/pro11_11_1_img09.png" alt="ONE-TOUCH system">
                        <div class="c36 fpx14 fw400" style="margin-top:5px">A convenient open &amp;close
                            method</div>
                    </div>

                    <div class="col-md-3" style="text-align:center">
                        <img src="cImages/pro11_11_1_img10.png" alt="ONE-TOUCH system">
                        <div class="c36 fpx14 fw400" style="margin-top:5px">Wall-mounted
                            type</div>
                    </div>
                </div>
    </section>
    <section id="content">
        <div class="container">
            <div class="tab-con-box nmpFunctions">

                <div class="tab-box-title">
                    <div class="tab-title">Functions</div>
                </div>


                <div class="col-md-12 marginTop30px">
                    <div class="pageTitle" style="margin-top:50px">
                        <img src="cImages/sub_dot.gif" alt="">Control panel designed for
                        convenient use
                    </div>
                    <div class="col-md-6">
                        <img src="cImages/pro11_11_1_img11.jpg" alt="Control panel designed for convenient use">
                    </div>
                    <div class="col-md-6">
                        <div class="fpx16 fw600 fblue2">ONE TOUCH system</div>
                        <div class="fpx14 line14" style="margin-top:15px;">Each function
                            selection is ionized, and you can outflow and stop the desired
                            water with one touch button.</div>
                        <div class="fpx16 fw600 fblue2" style="margin-top:10px;">Color
                            backlight
                            function</div>
                        <div class="fpx14 line14" style="margin-top:15px; letter-spacing:-0.2px;">
                            It distinguishes alkali water and acid water with the color of
                            backlight.
                        </div>
                        <div class="fpx16 fw600 fblue2" style="margin-top:10px;">2way
                            Control
                            method</div>
                    </div>
                </div>

                <div class="col-md-12 marginTop30px">
                    <div class="pageTitle"><img src="cImages/sub_dot.gif" alt="">DISPLAY
                        COLORS</div>
                    <div class="col-md-12">
                        <img src="cImages/pro11_11_1_img13.png" alt="DISPLAY COLORS">
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="pageTitle" style="margin-top:60px;"><img src="cImages/sub_dot.gif" alt="">Various
                        convenience functions
                    </div>
                    <div class="fw600 fblue2 fpx16">Fixed quantity discharge function</div>
                    <div class="fpx14 line14" style="margin-top:17px;">
                        You can drink the fixed amount of water which uses a
                        lot in our everyday lives such as 0.5L, 1.0L and 1.5L
                        via Fixed quantity discharge function.

                    </div>
                </div>
                <div class="col-md-12 marginTop30px">
                    <div class="col-md-6">
                        <img src="cImages/pro11_11_1_img15.png" alt="Real time Filter Usage Indication">
                    </div>
                    <div class="col-md-6">
                        <div class="fw600 fblue2 fpx16 mt60">Real time Filter Usage
                            Indication
                        </div>
                        <div class="fpx14 line14">
                            Level of filter usage can be identified through the
                            LCD Screen on a real time basis. The
                            recommended replacement time is automatically displayed.
                        </div>
                    </div>
                </div>
                <div class="col-md-12 marginTop30px">
                    <div class="col-md-6">
                        <div class="fw600 fblue2 fpx16">Upgraded Power
                            Turbo System</div>
                        <div class="fpx14 line14">
                            Turbo upgrades to the SMPS power supply system
                            produce both the acid turbo water and strong alkaline
                            water you need!
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img src="cImages/pro11_11_1_img17.png" alt="Upgraded Power Turbo System">
                    </div>
                </div>

                <div class="col-md-12 marginTop30px">
                    <div class="col-md-6">
                        <img src="cImages/pro11_11_1_img18.png" alt="ECO Mode">
                    </div>
                    <div class="col-md-6">
                        <div class="fw600 fblue2 fpx16">ECO Mode</div>
                        <div class="fpx14 line14" style="margin-top:15px;">
                            When setting the eco-mode, the LCD backlight will be
                            turned off automatically during periods of non-use and
                            the Time function is displayed
                        </div>
                    </div>
                </div>
                <div class="col-md-12 marginTop30px">
                    <div class="col-md-6">
                        <img src="cImages/pro11_11_1_img20.png" alt="Drain Faucet">
                    </div>
                    <div class="col-md-6">
                        <div class="pageTitle" style="margin-top:60px;">

                            Drain Faucet
                        </div>
                        <div class="fpx14 line14" style="margin-top:15px;">
                            It comes with a Drain faucet that can be easily
                            installed to anywhere on the sink.
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="pageTitle" style="margin-bottom:30px;"><img src="cImages/sub_dot.gif" alt="">LCD Display
                        and Touch Pad
                        Overview
                    </div>
                    <div class="imgWrap mt40"><img src="cImages/pro11_11_1_img21.png"
                            alt="LCD Display and Touch Pad Overview" class="auto"></div>
                </div>



                <div class="">
                    <div class="col-md-4">
                        <table class="buttonTable marginTop30px" style="width:100%;">
                            <tbody>
                                <tr>
                                    <td class="bg_sky">No</td>
                                    <td class="bg_gray"></td>
                                    <td class="bg_sky">Name</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">01</td>
                                    <td class="bg_gray"></td>
                                    <td>Turbo</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">02</td>
                                    <td class="bg_gray"></td>
                                    <td>Acidic Level 2</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">03</td>
                                    <td class="bg_gray"></td>
                                    <td>Acidic Level 1</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">04</td>
                                    <td class="bg_gray"></td>
                                    <td>Purified water</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">05</td>
                                    <td class="bg_gray"></td>
                                    <td>Alkaline water 1</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">06</td>
                                    <td class="bg_gray"></td>
                                    <td>Alkaline water 2</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">07</td>
                                    <td class="bg_gray"></td>
                                    <td>Alkaline water 3</td>
                                    <td class="bg_gray"></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <table class="buttonTable marginTop30px" style="width:100%;">
                            <tbody>
                                <tr>
                                    <td class="bg_sky">No</td>
                                    <td class="bg_gray"></td>
                                    <td class="bg_sky">Name</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">08</td>
                                    <td class="bg_gray"></td>
                                    <td>Time</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">09</td>
                                    <td class="bg_gray"></td>
                                    <td>pH</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">10</td>
                                    <td class="bg_gray"></td>
                                    <td>Water flow</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">11</td>
                                    <td class="bg_gray"></td>
                                    <td>Voltage/Flow/Time</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">12</td>
                                    <td class="bg_gray"></td>
                                    <td>Door open</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">13</td>
                                    <td class="bg_gray"></td>
                                    <td>Temperature</td>
                                    <td class="bg_gray"></td>
                                </tr>

                                <tr>
                                    <td class="bold">14</td>
                                    <td class="bg_gray"></td>
                                    <td>Remaining time</td>
                                    <td class="bg_gray"></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <table class="buttonTable marginTop30px" style="width:100%;">
                            <tbody>
                                <tr>
                                    <td class="bg_sky">No</td>
                                    <td class="bg_gray"></td>
                                    <td class="bg_sky">Name</td>
                                </tr>

                                <tr>
                                    <td class="bold">15</td>
                                    <td class="bg_gray"></td>
                                    <td>Fixed quantity discharge</td>
                                </tr>

                                <tr>
                                    <td class="bold">16</td>
                                    <td class="bg_gray"></td>
                                    <td>System set up</td>
                                </tr>

                                <tr>
                                    <td class="bold">17</td>
                                    <td class="bg_gray"></td>
                                    <td>Cleaning</td>
                                </tr>

                                <tr>
                                    <td class="bold">18</td>
                                    <td class="bg_gray"></td>
                                    <td>Volume On / Off</td>
                                </tr>

                                <tr>
                                    <td class="bold">19</td>
                                    <td class="bg_gray"></td>
                                    <td>1st Filter reset</td>
                                </tr>

                                <tr>
                                    <td class="bold">20</td>
                                    <td class="bg_gray"></td>
                                    <td>2nd Filter reset</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container ">
            <div class="otherCetificate">
                <h3>Tyent USA’s Other Certifications</h3>
                <div class="row">
                    <div class="col-md-2"><img src="cImages/certifications/tm.webp" height="39" width="76"
                            alt="Total Microorganism">
                        <p>Total Microorganism</p>
                    </div>
                    <div class="col-md-2"> <img src="cImages/certifications/kgmp.webp" height="39" width="80"
                            alt="Korea Good Manufacturing">
                        <p>Korea Good Manufacturing Practice</p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/kwpic.webp" height="39" width="91"
                            alt="Korea Water Purifier ">
                        <p>Korea Water Purifier Industry Cooperative</p>
                    </div>
                    <div class="col-md-2"> <img src="cImages/certifications/kipo.webp" height="39" width="80"
                            alt="Korean Intellectual Property">
                        <p>Korean Intellectual Property Office</p>
                    </div>
                    <div class="col-md-2"> <img src="cImages/certifications/ukas-1.jpg" height="39" width="80"
                            alt="UKAS Registered">
                        <p>UKAS Registered ISO14001:2004</p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/kfda.webp" height="39" width="76"
                            alt="Korea Food &amp; Drug Administration">
                        <p>Korea Food &amp; Drug Administration</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"><img src="cImages/certifications/ukas-2.jpg" height="39" width="80"
                            alt="UKAS Registered">
                        <p>UKAS Registered ISO9001:2000</p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/ce.webp" alt="CE Insignia">
                        <p>CE Insignia</p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/cb.webp"
                            alt="CB Scheme for Electrical Equipment">
                        <p>CB Scheme for Electrical Equipment</p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/product-safety.webp" alt="product-safety">
                        <p>TUV</p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/g.webp" alt="Good Design">
                        <p>Good Design</p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/ul.webp" alt="UL Collaborative Standards">
                        <p>UL Collaborative Standards Development System</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2"> <img src="cImages/certifications/ansi.webp" alt="ANSI">
                        <p>ANSI Accredited Certification Program </p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/iec.webp"
                            alt="Independent Electrical Contractors">
                        <p>Independent Electrical Contractors</p>
                    </div>
                    <div class="col-md-2"> <img src="cImages/certifications/innibiz.webp"
                            alt="Innovative Business Company">
                        <p>Innovative Business Company Limited</p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/bbb.webp" alt="Better Business Bureau">
                        <p>Better Business Bureau</p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/gmp.webp" alt="Good Manufacturing Practice">
                        <p>Good Manufacturing Practice</p>
                    </div>
                    <div class="col-md-2"><img src="cImages/certifications/permelic.webp" alt="Permelec Electrode">
                        <p>Permelec Electrode Ltd.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container paddingTopBottom">
            <div class="heading-block center ">
                <h3 class="poppins letterSpacing2px fontWeight100px">Comparison</h3>
            </div>
            <div class="textCenter">
                <img src="cImages/Differences.jpg">
            </div>
        </div>
        <?php include('form.php')?>
    </section>
    <?php include("phpIncludes/footer.php") ?>

    <!-- /footer ends -->
    <!-- Core JavaScript Files -->
    <script src="header/js/jquery.min.js"></script>
    <script src="header/js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/functions.js"></script>


    <!-- Main Js -->
    <script src="header/js/main.js"></script>

    <script type="text/javascript" src="js/xzoom.min.js"></script>
    <script type="text/javascript" src="js/sticky-sidebar.js"></script>

    <script src="js/customScript.js"></script>
    <script>
        $('#uceSeriesMenu').parent().addClass('active');
    </script>

    <script>
        $(function () {
            $("#specifications").hide();
            $("#side-navigation").tabs({
                show: {
                    effect: "fade",
                    duration: 400
                }
            });
        });
        $(".xzoom, .xzoom-gallery").xzoom({
            tint: '#333',
            Xoffset: 15,
            defaultScale: -0.3
        });
        $('#productLi').addClass('active');
        $(".mobileCloseButton").click(function () {
            $('.ui-tabs-tab').toggle();
            $(".mobileCloseButton").html("<i class='icon-line-rewind'></i>");
            $(".mobileCloseButton").closest("ul.sidenav").toggleClass("transparentBG");
        });
    </script>

</body>

</html>