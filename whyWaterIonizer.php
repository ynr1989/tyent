<!DOCTYPE html>
<html>
 
<head>
    <meta charset="utf-8">
    <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
    <!-- Bootstrap Core CSS -->
    <link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
        rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />

    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="customStyle.css">
<style>
@media screen and (max-width: 992px) and ( min-width:767px){
.col-md-4 {
    text-align: center;
    margin-bottom: 20px;
}
}
.nav-item a:hover {
    background-color: #12121359 !important;
}
.feature-tab-circle .nav-item.active a{
    background-color: #12121359 !important;
    color: #fff !important; 
}
</style>

</head>

<body id="page-top">
    <?php include("phpIncludes/header.php") ?>

    <!-- /navbar ends -->


    <section id="content whyAlkalineWaterPage">
        <div class="container headMargin paddingTopBottom poppins alkalineWaterSection">
            <h4 class="poppins ">Is the majority of the human body is <span>WATER</span>? </h4>

            <div class="col-md-12 ">
                <div class="col-md-4">
                    <img class="imgRounded" src="cImages/alkalineWater/waterContent.png">
                </div>
                <div class="col-md-8">

                    <p>
                        According to H.H. Mitchell, Journal of Biological Chemistry 158. Water plays an important role in our body, it constitutes 60 to 
                        70% of our body weight. On average, the body of an adult human being contains 60% water. Most of the water in the human 
                        body is contained inside our cells. In fact, our billions of cells must have water to live. The total amount of water in our 
                        body is found in three main locations: within our cells (two-thirds of the water), in the space between our cells and in our blood 
                        (one-third of the water). For example, a 70-kg man is made up of about 42L of total water.

                    </p>
                    <p>
                        So it's not much of a stretch to say that "the basis of vitality, Health & long life is water." If you want to feel good and be 
                        at your optimal health level, one should drink water with essential minerals on a daily basis. But not just any type of water.

                    </p>
                </div>
            </div>
        </div>
        <div class="section dark nomargin paddingTopBottom " style="background-color: #111; padding: 20px 0;">
            <div class="container clearfix poppins">

                <div class="heading-block center bottommargin-sm nobottomborder">
                    <h2 class="nott poppins" style="font-size: 46px;">Functions of water in human body</h2>
                    <span>This substance makes up a majority of your body weight and is involved in many important
                        functions</span>
                </div>

                <div class="row topmargin-sm">

                    <div class="col-md-4 col-sm-6">

                        <div class="feature-box fbox-dark fbox-effect fbox-right  fadeIn animated" data-animate="fadeIn">
                            <div class="fbox-icon">
                                <i class="icon-line-check"></i>
                            </div>
                            <h3>Flushing out waste from your body.</h3>
                        </div>

                        <div class="feature-box fbox-dark fbox-effect fbox-right topmargin fadeIn animated" data-animate="fadeIn" data-delay="200">
                            <div class="fbox-icon">
                                <i class="icon-line-check"></i>
                            </div>
                            <h3>It regulates your body temperature.</h3>
                        </div>

                        <div class="feature-box fbox-dark fbox-effect fbox-right topmargin fadeIn animated" data-animate="fadeIn" data-delay="400">
                            <div class="fbox-icon">
                                <i class="icon-line-check"></i>
                            </div>
                            <h3>Helping your brain function.</h3>
                        </div>
                        <div class="feature-box fbox-dark fbox-effect fbox-right topmargin fadeIn animated" data-animate="fadeIn" data-delay="400">
                            <div class="fbox-icon">
                                <i class="icon-line-check"></i>
                            </div>
                            <h3>It helps with nutrient absorption.</h3>
                        </div>
                        <div class="feature-box fbox-dark fbox-effect fbox-right topmargin fadeIn animated" data-animate="fadeIn" data-delay="400">
                            <div class="fbox-icon">
                                <i class="icon-line-check"></i>
                            </div>
                            <h3>It helps you lose weight.</h3>
                        </div>    
                    </div>

                    <div class="col-md-4 hidden-sm bottommargin center">
                        <img src="cImages/alkalineWater/waterGlass.jpg" alt="workout">
                    </div>

                    <div class="col-md-4 col-sm-6 bottommargin">

                        <div class="feature-box fbox-dark fbox-effect fadeIn animated" data-animate="fadeIn">
                            <div class="fbox-icon">
                                <i class="icon-line-check"></i>
                            </div>
                            <h3>Protects your tissues, spinal cord, and joints.</h3>
                        </div>

                        <div class="feature-box fbox-dark fbox-effect topmargin fadeIn animated" data-animate="fadeIn" data-delay="200">
                            <div class="fbox-icon">
                                <i class="icon-line-check"></i>
                            </div>
                            <h3>It helps excrete waste.</h3>
                        </div>

                        <div class="feature-box fbox-dark fbox-effect topmargin fadeIn animated" data-animate="fadeIn" data-delay="400">
                            <div class="fbox-icon">
                                <i class="icon-line-check"></i>
                            </div>
                            <h3>It helps maximize physical performance.</h3>
                        </div>
                        <div class="feature-box fbox-dark fbox-effect topmargin fadeIn animated" data-animate="fadeIn" data-delay="400">
                            <div class="fbox-icon">
                                <i class="icon-line-check"></i>
                            </div>
                            <h3>It improves blood oxygen circulation.</h3>
                        </div>
                        <div class="feature-box fbox-dark fbox-effect topmargin fadeIn animated" data-animate="fadeIn" data-delay="400">
                            <div class="fbox-icon">
                                <i class="icon-line-check"></i>
                            </div>
                            <h3>It helps keep skin clean and bright.</h3>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        
        <!-- <div class="container alkalineWaterSection2 paddingTopBottom">
            <h2>Problems with <br><span>Tap Water</span></h2>
            <div class="col-md-12">
                <div class="col-md-4">
                    <img src="cImages/alkalineWater/tapWater.jpg">
                </div>
                <div class="col-md-8">
                    <p>
                        Tap water is a healthier option than soft drinks. Most of India's tap water is unfit for
                        consumption. A vast majority of India's Tap water supply is contaminated with sewage and
                        agricultural runoff, containing high levels of arsenic, fluorine, iron, saline and other
                        contaminants which seep to the groundwater and makes it unfit for drinking. Nearly 76 million
                        people in India do not have access to safe drinking water, as poor storage infrastructure & old
                        pipelines over the years has created which makes the tap water no safe to drink.
                    </p>
                    <p>
                        At the current levels of contamination, the public is starting to question the quality of tap
                        water and wonders if it is safe enough to drink. Based on these facts, it's no wonder that the
                        demand for Ionized hydrogen is constantly increasing.
                    </p>
                </div>
            </div>
        </div> -->
        <div class="banner info-min viewport-section in-viewport">
			<div class="bg-stretch bg-agrochem" >
				
				<div class="container">
					<div class="text-center ">
						<h3 class="poppins">Take care of your body, its the only place you have to live in</h3>
					</div>
				</div>
			</div>
		</div>
        <div class="container alkalineWaterSection2 paddingTopBottom">
            <h2>Problems with <br><span>Bottled Water</span></h2>
            <div class="col-md-12">
                <div class="col-md-8">
                    <p>
                        Bottled water is not necessarily healthier than tap water and yet it costs much more! Although
                        associated with healthy water, bottled water is not guaranteed to be any healthier than tap
                        water. Often the only difference is the added minerals which have no proven health benefits.
                        Bottled water can still create health problems as it is slightly acidic, which may affect your
                        body's pH balance.
                    </p>
                    <p>
                        Testing from the NRDC found that over 30% of bottled water samples showed signs of
                        contamination. That means it may not be safer than some tap water options that are available in
                        the India and around the world. Finally, the environmental impact caused by the production and
                        disposal of plastic bottles is harmful to our ecosystem.
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="cImages/alkalineWater/bottledWater.jpg">
                </div>
            </div>
        </div>
        <div class="banner info-min viewport-section in-viewport">
			<div class="bg-stretch bg-agrochem" >
				
				<div class="container">
					<div class="text-center ">
						<h3 class="poppins">Your <strong>Health</strong> Is An Investment. Not An Expense.</h3>
					</div>
				</div>
			</div>
		</div>
        <div class="container alkalineWaterSection2 paddingTopBottom">
            <h2>Problems with <br><span>RO Water</span></h2>
            <div class="col-md-12">
                <div class="col-md-4">
                    <img src="cImages/alkalineWater/roWater.jpg">
                </div>
                <div class="col-md-8">
                    <p>
                        Reverse osmosis is a water purification technology that uses a semi-permeable membrane to remove
                        ions, molecules and larger particles from drinking water. In reverse osmosis, all the natural
                        use-full minerals which are present in the water will be removed along all the bacteria, virus
                        and other hard minerals such chlorine and Florine. These minerals not only provide good taste,
                        they also serve a vital function in the body's system. When stripped of these minerals, water
                        can be unhealthy.
                    </p>
                    <p>
                        The problem with this process is that dangerous chemicals like pesticides, herbicides, and
                        chlorine are molecularly smaller than water and can pass freely through the filter. Also, 2-3
                        gallons of water are wasted for every gallon of purified water produced.
                    </p>
                </div>

            </div>
        </div>
        <div class="banner info-min viewport-section in-viewport">
			<div class="bg-stretch bg-agrochem" >
				
				<div class="container">
					<div class="text-center ">
						<h3 class="poppins"><strong>Healthy water</strong> is the worlds first and foremost medicine</h3>
					</div>
				</div>
			</div>
        </div>
        
        <div class="container alkalineWaterSectionWaterIon paddingTopBottom">
            <h2>What is <span>Water ionizer</span>?</h2>
            <div class="col-md-12">
                
                <div class="col-md-8">
                    <p>
                    A water ionizer  is a home appliance which claims to raise the pH of drinking water by using 
                    electrolysis to separate the incoming water stream into acidic and alkaline components. 
                    The alkaline stream of the treated water is called alkaline water. Health experts claim that 
                    consumption of alkaline water results in a variety of health benefits.
                    </p>
                    <p>
                        Water ionizers produce 
                        alkaline water containing dissolved selective anti-oxidant called molecular hydrogen at the 
                        cathode (the negative electrode) and acidic water at the anode (the positive electrode).
                         Water ionizers are plugged into the AC outlet, the power is then transformed to direct current
                          (DC) so that electrolysis can be performed.
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="cImages/prod/NMP_thumb.png">
                </div>
            </div>
        </div>
        <div class="banner info-min viewport-section in-viewport">
			<div class="bg-stretch bg-agrochem" >
				
				<div class="container">
					<div class="text-center ">
						<h3 class="poppins"><strong>Healthy water</strong> helps in boosting immunity</h3>
					</div>
				</div>
			</div>
        </div>
        <div class="container alkalineWaterSectionHydrogen paddingTopBottom">
            <h2><span>Hydrogen</span> An Emerging Medical Gas</h2>
            <div class="col-md-12">
                
                <div class="col-md-8">
                    <p>
                        Although the research is early, the 1000+ scientific articles suggest that H2 has therapeutic potential in over 170 different human and animal disease models, and essentially every organ of the human body.
                        Molecular hydrogen (H2) or diatomic hydrogen is a tasteless, odorless, flammable gas.
                    </p>
                    <p>
                        H2 reduces oxidative stress and improves redox homeostasis partly mediated via the Nrf2 pathway, which regulates levels of glutathione, superoxide dismutase, catalase, etc.
                        *H2, like other gaseous-signaling molecules (e.g. NO, CO, H2 S), modulates signal transduction, protein phosphorylation, and gene expression, which provides its anti-inflammatory, anti-allergy, and anti-apoptotic protective effects.
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="cImages/Hydrogen_Tile-300x300.png">
                </div>
            </div>
        </div>
        <div class="banner info-min viewport-section in-viewport">
			<div class="bg-stretch bg-agrochem" >
				
				<div class="container">
					<div class="text-center ">
						<h3 class="poppins">Healthy <strong>water </strong>! Healthy <strong> life</strong> !</h3>
					</div>
				</div>
			</div>
        </div>
        <div class="container alkalineWaterSection2 paddingTopBottom">
            <h2>Advantages with <br><span>Tyent®️ ionized water</span></h2>
            <div class="col-md-12">

                <div class="col-md-8">
                    <p>
                        Ionized hydrogen rich alkaline Water will contain thousands of tiny bubbles when it pours out of
                        a Tyent water ionizer. Those bubbles are molecular hydrogen, which is the antioxidant property
                        of ionized alkaline water. Molecular hydrogen is the most fragile aspect of ionized water,
                        lasting only a maximum of 18-24 hours, which is why it is important to consume ionized water
                        when it is fresh out of your Tyent water ionizer. Ionized hydrogen rich alkaline Water is rich
                        in minerals such as calcium, magnesium, Potassium & silica which are most required for human
                        body metabolism. </p>
                    <p>
                        Ionized hydrogen rich alkaline Water helps in boosting immunity, anti-inflammatory properties,
                        anti-allergy properties, anti-apoptotic protective, anti-aging properties, colon-cleansing
                        properties, weight loss, cancer resistance & and other detoxifying properties. Although the
                        research says 1000+ scientific articles suggest that H2 has therapeutic potential in over 170
                        different human and animal disease models, and essentially every organ of the human body.
                    </p>
                </div>
                <div class="col-md-4">
                    <img src="cImages/alkalineWater/alkalineWater.jpg">
                </div>
            </div>
        </div>
        <div class="dark clearfix alkalineWaterSection3 paddingTopBottom paddingMobileLR5px" style="background-color: rgb(103, 58, 183);">
            <div class="container">
                <div class="col-md-12 strongText">
                    <h2 class="strongText">Did you know different pH level water serves a specific purpose? </h2>
                </div>
                <div class="col-md-12">
                    <div class="feature-contents section-heading ptb-100">
                        <div class="feature-content-wrap">
                            <div class="tab-content feature-tab-content">
                                <div class="tab-pane active" id="tab6-1">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h2 class="strongText">Strong Alkaline Water <span>pH 11.5</span> </h2>
                                            <p>Not for drinking. Strong Alkaline Water preserves hygiene in your daily
                                                life due to its Micro clustering property and higher alkalinity it has
                                                strong cleaning effect which helps in removing Wax content and
                                                Pesticides from fruits & Vegetables.</p>
                                            <p>Usage: Deep cleaning food and vegetables</p>
                                        </div>
                                        <div class="col-md-4">
                                            <img class="img-responsive imageShadow" src="cImages/phWater1.jpg">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab6-2">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h2 class="strongText">Alkaline Water <span>pH 8.5-9.5</span></h2>
                                            <p>This type of water is perfect for drinking and healthy cooking. This
                                                electrolytically-reduced, hydrogen-rich water works to restore your body
                                                to a more alkaline state, which is optimal for good health.</p>
                                            <p>Usage: Drinking, food preparation, coffee and tea, soups and stews.</p>
                                        </div>
                                        <div class="col-md-4">
                                            <img class="img-responsive imageShadow" src="cImages/phWater2.jpg">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab6-3">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h2 class="strongText">Clean Water <span>pH 7</span> </h2>
                                            <p>This type of water is perfect for drinking during food & medicine. Free
                                                of chlorine, rust and cloudiness. Neutral water is delicious drinking
                                                water.</p>
                                            <p>Usage: Preparing baby food, drinking water along with food & taking
                                                medication.</p>
                                        </div>
                                        <div class="col-md-4">
                                            <img class="img-responsive imageShadow" src="cImages/phWater3.jpg">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab6-4">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h2 class="strongText">Beauty Water <span>pH 5.0-5.5</span> </h2>
                                            <p>Not for drinking. This slightly acidic water is recognized for its
                                                astringent effects. It's terrific to use for gentle cleaning and beauty
                                                care.</p>
                                            <p>Usage: face wash, hair care, pet care, polishing, cleaning, and
                                                preserving frozen food.</p>
                                        </div>
                                        <div class="col-md-4">
                                            <img class="img-responsive imageShadow" src="cImages/phWater4.jpg">
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab6-5">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h2 class="strongText">Acidic Water <span>pH 4.0</span> </h2>
                                            <p>Not for drinking. This water has cleansing properties. Use Strong Acidic
                                                Water to clean kitchen utensils, counter-tops, and more to prevent
                                                cross-contamination.</p>
                                            <p>Usages: cleaning and reducing germs, hygiene, and commercial operation.
                                            </p>
                                        </div>
                                        <div class="col-md-4">
                                            <img class="img-responsive imageShadow" src="cImages/phWater5.jpg">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul class="nav nav-tabs feature-tab feature-tab-circle" data-tabs="tabs" >
                                <li class="nav-item active">
                                    <a class="h6" href="#tab6-1" data-toggle="tab" data-color="#673ab7">
                                        <span class="flaticon-cardiogram"><i
                                                class="icon-line-drop colorPurple"></i></span>
                                    
                                    Strong Alkaline Water</a>
                                </li>
                                <li class="nav-item">
                                    <a class="h6" href="#tab6-2" data-toggle="tab" data-color="#0088c6">
                                        <span class="flaticon-blood-pressure"><i
                                                class="icon-line-drop colorBlue"></i></span>
                                    
                                    Alkaline Water</a>
                                </li>
                                <li class="nav-item">
                                    <a class="h6" href="#tab6-3" data-toggle="tab" data-color="#287c3c">
                                        <span class="flaticon-bell"><i 
                                        class="icon-line-drop colorGreen"></i></span>
                                    
                                    Clean Water</a>
                                </li>
                                <li class="nav-item">
                                    <a class="h6" href="#tab6-4" data-toggle="tab" data-color="#ffba00">
                                        <span class="flaticon-hand-finger-pressing-a-circular-ring-button"><i
                                                class="icon-line-drop colorYellow"></i></span>
                                    
                                    Beauty Water</a>
                                </li>
                                <li class="nav-item">
                                    <a class="h6" href="#tab6-5" data-toggle="tab" data-color="#d91413">
                                        <span class="flaticon-hand-finger-pressing-a-circular-ring-button"><i
                                                class="icon-line-drop colorRed"></i></span>
                                    
                                    Acidic Water</a>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
    <?php include("phpIncludes/footer.php") ?>

    <!-- /footer ends -->
    <!-- Core JavaScript Files -->
    <script src="header/js/jquery.min.js"></script>
    <script src="header/js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/functions.js"></script>


    <!-- Main Js -->
    <script src="header/js/main.js"></script>
    <script src="js/customScript.js"></script>
    <script>
        $('.feature-tab-circle li a').click(function(){var color = $(this).attr('data-color');$('.alkalineWaterSection3').css('background-color',color);})
    </script>

</body>

</html>