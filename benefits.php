<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
    <!-- Bootstrap Core CSS -->
    <link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
        rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />

    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="customStyle.css">

</head>

<body id="page-top">
    <?php include("phpIncludes/header.php") ?>

    <!-- /navbar ends -->


    <section id="content">
        <div class="container headMargin paddingTop">
            <div class="title-block poppins">
                <h1 class="fontColorBlue">Drinking Tyent Water can improve your health in many ways.</h1>
                <p class="marginTop10px">Here is a list of ways you and your family can benefit from the touch of a
                    button</p>
            </div>
            <div class="col-md-12 benefitList">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/Gastric.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Gastric</h1>
                        <p>Ionized Hydrogen rich alkaline Water Improves Digestive Health and promotes
                            easy digestibility. Hydrogen stimulates gastric leptin and ghrelin. These are 
                            hormones that the body uses to regulate fat storage, energy, and a variety of other metabolic functions.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/skin.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Skin issues</h1>
                        <p> Using hydrogen water has the potential to be a safe and effective measure in anti-ageing
                            skin care. Alkaline water was shown to promote type-1 collagen synthesis and 
                            decrease destruction of keratin cells while reducing wrinkle formation.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 benefitList">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/Arthritis.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Arthritis</h1>
                        <p>Consumption of ionized alkaline water containing a high concentration of molecular 
                            hydrogen reduces oxidative stress and disease activity in patients with rheumatoid 
                            arthritis.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/respiratory.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Respiratory diseases</h1>
                        <p>Ionized Hydrogen-enriched alkaline water eliminates fine particles from the 
                            lungs and blood by enhancing phagocytic activity.</p>
                    </div>

                </div>
            </div>
            <div class="col-md-12 benefitList">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/Heartproblems.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Heart problems</h1>
                        <p>Drinking ionized hydrogen rich alkaline water is clinically proven as a prevention and
                             remedy for cardiovascular diseases. Hydrogen significantly abate 
                             oxidative stress by suppress the inflammation and improve
                              overall heart function.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/Diabetics.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Reduce Risk of Type 2 Diabetes </h1>
                        <p>Hydrogen water helps promote insulin resistance.
                             This was demonstrated through a beneficial antioxidant effect, more effective lipid metabolism and the regulation of glucose levels.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 benefitList">
            <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/cancer.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Cancer</h1>
                        <p>Dr. Otto Heinrich Warburg, winner of the 1931 Nobel prize in physiology discovered the cause of cancer in 1923. <span class="quotz">"Cancer grows in Oxygen deprived ACIDIC tissue, Disease can't survive in an Alkaline body"</span></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/consitipation.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Constipation</h1>
                        <p>Alkaline water was proved to be effective against constipation, and most
                             importantly its’ safety was verified. It is considered a departure for using medication. The 
                             effectiveness of  hydrogen rich alkaline water is very high at 80.5% than normal 
                             packaged mineral water.</p>
                    </div>

                </div>
            </div>
            <div class="col-md-12 benefitList">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/energy.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Increased energy</h1>
                        <p>Hydrogen rich alkaline water helps turn your cells into "an antioxidant factory".
                             Dr. Perricone claim that drinking ionized hydrogen rich alkaline water can give you 
                             more energy, slow the aging process and even speed muscle recovery after a workout.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/weightloss.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Promotes weight loss</h1>
                        <p>Because of its ability to reverse the effects of oxidative stress on the body, 
                            drinking hydrogen water has been shown to stave off and reverse negative effects of metabolic symptoms.
                             Ionized Hydrogen-rich alkaline water also decreases bad cholesterol and aid in controlled weight loss.</p>
                    </div>

                </div>
            </div>
            <div class="col-md-12 benefitList">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/radiantskin.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Radiant skin</h1>
                        <p>Spraying Hydrogen Water on your skin repairs damaged cells, reduces inflammation, 
                            and makes skin healthier. Alkaline Ionized Water helps to ensure that your skin looks hydrated. 
                            Alkaline Water is more easily absorbed, it helps your skin stay fresh, resulting in a healthier better-looking skin.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/recovery.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Faster recovery time after exercise</h1>
                        <p>Potent hydrogen molecules enter the mitochondria of every cell and help energize each cell from within.
                            The difference in energy levels is palpable and has incredible potential for anyone taking part 
                            in sports, whether at a recreational or professional level.</p>
                    </div>

                </div>
            </div>
            <div class="col-md-12 benefitList">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/cololClensing.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Colon cleanse</h1>
                        <p>Colon are hormones that the body uses to regulate fat storage, energy, and other  functions. 
                            Drinking Ionized hydrogen rich alkaline water helps in better hydration, gastrointestinal cleansing, 
                            improves digestion and assists with breaking down undesirable fat.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/greenclean.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Green cleaning</h1>
                        <p>Wiping the area dry with a clean cloth after spraying with strong alkaline water can remove 99%
                             of germs and bacteria. If you have a water ionizer powerful enough to produce acidic water at a 2.5 or 
                             lower pH, this water can kill most germs and bacteria on contact.</p>
                    </div>

                </div>
            </div>
            <div class="col-md-12 benefitList">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/freeWax.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Free from wax & pesticides</h1>
                        <p>Alkaline Turbo water helps break down and wash away
                            oil-based pesticides and insecticides on produce.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/molecular.png">
                    </div>
                    <div class="col-md-7">
                        <h1>Molecular hydrogen - Selective anti-oxidant</h1>
                        <p>In an early research, scientific suggest that Hydrogen has therapeutic potential in over 170 human
                             diseases, and essentially every organ of the human body. It reduces oxidative stress and improves
                              redox homeostasis and provides anti-inflammatory,anti-allergy, and anti-apoptotic protective effects.</p>
                    </div>

                </div>
            </div>
            <div class="col-md-12 benefitList">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/sanitizesurface.JPG">
                    </div>
                    <div class="col-md-7">
                        <h1>Sanitize surfaces</h1>
                        <p>Strong Acidic Water is regarded as an excellent disinfectant. In the food industry, it can
                             be substituted for alcohol and hypo-chlorine sodium. In the medical-care industry, it
                             is widely used for preventing infection within hospitals by disinfecting medical equipment.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/sleep.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Promotes better,
                            more restful sleep</h1>
                        <p>Antioxidant-rich hydrogen water also helps to maintain an optimum pH balance.
                            You lose fluids at night during your hours of sleep, so being properly hydrated helps 
                            to counter this.</p>
                    </div>

                </div>
            </div>
            <div class="col-md-12 benefitList">
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/kidneyIssue.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Kidney Problems</h1>
                        <p>Dr Kerijiroo explains that for severe kidney failure, water intake has to be restricted. 
                            However whereas ordinary tap water "contains phosphate, calcium carbonate and the acidic 
                            ions which reduce the efficiency of the calcium ions". Alkaline Ionic water will
                             "strengthen the heart and increase urination."</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-5">
                        <img src="cImages/benefits/dentalHealth.jpg">
                    </div>
                    <div class="col-md-7">
                        <h1>Dental Health</h1>
                        <p>Alkaline water has a basic pH, 100 times more alkaline than spring water. Alkaline water neutralizes the acid produced
                             by bacteria, promoting stronger teeth and bones. It also stimulates saliva production, which also helps rid
                              the mouth of excess bacteria.</p>
                    </div>

                </div>
            </div>

        </div>
        <div class="container  paddingTopBottom">
            <div class="col-md-12">
                <div class="title-block">
                    <h1 class="poppins fontColorBlue">Are There Benefits of Owning a Water Ionizer System Over Buying Bottled Alkaline
                        Water?</h1>
                    <p class="marginTop10px">Throughout our website you will find hundreds of benefits of owning a water
                        ionizer. See a quick summary of the benefits of our alkaline, ionized water now. Then, check out
                        how the benefits of owning a unit far outweigh buying bottled alkaline water. Here is a quick
                        list of the key topics that will be covered here:</p>
                </div>
                <div class="col-md-9">
                    <p>The idea of bottling alkaline water is great! Let’s face it; we have all been at an airport or on
                        a trip where finding quality water is difficult. So, it’s no wonder that there are over a dozen
                        or more bottled alkaline water companies on the market already. Some of their claims to fame are
                        providing electrolytes and trace minerals in addition to alkalinity—again, these are all
                        wonderful attributes to have available in a pinch.</p>
                    <p>However, buying bottled alkaline water on a daily basis when you can get your alkaline water
                        fresh from a quality water ionizer is just plain silly. If the cost of owning an ionizer seems
                        high, consider a payment plan because the cost of owning an ionizer is far less than the cost of
                        even regular bottled water, let alone bottled alkaline water. Bottled alkaline water costs
                        around $2 to $3 a bottle, so buying even one bottle every day at $2 a bottle means you spend
                        $730 dollars in one year. That adds up to over $15,000 in 25 years! Besides its high cost, the
                        bottled stuff fails to deliver in many other ways as well.</p>
                </div>
                <div class="col-md-3">
                    <img src="cImages/benefits/bottlevsionizer.png">
                </div>
            </div>
        </div>

        <?php include('form.php')?>

    </section>
    <?php include("phpIncludes/footer.php") ?>

    <!-- /footer ends -->
    <!-- Core JavaScript Files -->
    <script src="header/js/jquery.min.js"></script>
    <script src="header/js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/functions.js"></script>


    <!-- Main Js -->
    <script src="header/js/main.js"></script>


    <script src="js/customScript.js"></script>


</body>

</html>