<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="customStyle.css">

</head>

<body id="page-top">
<?php include("phpIncludes/header.php") ?>

	<!-- /navbar ends -->

	
	<section id="content ">
        <div class="certificationHead headMargin paddingTopBottom">
			<div class="container">
				<div class="emphasis-title col-md-12 text-left ">
                    <h2 class="uppercase poppins">Certifications</h2>
                    <p class="lead topmargin-sm poppins widthMob40P">
						At Tyent we have earned all the highest certifications for our products confirming our superior quality. All our certifications have been received from external assessment agencies ratifying Tyent products meet the stringent international safety, environmental and health standards.
					</p>
				</div>
			</div>
		</div>
		<div class="paddingTopBottom">
			<div class="container ">
				<div class="col-md-2">
					<img class="img-responsive" src="cImages/certifications/bbb-rating.webp"  alt="bbb-rating">
				  </div>
				<div class="col-md-10 ">
					<p class="poppins vAlignMiddle">
					Tyent’s belief is that reputation is everything and should be well-guarded.  BBB ratings are an important factor to consider before you decide who to do business with.  Tyent has an A+ rating with the BBB, and after selling thousands of units, our customers continue to tell us how great our customer service is!
					</p>
				 </div>
			</div>
		</div>
		<div class="container ">
			<div class="otherCetificate">
				<h3>Tyent USA’s Other Certifications</h3>
				<div class="row">
					<div class="col-md-2"><img src="cImages/certifications/tm.webp" height="39" width="76" alt="Total Microorganism">
                        <p>Total Microorganism</p></div>
					<div class="col-md-2"> <img src="cImages/certifications/kgmp.webp" height="39" width="80" alt="Korea Good Manufacturing">
                        <p>Korea Good Manufacturing Practice</p></div>
					<div class="col-md-2"><img src="cImages/certifications/kwpic.webp" height="39" width="91" alt="Korea Water Purifier ">
                        <p>Korea Water Purifier Industry Cooperative</p></div>
					<div class="col-md-2"> <img src="cImages/certifications/kipo.webp" height="39" width="80" alt="Korean Intellectual Property">
                        <p>Korean Intellectual Property Office</p></div>
					<div class="col-md-2"> <img src="cImages/certifications/ukas-1.jpg" height="39" width="80" alt="UKAS Registered">
						<p>UKAS Registered ISO14001:2004</p></div>
					<div class="col-md-2"><img src="cImages/certifications/kfda.webp" height="39" width="76" alt="Korea Food &amp; Drug Administration">
                        <p>Korea Food &amp; Drug Administration</p></div>
				</div>
				<div class="row">
					<div class="col-md-2"><img src="cImages/certifications/ukas-2.jpg" height="39" width="80" alt="UKAS Registered">
                        <p>UKAS Registered ISO9001:2000</p></div>
					<div class="col-md-2"><img src="cImages/certifications/ce.webp" alt="CE Insignia">
                        <p>CE Insignia</p></div>
					<div class="col-md-2"><img src="cImages/certifications/cb.webp" alt="CB Scheme for Electrical Equipment">
                        <p>CB Scheme for Electrical Equipment</p></div>
					<div class="col-md-2"><img src="cImages/certifications/product-safety.webp" alt="product-safety">
                        <p>TUV</p></div>
					<div class="col-md-2"><img src="cImages/certifications/g.webp" alt="Good Design">
                        <p>Good Design</p></div>
					<div class="col-md-2"><img src="cImages/certifications/ul.webp" alt="UL Collaborative Standards">
                        <p>UL Collaborative Standards Development System</p></div>
				</div>
				<div class="row">
					<div class="col-md-2"> <img src="cImages/certifications/ansi.webp" alt="ANSI">
                        <p>ANSI Accredited Certification Program </p></div>
					<div class="col-md-2"><img src="cImages/certifications/iec.webp" alt="Independent Electrical Contractors">
                        <p>Independent Electrical Contractors</p></div>
					<div class="col-md-2"> <img src="cImages/certifications/innibiz.webp" alt="Innovative Business Company">
                        <p>Innovative Business Company Limited</p></div>
					<div class="col-md-2"><img src="cImages/certifications/bbb.webp" alt="Better Business Bureau">
                        <p>Better Business Bureau</p></div>
					<div class="col-md-2"><img src="cImages/certifications/gmp.webp" alt="Good Manufacturing Practice">
                        <p>Good Manufacturing Practice</p></div>
					<div class="col-md-2"><img src="cImages/certifications/permelic.webp" alt="Permelec Electrode">
                        <p>Permelec Electrode Ltd.</p></div>
				</div>
			</div>
		</div>
		<div class="container paddingBottom">
			<div class="certHolder">
				<h3>Certification details</h3>
                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/cert_ce.webp"class="img-responsive" alt="cert_ce">
                    </div>
                    <div class="certDetail col-md-10">
                        CE - The CE insignia is a mandatory conformity mark on many consumer products. The CE marking certifies that a product has met consumer safety, health or environmental requirements.
                    </div>
                </div>
                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/cert_cb.webp" class="img-responsive" alt="cert_cb">
                    </div>
                    <div class="certDetail col-md-10">
                        CB - The International Electrotechnical Committee for Conformity Testing to Standards (IECEE) established the CB Scheme for Electrical Equipment. It is an international system for acceptance of tests. The CB Scheme - or CB Agreement - is a multilateral agreement among participant countries and certification organizations utilizing a CB Test Report issued by one of these organizations.
                    </div>
                </div>
                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/cert_tuv.webp"class="img-responsive" alt="cert_tuv">
                    </div>
                    <div class="certDetail col-md-10">
                        TUV - This mark gives evidence that the essential safety requirements of the product have been fulfilled and the production has been inspected by the impartial organization TÜV Rheinland Product Safety. The TYPE APPROVED mark shows both the manufacturer and the consumer that the product meets the required safety standards.
                    </div>
                </div>
                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/cert_tm.webp"class="img-responsive" alt="cert_tm">
                    </div>
                    <div class="certDetail col-md-10">
                        Total Microorganism
                    </div>
                </div>
                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/cert_kfda.webp" class="img-responsive" alt="cert_kfda">
                    </div>
                    <div class="certDetail col-md-10">
                        Korea Food &amp; Drug Administration
                    </div>
                </div>
                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/cert_kgmp.webp"class="img-responsive" alt="cert_kgmp">
                    </div>
                    <div class="certDetail col-md-10">
                        Korea Good Manufacturing Practice
                    </div>
                </div>
                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/cert_kwpic.webp" class="img-responsive" alt="cert_kwpic">
                    </div>
                    <div class="certDetail">
                        Korea Water Purifier Industry Cooperative
                    </div>
                </div>
                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/cert_kipo.webp" class="img-responsive" alt="cert_kipo">
                    </div>
                    <div class="certDetail col-md-10">
                        Korean Intellectual Property Office
                    </div>
                </div>
                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/cert_ukas.webp" class="img-responsive" alt="cert_ukas">
                    </div>
                    <div class="certDetail col-md-10">
                        UKAS Registered ISO14001:2004
                    </div>
                </div>
                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/cert_ukas.webp" class="img-responsive" alt="cert_ukas">
                    </div>
                    <div class="certDetail col-md-10">
                        UKAS Registered ISO9001:2000
                    </div>
                </div>


                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/g.webp" class="img-responsive" alt="Good Design">
                    </div>
                    <div class="certDetail col-md-10">
                       Good Design
                    </div>
                </div>


                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/ul.webp" class="img-responsive" alt="ul">
                    </div>
                    <div class="certDetail col-md-10">
                        UL Collaborative Standards Development System
                    </div>
                </div>


                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/ansi.webp" class="img-responsive" alt="ansi">
                    </div>
                    <div class="certDetail col-md-10">
                        ANSI Accredited Certification Program 
                    </div>
                </div>


                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/iec.webp" class="img-responsive" alt="iec">
                    </div>
                    <div class="certDetail col-md-10">
                        Independent Electrical Contractors
                    </div>
                </div>


                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/innibiz.webp" class="img-responsive" alt="Innovative Business">
                    </div>
                    <div class="certDetail col-md-10">
                       Innovative Business Company Limited
                    </div>
                </div>


                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/bbb.webp" class="img-responsive" alt="Better Business Bureau">
                    </div>
                    <div class="certDetail col-md-10">
                        Better Business Bureau
                    </div>
                </div>


                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/gmp.webp" class="img-responsive" alt="Good Manufacturing Practice">
                    </div>
                    <div class="certDetail col-md-10">
                        Good Manufacturing Practice
                    </div>
                </div>


                <div class="certbox1 col-md-12">
                    <div class="certImg1 col-md-2">
                        <img src="cImages/certifications/permelic.webp" class="img-responsive" alt="Permelec Electrode">
                    </div>
                    <div class="certDetail col-md-10">
                        Permelec Electrode Ltd.
                    </div>
                </div>
            </div>
		</div>


	</section>
    <?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="header/js/jquery.min.js"></script>
	<script src="header/js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/functions.js"></script>

  
	<!-- Main Js -->
    <script src="header/js/main.js"></script>

	
	<script src="js/customScript.js"></script>
    <script>
    		$('#aboutLi').addClass('active');

</script>

</body>

</html>