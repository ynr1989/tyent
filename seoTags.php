<?php

$mandatoryTags = "<script>
gtag('config', 'AW-802308772/MkcECNbr7uEBEKSFyf4C', {
  'phone_conversion_number': '+91 9182414181'
});
</script>

";

$index = $mandatoryTags.'<title>Tyent India | World\'s Best Water Ionizer Technology </title>
<meta name="viewport" content="width=device-width" />
<meta name="description"  content="Discover the undeniable benefits of ionized hydration. Click now to learn more about our company, our products, and what sets our alkaline water machines apart." />
<meta name="keywords" content="tyent, ionizers, tyent India, Kangen water" />
<meta name="robots" content="INDEX,FOLLOW" />';

$aboutUs =  $mandatoryTags."<title>About Us | Tyent India</title>";

$applications =  $mandatoryTags."<title>Applications of Alkaline Water | Tyent India</title>";

$benefits =  $mandatoryTags."	<title>Benefits of Alkaline Water | Tyent India</title>";

$certifications =  $mandatoryTags."<title>Tyent Water Ionizer Certifications | Tyent India </title>";

$contact = $mandatoryTags."	<title>Contact us | Tyent India</title>";

$products =  $mandatoryTags."	<title>Products | Tyent India</title>";

$technology =  $mandatoryTags."<title>Tyent's Advanced Technology | Tyent India </title>";

$testimonials =  $mandatoryTags."	<title>Testimonials | Tyent India</title>";

$why_tyent =  $mandatoryTags."<title>Why Tyent | Tyent India </title>";

$whyWaterIonizer =  $mandatoryTags."	<title>Why should we use water ionizer | Advantages of water ionizer | Tyent India</title>";

$npmSeries =  $mandatoryTags."	<title>NMMP Water Ionizer - Tyent India</title>";

$uceSeries =  $mandatoryTags."	<title>Tyent UCE Water Ionizer | Tyent India</title>";

$electric_water_generator =  $mandatoryTags."	<title>Electrolyzed Sterilizing Water Generator | Tyent India</title>";

$tyentsoap =  $mandatoryTags."<title>Tyent TM-X Beauty Soap | Tyent India</title>";

$h2Hybrid = $mandatoryTags."<title>Tyent H2 Hybrid | Tyent India</title>";

$npm5marketing = $mandatoryTags."<title>NPM 5 plate | Tyent India</title>";

?>