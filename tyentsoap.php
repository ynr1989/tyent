<?php 

	$price = "2,10,000";
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="../../js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link href="../../header/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../../style.css" type="text/css" />
	<link rel="stylesheet" href="../../css/dark.css" type="text/css" />
	<link rel="stylesheet" href="../../css/animate.css" type="text/css" />
	<link rel="stylesheet" href="../../css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="../../css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="../../include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="../../include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="../../css/xzoom.css" media="all" />

	<link rel="stylesheet" type="text/css" href="../../customStyle.css">

</head>

<body id="page-top">
	<?php include("phpIncludes/header.php") ?>

	<!-- /navbar ends -->
	<div class="headMargin tyentSoapHeadImage">
	</div>
	<section id="content ">
		<div class="container paddingTopBottom ">
			<div class="col-md-12 ">
				<div class="col-md-4 productImages">
					<img class="xzoom" id="main_image" src="../../cImages/tyentsoap/tm-x-beauty-soap.jpg" xoriginal="cImages/tyentsoap/tm-x-beauty-soap.jpg">

				
				</div>
				<div class="col-md-8">
					<div class="text- pro-infor marginTop10px">

						<div class="pro-infor-title poppins">Tyent TM-X Beauty Soap</div>
						<p>Tyent TM-X Beauty Soap is made best ingredients which can maintain the delicate balance
							of oil and moisture that is required for healthy skin. This is made possible through a 600-hours 
							TM fermentation method, Resulting in a premium cleansing soap that is truly natural.</p>

						<table  class="pro-inforTable">

							<colgroup>
								<col width="135">
								<col width="">
							</colgroup>

							<tbody>
								<tr>
									<td class="bold1">Weight</td>
									<td>80g</td>
								</tr>

								<tr>
									<td colspan="2" height="13"></td>
								</tr>

								<tr>
									<td colspan="2" height="13"></td>
								</tr>
								<tr>
									<td class="bold1">Price</td>
									<td>₹ 900</td>
								</tr>


							</tbody>
						</table>

					</div>

				</div>
			</div>
			<div class="col-md-12">
				<div class="beautySection2" >
					<div class="fancy-title title-bottom-border textCenter">
						<h2>More About <span>TM-X Beauty Soap</span></h2>
					</div>
					<div class="product-feature-box">
											
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_01.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_02.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_03.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_04.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_05.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_06.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_07.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_08.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_09.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_10.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_11.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_12.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_13.jpg" alt=""></p>
						<p><img src="../../cImages/tyentsoap/TM-X_Beauty_Soap_Details_Page_14.jpg" alt=""></p>										</div>
				</div>
			</div>

		</div>

		<?php include('form.php')?>


	</section>
	<?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="../../header/js/jquery.min.js"></script>
	<script src="../../header/js/bootstrap.min.js"></script>
	<script src="../../js/plugins.js"></script>
	<script src="../../js/functions.js"></script>


	<!-- Main Js -->
	<script src="../../header/js/main.js"></script>

	<script type="text/javascript" src="../js/xzoom.min.js"></script>
	<script type="text/javascript" src="../js/sticky-sidebar.js"></script>

	<script src="../../js/customScript.js"></script>

	<script>
				$('#productLi').addClass('active');

		$(function () {
			$("#specifications").hide();
			$("#side-navigation").tabs({
				show: {
					effect: "fade",
					duration: 400
				}
			});
		});
		$(".xzoom, .xzoom-gallery").xzoom({
			tint: '#333',
			Xoffset: 15,
			defaultScale: -0.3
		});
	
		$(".mobileCloseButton").click(function () {
			$('.ui-tabs-tab').toggle();
			$(".mobileCloseButton i").toggleClass("icon-line-rewind");
			$(".mobileCloseButton").closest("ul.sidenav").toggleClass("transparentBG");
		});

	</script>

</body>

</html>