<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />
	<!-- Photography Specific Stylesheet -->
	<link rel="stylesheet" href="demos/photography/photography.css" type="text/css" />
	<link rel="stylesheet" href="demos/photography/css/photography-addons.css" type="text/css" />
	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="customStyle.css">
<style>
	body{
    background-color: #ffffff;
}
</style>
</head>

<body id="page-top">
<?php include("phpIncludes/header.php") ?>

	<!-- /navbar ends -->

	
	<section id="content ">
		<div class="container headMargin ">
			<div class="paddingTop">
				<div class="heading-block center noborder">
					<h3 class="fontColorBlue">Tyent India Testimonies </h3>
					<span class="strongText">Testimonies from our customers</span>
				</div>
				<ul class="testimonials-grid grid-3 clearfix">
					<li >
						<div class="testimonial">
							<div class="testi-content">
								<p>I was really skeptical about ionization, but it has made a big difference in my energy level. I got it just to use as a filter because the tap water in the city I am in is really gross and I was spending a small fortune on bottled water. I am glad I did.</p>
								<div class="testi-meta">
								Saveen,<span>Celebrity fitness trainer</span><span>The FITT studio, Bangalore.</span>
								</div>
							</div>
						</div>
					</li>
					<li >
						<div class="testimonial">
							<div class="testi-content">
								<p>I am sleeping better, much more energy, happier, edema gone, bad back better, and I feel 20 years younger.</p>
								<div class="testi-meta">
								DR. V. Surya narayana reddy
									<span>MBBS, D.C.H, FCIP</span>
									<span>Laxmi hospitals,Madinaguda</span>
								</div>
							</div>
						</div>
					</li>
		
					<li >
						<div class="testimonial">
							<div class="testi-content">
								<p>Drinking absolutely pure water has turned into a daily enjoyment for us. Technology, utility, operation, quality, post purchase services, from every angle Tyent is unmatched. Thank you so much for the wonderful customer service you provided…the company and customers are certainly in good hands.</p>
								<div class="testi-meta">
								M. Sravan Prasad
									<span>Chemistry Professor</span>
									<span>JNTUH</span>
								</div>
							</div>
						</div>
					</li>
		
					<li >
						<div class="testimonial">
							<div class="testi-content">
								<p>My association with Tyent has been for a no of years. The products I have used till date are of superior quality and long term durability. My complete satisfaction with their products propells me to further recommend it to other users</p>
								<div class="testi-meta">
								Sai Korrapati
									<span>Producer & Owner</span>
									<span>Vaaraahi Chalana Chitram</span>
								</div>
							</div>
						</div>
					</li>
		
					<li >
						<div class="testimonial">
							<div class="testi-content">
								<p>We are amazed to see the innovative water purifier range of Tyent.Tyent is sufficient for any organization to ensure the class and credibility of a water purifier to install for its employees. We cannot believe other purifiers after using Tyent purifier due to its best cost, quality and services.</p>
								<div class="testi-meta">
								Dr. Sandya rani reddy
									<span>Gynecologist</span>
								</div>
							</div>
						</div>
					</li>

					<li >
						<div class="testimonial">
							<div class="testi-content">
								<p>For certified purity, we just bank on Tyent Purifiers. The best technology, wide range, and modern aesthetics and so on everything is just superb. We’re proud to have Tyent at our hospital.</p>
								<div class="testi-meta">
								Dr.Naren shetty
									<span>Narayana netralaya</span>
								</div>
							</div>
						</div>
					</li>
		
					<li >
						<div class="testimonial">
							<div class="testi-content">
								<p>Thank you so much for the wonderful service…I would not only purchase from you again, but I will recommend you to everyone. You provided me with an ‘on-hands’ service, which I very much appreciated. No complaints whatsoever; nothing but compliments in dealing with you and your company.</p>
								<div class="testi-meta">
								DSP Shiva naidu
									<span>CBI</span>
								</div>
							</div>
						</div>
					</li>
		
					<li >
						<div class="testimonial">
							<div class="testi-content">
								<p>I am very pleased with my NMP 5 plate machine! Just after a few days, we are feeling better, more alert and energetic; I wish I would have done this sooner! I am very impressed with Tyent Water Ionizers!</p>
								<div class="testi-meta">
								Bharat kumar
									<span>Director at South central railway</span>
								</div>
							</div>
						</div>
					</li>
		
					<li >
						<div class="testimonial">
							<div class="testi-content">
								<p>I wash my face every morning with the pH4 water. I've notice a decrease in facial blemishes and ingrown hairs. As a black man I really appreciate that. Razor bumps are a serious thing. I'm so happy with my machine. This seems to be a great investment.</p>
								<div class="testi-meta">
								Dr. Geetha
									<span>Naturopathy</span>
								</div>
							</div>
						</div>
					</li>
		
		
				</ul>
			</div>
		
			<div class="content-wrap">
				<div class="container clearfix">
					<div class="heading-block center noborder">
						<h3>Tyent International Testimonies </h3>
						<span>Testimonies from A-Listed Celebrities, Athletes and Fitness experts</span>
					</div>

					<!-- Grid Items
					============================================= -->
					<div class="grid">
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/tim-tebow-goodlife.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
							
								<img src="cImages/testimonials/TIMTEBOW.jpeg" alt="img01" />
								<p class="gridP">Tim Tebow
									<br><span>American professional baseball player</span></p>
								<div class="description description-grid">
									<h3>Tim Tebow</h3>
									<p>American professional baseball player - Recommends Tyent to use GoodLife bottle.</span></p>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/RogerDaltrey.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="cImages/testimonials/RogerDaltrey.jpg" alt="img02" />
								<p class="gridP">Roger Daltrey
									<br><span>Founder of the rock band "The Who"</span></p>
								<div class="description description-grid">
									<h3>Roger Daltrey</h3>
									<p>While on tour in October 2009, I became aware of the supposed benefits of high pH water. Since I suffer from mild Psoriasis, I have been using high pH water from a Tyent machine for the last 6 months and my general well being has improved. There has also been a noticeable improvement in the condition of my skin. I, now, take a Tyent water purifier on the road with me. It's easy to attach to any water source,convenient to transport and needs no chemicals for cleaning.</p>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/Tony_Robbins.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="cImages/testimonials/Tony_Robbins.jpg" alt="img03" />
								<p class="gridP">Anthony Robbins
									<br><span>Author, Peak Performance Coach</span></p>
								<div class="description description-grid">
									<h3>Anthony Robbins</h3>
									<p>Alkalize your body and live a healthier, more energized, and ultimately more fulfilling life. Our acid-alkaline balance is a baseline determinant of our physical health. When you break your old eating patterns, you will find yourself getting back to the real you, filled with the vitality and energy that your desire and deserve.
									</p>
									
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/TJWARD.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="cImages/testimonials/TJWARD.jpg" alt="img04" />
								<p class="gridP">TJ Ward<br><span>Safety, Denver Broncos</span></p>
								<div class="description description-grid">
									<h3>TJ Ward</h3>
									<p>“With Tyent Water, my recovery time is much faster and my energy levels have noticeably increased.”

										Wow, we think that is really incredible. But TJ wasn’t finished telling about the benefits he received from our water and went on to share:
										
										“I do not feel weighed down when I drink Tyent Water™ like I do with I drink regular bottled water or sports drinks. Plus, I feel more hydrated.”</p>
									
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/MADELINEZIMA.jpeg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="cImages/testimonials/MADELINEZIMA.jpeg" alt="img05" />
								<p class="gridP">MADELINE ZIMA<br><span>American actress</span></p>
								<div class="description description-grid">
									<h3>MADELINE ZIMA</h3>
									<p>The Tyent 9090 is Awesome, nice! Cool! It’s so great!.

										Okay Madeline, we actually feel the same way. Then, she summed it all up by telling us:
										
										“The Tyent 9090 Water Ionizer is amazing!”</p>
									
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/terryfactor.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="cImages/testimonials/terryfactor.jpg" alt="img06" />
								<p class="gridP">TERRY FATOR
									<br><span>American ventriloquist
									</span></p>

								<div class="description description-grid">
									<h3>TERRY FATOR</h3>
									<p>Since I got the Tyent UCE-11 ionizer I have noticed a marked difference. My throat feels much more lubricated and I have my full vocal range back!

										I recommend it to anyone who need to keep their voice and body hydrated.</p>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/grantBowler.jpeg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="cImages/testimonials/grantBowler.jpeg" alt="img07" />
								<p class="gridP">GRANT BOWLER<br><span>Australian-New Zealand actor
								</span></p>
								<div class="description description-grid">
									<h3>GRANT BOWLER</h3>
									<p>“Ah! This portable alkalizer is a must-have! I could definitely use the TYgo at home.”</p>
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/ClaraHughes2-1.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="cImages/testimonials/ClaraHughes2-1.jpg" alt="img08" />
								<p class="gridP">Clara Hughes
									<br><span>Canadian cyclist
									</span></p>

								<div class="description description-grid">
									<h3>Clara Hughes</h3>
									<p>After a week of trying Tyent ionized water out on myself, I could not believe the difference I felt after the extreme efforts of training and effort.</p>

								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/yannick-bisson.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="cImages/testimonials/yannick-bisson.jpg" alt="img09" />
								<p class="gridP">YANNICK BISSON
									<br><span>Canadian film actor
									</span></p>
								<div class="description description-grid">
									<h3>YANNICK BISSON</h3>
									<p>Since having the Tyent Water Ionizer installed in our brand new home six months ago everybody keeps asking me “what I’m doing differently, you’re like an alien who never ages” and I tell them it’s all in my water!”</p>
									
								</div>
							</a>
						</div>
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/JONATHANLIPNICKI.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="cImages/testimonials/JONATHANLIPNICKI.jpg" alt="img10" />
								<p class="gridP">JONATHAN LIPNICKI
									<br><span>American actor
									</span></p>
								<div class="description description-grid">
									<h3>Mother's Love</h3>
									<p>TYgo is great for working out</p>
									
								</div>
							</a>
						</div>
						
						<div class="grid-item" data-size="1280x853">
							<a href="cImages/testimonials/daveGreen.jpg" class="img-wrap img-grayscale" data-animate="fadeInUp" onclick="return false;">
								<img src="cImages/testimonials/daveGreen.jpg" alt="img19" />
								<p class="gridP">Dave Greene
									<br><span> Fitness Club CEO
									</span></p>
								<div class="description description-grid">
									<h3>Dave Greene</h3>
									<p>yent USA alkaline water ionizer is used by athletes in health clubs and on Olympic teams!</p>
									<div class="details">
										<ul>
											<li><i class="icon icon-line-camera"></i><span>Canon PowerShot S95</span></li>
											<li><i class="icon icon-line-eye"></i><span>22.5mm</span></li>
											<li><i class="icon icon-line-printer"></i><span>&fnof;/5.6</span></li>
											<li><i class="icon icon-line-cog"></i><span>1/1000</span></li>
											<li><i class="icon icon-line-sun"></i><span>80</span></li>
										</ul>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- /grid -->
					<div class="preview">
						<button class="action action-close"><i class="icon-line2-close"></i><span class="text-hidden">Close</span></button>
						<div class="description description-preview"></div>
					</div>
				</div>

		</div>
		
	

	</section>

	<?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="header/js/jquery.min.js"></script>
	<script src="header/js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/functions.js"></script>

  
	<!-- Main Js -->
    <script src="header/js/main.js"></script>	
	<script src="js/customScript.js"></script>

	<script src="demos/photography/js/photography-addons.js"></script>
	<script>
		(function() {
			var support = { transitions: Modernizr.csstransitions },
				// transition end event name
				transEndEventNames = { 'WebkitTransition': 'webkitTransitionEnd', 'MozTransition': 'transitionend', 'OTransition': 'oTransitionEnd', 'msTransition': 'MSTransitionEnd', 'transition': 'transitionend' },
				transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
				onEndTransition = function( el, callback ) {
					var onEndCallbackFn = function( ev ) {
						if( support.transitions ) {
							if( ev.target != this ) return;
							this.removeEventListener( transEndEventName, onEndCallbackFn );
						}
						if( callback && typeof callback === 'function' ) { callback.call(this); }
					};
					if( support.transitions ) {
						el.addEventListener( transEndEventName, onEndCallbackFn );
					}
					else {
						onEndCallbackFn();
					}
				};

			new GridFx(document.querySelector('.grid'), {
				imgPosition : {
					x : -0.5,
					y : 1
				},
				onOpenItem : function(instance, item) {
					instance.items.forEach(function(el) {
						if(item != el) {
							var delay = Math.floor(Math.random() * 50);
							el.style.WebkitTransition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), -webkit-transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
							el.style.transition = 'opacity .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1), transform .5s ' + delay + 'ms cubic-bezier(.7,0,.3,1)';
							el.style.WebkitTransform = 'scale3d(0.1,0.1,1)';
							el.style.transform = 'scale3d(0.1,0.1,1)';
							el.style.opacity = 0;
						}
					});
				},
				onCloseItem : function(instance, item) {
					instance.items.forEach(function(el) {
						if(item != el) {
							el.style.WebkitTransition = 'opacity .4s, -webkit-transform .4s';
							el.style.transition = 'opacity .4s, transform .4s';
							el.style.WebkitTransform = 'scale3d(1,1,1)';
							el.style.transform = 'scale3d(1,1,1)';
							el.style.opacity = 1;

							onEndTransition(el, function() {
								el.style.transition = 'none';
								el.style.WebkitTransform = 'none';
							});
						}
					});
				}
			});
		})();

	</script>
	<script>
    		$('#aboutLi').addClass('active');

</script>
</body>

</html>