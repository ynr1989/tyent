<?php 

	$price = "2,10,000";
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="../../js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link href="../../header/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../../style.css" type="text/css" />
	<link rel="stylesheet" href="../../css/dark.css" type="text/css" />
	<link rel="stylesheet" href="../../css/animate.css" type="text/css" />
	<link rel="stylesheet" href="../../css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="../../css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="../../include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../../include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="../../include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="../../css/xzoom.css" media="all" />

	<link rel="stylesheet" type="text/css" href="../../customStyle.css">

</head>

<body id="page-top">
	<?php include("phpIncludes/header.php") ?>

	<!-- /navbar ends -->
	<div class="headMargin eswgHeadImage">
		<div class="container ">
			<div class="col-md-8">
				<h1 class="">Need Industrial Grade Sanitation?</h1>
				<h2 class="">Check out the Tyent SHG-1</h2>
				<ul class="fa-ul h4" style="list-style-type: none;">
					<li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>100% Natural ingredients</li>
					<li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>99.9% Sterilization power</li>
					<li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>Sterilization Ingredients of FDA GRAS Class</li>
					<li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>*GRAS(Generally Recognized As Safe, CITE: 21CFR178.1010)</li>
					<li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span><a href="https://www.gpc.center/kfda_/650" target="_blank">KFDA</a> Approved for Food Safety</li>
					<li><span class="fa-li"><i class="fas fa-check-square" aria-hidden="true"></i></span>Novel Coronavirus(COVID-19) disinfectant suggested by <a href="https://www.who.int/health-topics/coronavirus" target="_blank">WHO</a></li>
				</ul>
			</div>
			<div class="col-md-4">
				<img src="../../cImages/prod/eswg5.jpg" alt="Tyent SHG-1">
			</div>
		</div>
	</div>
	<section id="content ">
		<div class="container paddingTop">
			
			<div class="col-md-12 ">
				<div class="col-md-4 productImages">
					<img class="xzoom" id="main_image" src="../../cImages/prod/eswg.png" xoriginal="cImages/prod/eswg.png">

					<div class="xzoom-thumbs marginTop50px">
						<a href="cImages/prod/eswg.png">
							<img class="xzoom-gallery" width="60" src="../../cImages/prod/eswg.png">
						</a>
						<a href="cImages/prod/eswg4.jpg">
							<img class="xzoom-gallery" width="60" src="../../cImages/prod/eswg4.jpg">
						</a>
				</div>
				</div>
				<div class="col-md-8">
					<div class=" pro-infor marginTop10px">
	
						<div class="pro-infor-title">TIE-N15WR</div>
						
						<table class="table">
					
							
							<tbody><tr>
								<td class="bold">Product</td>
								<td>Sterilizing water generator</td>
							</tr>
							<tr>
								<td class="bold">Model</td>
								<td>TIE-N15WR</td>
							</tr>
							<tr>
								<td class="bold">Size(mm)</td>
								<td>545x225x470</td>
							</tr>
							<tr valign="top">
								<td class="bold">Purpose of use</td>
								<td>disinfection and sterilization of food, utensils, and other apparatus</td>
							</tr>
							<tr valign="top">
								<td class="bold">Places to use</td>
								<td>School, military, hospital, hotel restaurant, cafe</td>
							</tr>
							
						</tbody></table>
						
					</div>

				</div>
			</div>

		</div>

		<div class="">
			<div class="container paddingMobileLR5px">
	
				<div class="marginTop30px">
				
					<div class="productDivDesc" id="features">
						<div class="container clearfix paddingBottom">

							<div id="side-navigation" class="ui-tabs ui-corner-all ui-widget ui-widget-content ">

								<div class="col-md-3 nobottommargin sidebar">

									<ul class="sidenav ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header sidebar__inner"
										role="tablist">
										<button class="button button-3d button-rounded button-teal mobileCloseButton"><i
												class="icon-remove"></i></button>

										<li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" role="tab"
											tabindex="-1" aria-controls="snav-content1" aria-labelledby="ui-id-1"
											aria-selected="false" aria-expanded="false"><a href="#snav-content1"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1"><i
													class="icon-screen"></i>Generative principle & purpose of use</a></li>
										<li role="tab" tabindex="-1"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content2" aria-labelledby="ui-id-2"
											aria-selected="false" aria-expanded="false"><a href="#snav-content2"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2"><i
													class="icon-magic"></i>Product formation<i class="icon-chevron-"></i></a>
										</li>
										<li role="tab" tabindex="-1"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content3" aria-labelledby="ui-id-3"
											aria-selected="false" aria-expanded="false"><a href="#snav-content3"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-3"><i
													class="icon-star3"></i>Main function<i class="icon-chevron-"></i></a>
										</li>
										<li role="tab" tabindex="0"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content4" aria-labelledby="ui-id-4" aria-selected="true"
											aria-expanded="true"><a href="#snav-content4" role="presentation"
												tabindex="-1" class="ui-tabs-anchor" id="ui-id-4"><i
													class="icon-gift"></i>Installation  <i class="icon-chevron-"></i></a></li>
									
									</ul>
								</div>

								<div class="col-md-9 col_last nobottommargin">

									<div id="snav-content1" aria-labelledby="ui-id-1" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div id="tabPosition1">

											<div class="tab-con-box pro-img-bg col-md-12" >
											
												<div class="tab-box-title">
													<div class="tab-title">01 Generative principle &amp; purpose of use</div>
												</div>
												
												<div class="tab-product-title" style="margin-bottom:59px;">
													Gushing sterilized water just from <span class="fblue fw600">water and salt!</span>
												</div>
												
												<div class="col-md-8">
													<ul>
												
														<li>
															<div class="fpx18 fw600 clearFix">
																<div class="text-left" style="margin-top:7px;color:#363636;"><img src="../../cImages/eswg/infor_icon1.gif" alt="">Convenient hygiene management</div>
															</div>
															<div class="fpx14 mt20 line15">
															- Process that does not require dilution increases the production efficiency. 
															- The automatic consecutive system allows you to use the water like a regular tap water. 
															- The one-touch control provides easy and safe use. 
															</div>
														</li>
														
														<li class="mt40">
															<div class="fpx18 fw600 clearFix">
																<div class="text-left" style="margin-top:7px;color:#363636;"><img src="../../cImages/eswg/infor_icon2.gif" alt="">Clean quality assurance</div>
															</div>
															<div class="fpx14 mt20 line15">
															- Quick sterilization and minimal residue is a great advantage. 
															- Ideal for food sterilization without affecting the taste and the smell of the food.
															</div>
														</li>	
														
														<li class="mt40">
															<div class="fpx18 fw600 clearFix">
																<div class="text-left" style="margin-top:7px;color:#363636;"><img src="../../cImages/eswg/infor_icon3.gif" alt="">Safe working environment</div>
															</div>
															<div class="fpx14 mt20 line15">
															The electrolysis of water and salt does not affect the environment. 
															When the residue is contacted with organic matter after the use, 
															the active chlorine decreases and naturally dissolute. 
															</div>
														</li>	
														
													</ul>
												
												</div>
											</div>
											
											<div class="tab-con-box col-md-12 marginTop30px">
												
												<div class="tab-box-title">Generative principle</div>
												
												<div class="clearFix mt50">
													
													<div class="col-md-3 fw600 fpx22">Sterilized water producer</div>
													<div class="col-md-9 " >
														<div class="fpx16 fblue2 fw600">Is a device that electrolyzes water and small quantity of salt to produce an environment friendly sterilized water (sodium hypochlorite)</div>
														<div class="l-s05 line15 mt30">
														The sterilized water produced is popularly used in various places and areas that require sterilization and
														disinfection as it has strong sterilizing power especially in virus and germs found in the hospital and the 
														resistant bacteria. This is an innovative HACCP product that considers both sanitation and safety and
														solves the problem of hard handling of the original chemical based toxic sterilizer, dilution, and ruining 
														the environment. 
														</div>
													</div>
													
												</div>
												
												<div class="imgWrap mt45"><img src="../../cImages/eswg/pro13_1_1_img01.gif" alt="Sterilized water producer"></div>
												
												
											</div>
											
											<div class="tab-con-box">
											
												<div class="tab-box-title">Purpose of use</div>
												<div class="imgWrap mt50"><img src="../../cImages/eswg/pro13_1_1_text01.gif" alt="Let the Sterilized Water Producer take care of it all!" ></div>
												<div class="clearFix mt40 l-s05">	
												
													<div class="col-md-12">
														<div class="clearFix">
															<div class="col-md-3">
																<img src="../../cImages/eswg/pro13_1_1_img02.jpg" alt="Food additives(sterilizer)- For food">
															</div>
															<div class="col-md-9" style="margin-top:26px;">
																<div class="fpx18 fw600" style="color:#363636;">Food additives(sterilizer)- For food</div>
																<div class="line14" style="margin-top:22px;">
																Must be used on the fruits, vegetables and
																other foods labeled “food additive”, and 
																must be removed completely before 
																cooking the food.
																</div>
															</div>
															
														</div>
													</div>
													
													<div class="col-md-12" >
														<div class="clearFix">
															<div class="col-md-3">
																<img src="../../cImages/eswg/pro13_1_1_img03.jpg" alt="Disinfectants for furniture and others - For apparatus">
															</div>
															<div class="col-md-9" style="margin-top:26px;">
																
																<div class="fpx18 fw600" style="color:#363636;">Disinfectants for furniture and others - For apparatus</div>
																<div class="line14" style="margin-top:22px;">
																Must be used on the products that are 
																labeled as “disinfectants for furniture and 
																others” and must follow the standards of the 
																use on each products  
																</div>
															</div>
														
														</div>
													</div>	
												
												</div>	
												
												<div class="imgWrap mt50"><img src="../../cImages/eswg/pro13_1_1_img04.jpg" alt="How to prevent food poisoning"></div>
												<div class="clearFix mt50">
													
													<div class="col-md-6 fw600">
														<div class="fblue2 fpx22">How to prevent food poisoning</div>
														<div class="fpx18 line14" style="margin-top:13px;">
														Strict disinfecting and sterilizing of the 
														food poisoning causative bacteria. 
														</div>
													</div>
													
													<div class="col-md-6 line15" >
													Average amount of microorganisms found in raw vegetables are 103~109CFU/g. 
													Common bacteria and colon bacillus on the surface of fruit and vegetable cannot be 
													completely removed by tap water. The harmful microorganisms must be removed from 
													the cooking utensils after their use with proper sterilizing method. 
													<strong>Disinfection and sterilization is not a choice, but a must!</strong>
													</div>
													
												</div>
												
												<table class="table pH_table fpx14 center mt50" style="width:100%">
													
													<colgroup>
														<col width="490">			
														<col width="">
													</colgroup>
													
													<tbody><tr>
														<td class="pH_title pH_line_r">Category</td>
														<td class="pH_title">Active chlorine concentration</td>
													</tr>
													
													<tr>
														<td class="pH_line_r">Fruits, vegetables, and fresh foods</td>
														<td>Below 100ppm</td>
													</tr>
													
													<tr>
														<td class="pH_line_r">Cooking utensils and plates</td>
														<td>Below 200ppm</td>
													</tr>
													
													<tr>
														<td class="pH_line_r">Order removal of contaminated area</td>
														<td>Below 500ppm</td>
													</tr>
													
												</tbody></table>
												
											</div>
											
											</div>
									</div>

									<div id="snav-content2" aria-labelledby="ui-id-2" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div id="tabPosition2">

											<div class="tab-con-box">
											
												<div class="tab-box-title">
													<div class="tab-title">02 Product formation</div>
												</div>
												
												<div  class="row productFormationDiv" style="margin-top:58px;">
												
													<div class="col-md-3 imgWrap"  ><img src="../../cImages/eswg/pro13_1_1_img13.jpg" alt="Product formation"></div>
													<div class="col-md-9">
														<div class="pro-feature-title"><img src="../../cImages/eswg/pro_view_dot.gif" alt=""><span>ONE TOUCH</span> automatic flush</div>
														<div class="pro-feature-text">The touch sense system allows you to easily get the water going or stop. You can see the status of the button you chose. </div>
														<div class="pro-feature-title mt47"><img src="../../cImages/eswg/pro_view_dot.gif" alt=""><span>Individual button &amp; live status display</span></div>
														<div class="pro-feature-text">You can easily identify the different functions and see the live status of the product that is running. </div>
													</div>
												
												</div>
																								
												<div class="row productFormationDiv" >
												
													<div class="col-md-3 imgWrap"  ><img src="../../cImages/eswg/pro13_1_1_img06.jpg" alt="Product formation"></div>
													<div class="col-md-9"  >
														<div class="pro-feature-title"><img src="../../cImages/eswg/pro_view_dot.gif" alt="">Production of different concentrations of active chlorine</div>
														<div class="pro-feature-text">The product automatically produces the sterilized water based on its use (100/200/500ppm)</div>	
													</div>
												
												</div>
												
												
												<div class="row productFormationDiv"  >
												
													<div class="col-md-3 imgWrap"  ><img src="../../cImages/eswg/pro13_1_1_img07.jpg" alt="Product formation"></div>
													<div class="col-md-9"  >
														<div class="pro-feature-title"><img src="../../cImages/eswg/pro_view_dot.gif" alt=""><span>Simplified</span> cleaning function</div>
														<div class="pro-feature-text">After outflow, automatic internal cleaning removes residual 
														 chlorine to increase durability of the product.  
														 When cleaning is required, you can perform the cleaning function 
														 manually by pressing the cleaning button as a one-touch system.
														</div>	
													</div>
												
												</div>
												
												
												<div  class="row productFormationDiv">
												
													<div class="col-md-3 imgWrap"  ><img src="../../cImages/eswg/pro13_1_1_img15.jpg" alt="Product formation"></div>
													<div class="col-md-9"  >
														<div class="pro-feature-title"><img src="../../cImages/eswg/pro_view_dot.gif" alt="">Solution of Salt solidification problem</div>
														<div class="pro-feature-text">A salt solidification phenomenon was solved by the structure of the salt tank 
														 which strengthened circulation function.
														</div>	
													</div>
												
												</div>
												
												
												<div  class="row productFormationDiv">
												
													<div class="col-md-3 imgWrap"  ><img src="../../cImages/eswg/pro13_1_1_img16.jpg" alt="Product formation"></div>
													<div class="col-md-9"  >
														<div class="pro-feature-title"><img src="../../cImages/eswg/pro_view_dot.gif" alt=""><span>SMPS</span> Power Supply method</div>
														<div class="pro-feature-text">It supplies stable power by SMPS(Switching Mode Power Supply) power supply method.
														</div>	
													</div>
												
												</div>
												
												<div  class="row productFormationDiv">
												
													<div class="col-md-3 imgWrap"  ><img src="../../cImages/eswg/pro13_1_1_img14.jpg" alt=""></div>
													<div class="col-md-9" style="margin-top:24px;">
														<div class="pro-feature-title"><img src="../../cImages/eswg/pro_view_dot.gif" alt="">Wall-mounted product for effective use of the space</div>
														<div class="pro-feature-text">The wall-mounted design provides effective use of the space. </div>	
													</div>
												
												</div>
											
											</div>
											
											</div>

									</div>

									<div id="snav-content3" aria-labelledby="ui-id-3" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div id="tabPosition3">

											<div class="tab-con-box">
												
												<div class="tab-box-title">
													<div class="tab-title">03 Main function</div>
												</div>
												
												<div class="imgWrap" style="border-bottom:1px solid #ececec;"><img src="../../cImages/eswg/pro13_1_1_img10.jpg" alt="Main function"></div>
												
												<div class="clearFix mt50">
												
													<div class="col-md-6 imgWrap"><img src="../../cImages/eswg/pro13_1_1_img11.jpg" alt="Controls and display"></div>
													<div class="col-md-6 ">
														<div class="pro-feature-title"><img src="../../cImages/eswg/pro_view_dot.gif" alt="">Controls and display</div>
														<table class="buttonTable table">
															<tbody><tr>
																<td class="bg_sky">No</td>
																 
																<td class="bg_sky">NAME</td>
																 
																<td class="bg_sky">No</td>
																 
																<td class="bg_sky">NAME</td>
															</tr>
															
															<tr>
																<td class="bold">01</td>
																 
																<td>PPM (Chlorine concentration) display</td>
																 
																<td class="bold">06</td>
																 
																<td>200ppm water flow</td>
															</tr>
															
															<tr>
																<td class="bold">02</td>
																 
																<td>Flow rate display</td>
																 
																<td class="bold">07</td>
																 
																<td>500ppm water flow</td>
															</tr>
															
															<tr>
																<td class="bold">03</td>
																 
																<td>Voltage display</td>
																 
																<td class="bold">08</td>
																 
																<td>Cleaning button</td>
															</tr>
															
															<tr>
																<td class="bold">04</td>
																 
																<td>Current display</td>
																 
																<td class="bold">09</td>
																 
																<td>System setup button</td>
															</tr>
															
															<tr>
																<td class="bold">05</td>
																 
																<td>100ppm water flow</td>
																 
																<td class="bold">10</td>
																 
																<td>Information for device use time</td>
															</tr>
															
													
															
														</tbody></table>
													</div>
												
												</div>
												
											</div>
											
											</div>
									</div>

									<div id="snav-content4" aria-labelledby="ui-id-4" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="false">
										<div id="tabPosition4">

											<div class="tab-con-box" style="padding-bottom:10px; ">
												
												<div class="tab-box-title">
													<div class="tab-title">04 Installation</div>
												</div>
												
												<div class="imgWrap" style="margin-top:60px;"><img src="../../cImages/eswg/pro13_1_1_img12.jpg" alt="Installation"></div>
												
											</div>
											
											
											</div>
									</div>
								</div>

							</div>


						</div>
					</div>
				</div>

			</div>
		</div>

		<?php include('form.php')?>


	</section>
	<?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="../../header/js/jquery.min.js"></script>
	<script src="../../header/js/bootstrap.min.js"></script>
	<script src="../../js/plugins.js"></script>
	<script src="../../js/functions.js"></script>


	<!-- Main Js -->
	<script src="../../header/js/main.js"></script>

	<script type="text/javascript" src="../../js/xzoom.min.js"></script>
	<script type="text/javascript" src="../../js/sticky-sidebar.js"></script>

	<script src="../../js/customScript.js"></script>

	<script>
		$(function () {
			$("#specifications").hide();
			$("#side-navigation").tabs({
				show: {
					effect: "fade",
					duration: 400
				}
			});
		});
		$(".xzoom, .xzoom-gallery").xzoom({
			tint: '#333',
			Xoffset: 15,
			defaultScale: -0.3
		});
		var a = new StickySidebar('.sidebar', {
			topSpacing: 110,
			bottomSpacing: 20,
			containerSelector: '#features',
			innerWrapperSelector: '.sidebar__inner'
		});
		$('#productLi').addClass('active');
		$(".mobileCloseButton").click(function () {
			$('.ui-tabs-tab').toggle();
			$(".mobileCloseButton i").toggleClass("icon-line-rewind");
			$(".mobileCloseButton").closest("ul.sidenav").toggleClass("transparentBG");
		});
	</script>
</body>

</html>