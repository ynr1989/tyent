<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="whytyent/font/flaticon.css">
	<link rel="stylesheet" type="text/css" href="customStyle.css">

</head>

<body id="page-top">
<?php include("phpIncludes/header.php") ?>

	<!-- /navbar ends -->

	
	<section id="content">
        <div class="container headMargin paddingBottom">
            <div class="heading-block topmargin-lg center">
                <h2>Why Tyent</h2>
			</div>
			<div class="row course-categories clearfix mb-4 cardNHolder">
				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="1">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(251,51,100,0.8);">
							<span><i class="flaticon-birth-control"></i>Large Plate Size</span>
						</a>
					</div>
				</div>

				<div class="col-lg-3 col-sm-3 col-6 mt-4  cardN" id="2">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(29,74,103,0.8);">
							<span><i class="flaticon-mesh"></i>Solid/Mesh Hybrid Plate</span>
						</a>
					</div>
				</div>


				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="3">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(50,71,66,0.8);">
							<span><i class="flaticon-thunderbolt"></i>SMPS PLUS® Power Supply</span>
						</a>
					</div>
				</div>

				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="4">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(70,58,69,0.8);">
							<span><i class="flaticon-glass"></i>Super Water Capability</span>
						</a>
					</div>
				</div>

				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="5">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(252,108,52,0.8);">
							<span><i class="flaticon-filter"></i>Extreme Filtration Capacity</span>
						</a>
					</div>
				</div>

				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="6">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(158,108,87,0.8);">
							<span><i class="icon-leaf"></i> Chemical and Lead Free</span>
						</a>
					</div>
				</div>

				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="7">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(164,108,119,0.85);">
							<span><i class="icon-line-sun"></i>PAST Cleaning System</span>
						</a>
					</div>
				</div>

				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="8">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(80,167,159,0.8);">
							<span><i class="flaticon-policy"></i>Certifications</span>
						</a>
					</div>
				</div>

				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="9">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(23,116,234,0.8);">
							<span><i class="flaticon-approved"></i>KFDA, GMP, UL, CSA Approved</span>
						</a>
					</div>
				</div>

				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="10">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(108,156,148,0.85);">
							<span><i class="flaticon-star"></i>BBB A+ Rating</span>
						</a>
					</div>
				</div>

				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="11">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(227,141,66,0.8);">
							<span><i class="icon-ioxhost"></i>Highest Antioxidant Production</span>
						</a>
					</div>
				</div>

				<div class="col-lg-3 col-sm-3 col-6 mt-4 cardN" id="12">
					<div class="card hover-effect">
						<a href="#" class="card-img-overlay rounded p-0" style="background-color: rgba(39,103,240,0.8);">
							<span><i class="icon-globe1"></i>3 Years Warranty</span>
						</a>
					</div>
				</div>

			</div>
        </div>

	</section>
	<?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="header/js/jquery.min.js"></script>
	<script src="header/js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/functions.js"></script>

  
	<!-- Main Js -->
    <script src="header/js/main.js"></script>
 
	
	<script src="js/customScript.js"></script>
	<script>$('#aboutLi').addClass('active');</script>
	<script>
	$(document).ready(function() {
  
  var expanded = 0;

  
  $( ".cardN" ).click(function(e) {
	
	e.preventDefault();
	   
	var clicked = $(this).attr("id");
	var style=  $(this).find("a").attr('style');

	var titles=[ "Largest Plate Size in the Industry","Solid/Mesh Hybrid Plate","SMPS PLUS® Power Supply",
	"Super Water Capability","Extreme Filtration Capacity","Chemical and Lead Free","PAST Cleaning System",
	"Certifications","Korean Food and Drug Administration (KFDA), GMP, UL, CSA Approved","Better Business Bureau A+ Rating",
	"Highest Antioxidant Production Levels","3 Years Warranty","Doorstep service"
	]
	var images = [];
	var desc = [
		"Tyent ionizer's have the largest, thickest and highest-quality plates in the industry. Larger plates mean greater alkaline and acidic water capabilities and stronger pH, which makes Tyent the leader in producing the strongest negative ORP available on the market. Our medical-grade, strengthened titanium plates are dipped multiple times in the highest quality platinum. After our plates are dipped multiple times in platinum, they are baked to ensure quality and longevity",
		
		"All Tyent Water Ionizers come standard with medical-grade Solid/Mesh Hybrid plate technology. Solid/Mesh Hybrid plates offer the durability of solid plates with the extensive surface area of mesh plates. Our engineers begin the Tyent plate-making process with the thickest and longest plates in the industry, and then they incorporate cutting-edge electrolysis distribution methods for maximum electrical conductivity. </p><p>Most ionizers have plates that are either solid or mesh. Tyent engineers were not satisfied with either design. Here’s why: Solid plates offer strength but lack premium electrical distribution. Since electricity is drawn to edges (which is why a lightning rod has a pointed edge) a solid plate’s electrical current will only be transmitted around the outer edge of the plate. The edges are key, which is why mesh plates seem ideal; but since they lack durability, they do not meet Tyent’s high standards. Since neither of the available plate styles was sufficient, our Tyent engineers developed the cutting-edge Tyent Solid/Mesh Hybrid plate. The Tyent Solid/Mesh Hybrid plate was engineered according to Faraday’s electrolysis distribution methods.\
		</p><p>whytyentTo make a Tyent Solid/Mesh Hybrid plate, small holes are first drilled into our strengthened solid plate to dramatically increase the amount of surface area available for electrolysis. Next, our plate is dipped multiple times in platinum. Our Tyent Solid/Mesh Hybrid plate truly offers the best of both worlds. </p><p>\
		Tyent is the only water ionizer company in the world using this highly conductive hybrid plate design. Tyent’s Solid/Mesh Hybrid plate will never break, crumble or leach. Since every Tyent Water Ionizer is built with the very best components and most advanced features, you will never be asked to pay more for our premium plates. Plus, we are able to offer 15 years warranty because of the strength and durability of plates.",

		"Three types of power supplies are used in water ionizer systems\
		<ul><li>The oldest technology: power supply units (PSUs) with a heavy transformer\
		</li><li>The newer technology: Standard switched managed power supply (SMPS)\
		</li><li>The newest most advanced technology: SMPS Plus\
		</li></ul>Here is a brief description of the three types. The older linear power supply units, known as PSUs with heavy transformers, are hefty and prone to overheating. Due to their simplicity, linear PSUs do not allow for voltage adjustability, and the technology behind linear PSUs was developed in the early 1900s. This ancient circuit design is less efficient than newer technology. Some ionizer companies still use PSUs because they are less expensive.\
		The newer technology is the SMPS, and it is the same technology that is used in flat-panel televisions, laptop computers, and other modern electronic devices. Compared to linear PSUs, SMPS units are lighter, smaller and more efficient overall.Tyent engineers went back to the drawing board and created their own PSU called the SMPS Plus. They took a traditional SMPS and enhanced it to work specifically in a water ionizer. Tyent’s SMPS Plus gives you 55 adjustable power settings so you can create the perfect glass of alkaline water based on your source water. The power and efficiency of SMPS Plus are what enable Tyent ionizers to create strong alkaline and acidic water without using chemicals, providing a longer lifespan. With all of the advantages of Tyent’s SMPS Plus, you have to wonder why some companies still choose the cheaper, less efficient linear power supply in their water ionizers.",

		"Tyent coined the term “Super Water,” and we are still the ONLY machine with the largest Solid/Mesh Hybrid plates and enough power to produce super water without chemicals. Super Water means that Tyent ionizer's have a range of 2.5 to 11.5* on the pH scale, while our water ionizers have a range of 10.0 to 4.0* on the pH scale. On the low end of this scale, water with a pH of 2.3–3.5 works as an antiseptic and can be used as an all natural sanitizer on surfaces. On the high end of this scale, water with a pH of 10 and higher is great to use for removing oil-based herbicides and pesticides from fruits and vegetables, for removing stains and for cooking. Tyent ionizers doesn't uses any chemical to produce 2.5 to 11.5 pH but other ionizers in the market uses chemicals or salt to produce strongest acid or strongest alkaline.",

		"Alkaline water is very beneficial, but its benefits depend on having the purest drinking water possible. Tyent Water Ionizers have two filters, offering a customized multi-stage, sophisticated system designed to cleanse your tap water of impurities. Our Ultra filters offer a .01 micron filter media, equal to those found in kidney dialysis machines. We also have the largest physical filter in the industry. The size of the filters increases the purity of Tyent Water™. Water passes lengthwise through the filters; therefore, the longer the filter, the more media it has to pass through. That means even purer water. Tyent is the industry leader in filtration.",

		"Tyent’s large plates and adjustable power settings enable to reach the lower acidic pH levels and higher alkaline pH levels without the use of dangerous chemicals. The Tyent water ionizers are certified chemical and lead free, which is why so many health professionals and wellness centers prefer Tyent over any other water ionizer manufacturer in the industry. </p><p>\
		Insist on a chemical-free machine. Regardless of what you are told by the sellers of chemical-based machines, these chemicals travel through the same water cell that your drinking water comes from. Logic tells you that traces of this chemical will be in your drinking water.",

		"Tyent Water Ionizers use an automated Polarity Anti Scale Technology (PAST)—one of the most advanced cleaning systems in the industry. This automated cleaning cycle is triggered by both a timer and an internal sensor to ensure minimal mineral scale buildup on the electrodes for long-term performance. Better cleaning means longer-lasting components and healthier water.",

		"Tyent is both ISO 9001, ISO 14001 & ISO 13485 certified. The International Standards Organization (ISO), is responsible for certifying facilities for quality and consistency in product manufacturing. ISO certification ensures the lowest possible defect rates and confirms that the manufacturing facility operates in a manner that is safe for workers and the environment.",

		"Tyent is a certified medical device in Korea and is used by thousands of doctors in Asia to treat and prevent a wide variety of illnesses. Tyent is UL listed (E334893). The Underwriters Laboratory (UL) is the highest standard in safety and the most difficult safety listing to obtain. The Canadian Standards Association (CSA) uses the same standards as UL. These organizations certify devices for electrical safety. Tyent USA products are both CSA and UL approved for electrical safety.",

		"An A+ rating from the Better Business Bureau (BBB) is what companies strive for. The BBB's vision is to create an ethical marketplace where buyers and sellers can trust each other. The BBB is a well-recognized and highly respected organization which rates companies based on its Standards of Trust. Tyent USA has an A+ rating from the BBB.",

		"Our bodies use antioxidants to protect us from the harmful effects of free radicals. Free radicals cause damage to our cells and contribute to disease and premature aging. Antioxidants help reduce oxidative affliction and free-radical damage caused by an assortment of daily factors such as UVA/UVB exposure, stress, pollution, pesticides and more. Premium Tyent Water contains antioxidants. Scientific research has shown that Tyent Water Ionizers with Solid/Mesh Hybrid plates which greater surface area produce the highest antioxidants levels than any water ionizer.",

		"3 years warranty on ionizer and 15 years warranty on ionization chamber. </p><p>Read our warranty carefully and then compare it to our competitors’ warranties. Your investment is fully covered, regardless of your water quality or the quantity of water you produce.",

		"Tyent offers 3 general services per year in warranty period. In fact we are the only ionizer company providing doorstep services in warranty period regardless of location."

	];
	var divHtml = '\
		<div class="expanded dark col-lg-12 col-md-12 col-sm-12 col-xs-12" style="'+style+'">\
			 <div class="container">\
				<div class="btnDiv" ><span class="closebtn">×</span></div>\
				<h4 class="poppins">'+titles[clicked-1]+'</h4>	\
				<p>'+desc[clicked-1]+'</p>	\
				</div>\
		</div>';
	var width = $(window).width();
	var posAdd = 0;
    if (width <= 480) {
      posAdd = clicked;
    } else {
		posAdd = Math.ceil(clicked/4)*4;
    }

	if (clicked == expanded){
	  $( ".expanded" ).slideUp("slow", function(){
		  $(".expanded").remove();
		  expanded = 0;
		});      
	}else if (expanded == 0){
	  $(".cardNHolder #"+posAdd).after(divHtml);
	  $( ".expanded" ).slideDown("slow");
	  expanded = clicked;
	}else{
	  $( ".expanded" ).slideUp("slow", function(){
		$(".expanded").remove();
		$(".cardNHolder #"+posAdd).after(divHtml);
		$( ".expanded" ).slideDown("slow", function(){      
		  expanded = clicked;
		});      
	  });
	}   
  }); 
  $(document.body).on('click', '.btnDiv' ,function(){	   
	
	  $( ".expanded" ).slideUp("slow", function(){
		  $(".expanded").remove();
		  expanded = 0;
		});      
  });
  });

  

</script>
</body>

</html>