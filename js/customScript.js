$( document ).ready(function() {
          $( "#mobileButton" ).click(function() {
            $( ".mobileClose" ).toggle( "slow", function() {  });
            $( ".mobileBars" ).toggle( "slow", function() {  });
            $( "#navbar-brand-centered" ).toggleClass("collapse");
          });    
          
          $(function(){
            $('.nav a').filter(function(){return this.href==location.href}).parent().addClass('active').siblings().removeClass('active')
          
          })

          $( ".productDivSec" ).click(function() {
               $(".productDivSec").removeClass("active");
              $(this).addClass("active");
              var pos = $(this).index();
              if(pos !=0){
                $( "#features" ).hide();
                $( "#specifications" ).show();
              }else{
                $( "#features" ).show();
                $( "#specifications" ).hide();
              }
          });

          $( ".dropdown" ).hover(
            function() {
              $( this ).addClass("open");
            }, function() {
              $( this ).removeClass("open");
            }
          );
          
          function isMobile(){
            if($('.isMobile').css('display')=="none") return false;
            else return true;
          }
          $('#productsMenu').click(function () {
              window.location.href='/products';
          });
          var input    = document.querySelector("#city"), // Selects the input.
          datalist = document.querySelector("datalist"); // Selects the datalist.
      
      if($('#contact_form').length){
// Adds a keyup listener on the input.
      input.addEventListener("keyup", (e) => {
      
          // If input value is longer or equal than 2 chars, adding "users" on ID attribute.
          if (e.target.value.length > 2) {
              datalist.setAttribute("id", "cars");
          } else {
              datalist.setAttribute("id", "");
          }
      });

      }
      function gtag_report_conversion(url) {
        var callback = url;
        gtag('event', 'conversion', {
            'send_to': 'AW-802308772/niFkCPPoy-EBEKSFyf4C',
            'event_callback': callback
        });
        return false;
      }
          
    $('#contact_form').submit(function (e) {
      e.preventDefault();
      cur_url = window.location.href;
       if(validateForm($('#contact_form'))){
        gtag_report_conversion(cur_url);
          data = getFormData($('#contact_form'));
          $('.loadingUI').removeClass('dNone');
            $.ajax({
            type: 'post',
            url: 'submitForm',
            data: data,
            success: function(result){
              $("#contact_form").hide();
              $("#dynaMessage").empty();
              $("#dynaMessage").append(appendSuccessMessage(result));
              scrollToDynamessage();
              $('.loadingUI').addClass('dNone');
            },
            error:function(result){
              $("#dynaMessage").empty();
                $("#dynaMessage").append(appendErrorMessage(result.responseText));
                scrollToDynamessage();
                $('.loadingUI').addClass('dNone');
            }
          });
      }
    });

    $('#contact_formHead').submit(function (e) {
      e.preventDefault();
      cur_url = window.location.href;
       if(validateForm($('#contact_formHead'),true)){
        gtag_report_conversion(cur_url);
          data = getFormData($('#contact_formHead'));
          $('.loadingUI').removeClass('dNone');
            $.ajax({
            type: 'post',
            url: 'submitForm',
            data: data,
            success: function(result){
              $("#contact_formHead").hide();
              $("#dynaMessageHead").empty();
              $("#dynaMessageHead").append(appendSuccessMessage(result));
              $('.loadingUI').addClass('dNone');
            },
            error:function(result){
              $("#dynaMessageHead").empty();
                $("#dynaMessageHead").append(appendErrorMessage(result.responseText));
                $('.loadingUI').addClass('dNone');
            }

          });
      }
    });

    function appendSuccessMessage(data){
        a = '<div class="alert alert-success marginTop10 textAlignCenter" >'+data+'</div>';
        return a;
    }
      function appendErrorMessage(data){
          a = '<div class="alert alert-danger marginTop10 textAlignCenter" >'+data+'</div>';
          return a;
      }

    function validateForm(data,isHead=false) {
      function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
        }

        if(isHead){
          var msgDiv = "#dynaMessageHead";
        }
        else{
          var msgDiv = "#dynaMessage";
        }
        data = getFormData(data);
      $(msgDiv).empty();
      var valid = true;	
        errMessage ="";
      if(data.name=="") {
          errMessage = errMessage +'Please enter your name<br>';
          valid = false;
      }
      if(data.number.length!=10) {
          errMessage = errMessage +'Please enter 10 digit mobile Number<br>';
          valid = false;
      }
      if(!isEmail(data.email)){
              errMessage = errMessage +'Please enter valid email address<br>';
              valid = false;
      }
      if(data.city.length<4){
          errMessage = errMessage +'Please enter valid city with more than 3 characters<br>';
          valid = false;
      }
      if(data.message.length<10){
        errMessage = errMessage +'Please enter valid message with more than 10 characters<br>';
        valid = false;
    }
      if(valid==false)$(msgDiv).append(appendErrorMessage(errMessage));
      return valid;
  }

  function getFormData($form){
      var unindexed_array = $form.serializeArray();
      var indexed_array = {};
  
      $.map(unindexed_array, function(n, i){
          indexed_array[n['name']] = n['value'];
      });
  
      return indexed_array;
  }

  function scrollToDynamessage(){
      $('html, body').animate({
          scrollTop: $("#dynaMessage").offset().top-250
      }, 2000);
  }
});