var simplemaps_countrymap_mapdata={
  main_settings: {
   //General settings
    width: "responsive", //'700' or 'responsive'
    background_color: "#FFFFFF",
    background_transparent: "yes",
    border_color: "#ffffff",
    
    //State defaults
    state_description: "",
    state_color: "#88A4BC",
    state_hover_color: "#3B729F",
    state_url: "http://simplemaps.com",
    border_size: 1.5,
    all_states_inactive: "no",
    all_states_zoomable: "yes",
    
    //Location defaults
    location_description: "",
    location_color: "#e04006",
    location_opacity: 0.8,
    location_hover_opacity: 1,
    location_url: "",
    location_size: 50,
    location_type: "marker",
    location_image_source: "frog.png",
    location_border_color: "#FFFFFF",
    location_border: 2,
    location_hover_border: 2.5,
    all_locations_inactive: "no",
    all_locations_hidden: "no",
    
    //Label defaults
    label_color: "#d5ddec",
    label_hover_color: "#d5ddec",
    label_size: 22,
    label_font: "Arial",
    hide_labels: "no",
    hide_eastern_labels: "no",
   
    //Zoom settings
    zoom: "yes",
    back_image: "no",
    initial_back: "no",
    initial_zoom: "-1",
    initial_zoom_solo: "no",
    region_opacity: 1,
    region_hover_opacity: 0.6,
    zoom_out_incrementally: "yes",
    zoom_percentage: 0.99,
    zoom_time: 0.5,
    
    //Popup settings
    popup_color: "white",
    popup_opacity: 0.9,
    popup_shadow: 1,
    popup_corners: 5,
    popup_font: "12px/1.5 Verdana, Arial, Helvetica, sans-serif",
    popup_nocss: "no",
    
    //Advanced settings
    div: "map",
    auto_load: "yes",
    url_new_tab: "no",
    images_directory: "default",
    fade_time: 0.1,
    link_text: "View Website",
    popups: "detect",
    state_image_url: "",
    state_image_position: "",
    location_image_url: "",
    manual_zoom: "yes"
  },
  state_specific: {
    "1": {
      name: "Andaman and Nicobar"
    },
    "2": {
      name: "Andhra Pradesh"
    },
    "3": {
      name: "Arunachal Pradesh"
    },
    "4": {
      name: "Assam"
    },
    "5": {
      name: "Bihar"
    },
    "6": {
      name: "Chandigarh"
    },
    "7": {
      name: "Chhattisgarh"
    },
    "8": {
      name: "Dadra and Nagar Haveli"
    },
    "9": {
      name: "Daman and Diu"
    },
    "10": {
      name: "Delhi"
    },
    "11": {
      name: "Goa"
    },
    "12": {
      name: "Gujarat"
    },
    "13": {
      name: "Haryana"
    },
    "14": {
      name: "Himachal Pradesh"
    },
    "16": {
      name: "Jharkhand"
    },
    "17": {
      name: "Karnataka"
    },
    "18": {
      name: "Kerala"
    },
    "19": {
      name: "Lakshadweep"
    },
    "20": {
      name: "Madhya Pradesh"
    },
    "21": {
      name: "Maharashtra"
    },
    "22": {
      name: "Manipur"
    },
    "23": {
      name: "Meghalaya"
    },
    "24": {
      name: "Mizoram"
    },
    "25": {
      name: "Nagaland"
    },
    "26": {
      name: "Orissa"
    },
    "27": {
      name: "Puducherry"
    },
    "28": {
      name: "Punjab"
    },
    "29": {
      name: "Rajasthan"
    },
    "30": {
      name: "Sikkim"
    },
    "31": {
      name: "Tamil Nadu"
    },
    "32": {
      name: "Tripura"
    },
    "33": {
      name: "Uttar Pradesh"
    },
    "34": {
      name: "Uttaranchal"
    },
    "35": {
      name: "West Bengal"
    },
    "36": {
      name: "Jammu and Kashmir"
    },
    "37": {
      name: "Telangana"
    }
  },
  locations: {
    "0": {
      lat: "18.987807",
      lng: "72.836447",
      name: "Mumbai",
    },
    "1": {
      lat: "28.651952",
      lng: "77.231495",
      name: "Delhi"
    },
    "2": {
      lat: "22.562627",
      lng: "88.363044",
      name: "Kolkata"
    },
    "3": {
      lat: "12.977063",
      lng: "77.587106",
      name: "Bangalore",
      description: "Branch Office",
      color: "#f0a500"
    },
    "4": {
      lat: "17.384052",
      lng: "78.456355",
      name: "Hyderabad",
      description: "Home Office",
      color:"#f0a500"
    },
    "5": {
      lat: "23.025793",
      lng: "72.587265",
      name: "Ahmedabad"
    },
    "6": {
      lat: "26.913312",
      lng: "75.787872",
      name: "Jaipur"
    },
    "7": {
      lat: "26.839281",
      lng: "80.923133",
      name: "Lucknow"
    },
    "8": {
      lat: "21.195944",
      lng: "72.830232",
      name: "Surat"
    },
    "8": {
      lat: "18.513271",
      lng: "73.849852",
      name: "Pune"
    },
    "9": {
      lat: "22.7196",
      lng: "75.8577",
      name: "Indore"
    },
    "10": {
      lat: "26.4499",
      lng: "74.6399",
      name: "Ajmer"
    },
    "11": {
      lat: "25.2138",
      lng: "75.8648",
      name: "Kota"
    },
    "12": {
      lat: "30.9010",
      lng: "75.8573",
      name: "Ludhiana"
    },
    "13": {
      lat: "28.4744",
      lng: "77.5040",
      name: "Greater Noida"
    },
    "14": {
      lat: "28.9931",
      lng: "77.0151",
      name: "Sonipat"
    },
    "15": {
      lat: "16.5062",
      lng: "80.6480",
      name: "Vijayawada"
    },
    "16": {
      lat: "17.6868",
      lng: "83.2185",
      name: "Visakhapatnam",
      description:"Branch Office",
      color: "#f0a500"
    },
    "17": {
      lat: "29.9094",
      lng: "73.8800",
      name: "Ganga Nagar"
    },
    "18": {
      lat: "25.3176",
      lng: "82.9739",
      name: "Varanasi"
    },
    "19": {
      lat: "28.6692",
      lng: "77.4538",
      name: "Ghaziabad"
    },
    "20": {
      lat: "21.2514",
      lng: "81.6296",
      name: "Raipur"
    },
    "21": {
      lat: "22.0797",
      lng: "82.1409",
      name: "Bilaspur"
    }, 
    "21": {
      lat: "23.3441",
      lng: "85.3096",
      name: "Ranchi"
    }, 
    "21": {
      lat: "25.5941",
      lng: "85.1376",
      name: "Patna"
    }, 
    "22": {
      lat: "23.1815",
      lng: "79.9864",
      name: "Jabalpur"
    }, 
    "23": {
      lat: "23.2599",
      lng: "77.4126",
      name: "Bhopal"
    }, 
  },
  labels: {},
  regions: {}
};