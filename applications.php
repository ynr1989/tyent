<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
    <!-- Bootstrap Core CSS -->
    <link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
        rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />

    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="customStyle.css">

</head>

<body id="page-top">
    <?php include("phpIncludes/header.php") ?>

    <!-- /navbar ends -->


    <section id="content">
        <div class="container headMargin paddingTopBottom">
            <div class="subBox">	                
                <div class="fancy-title title-bottom-border">
                    <h2>Application of <span>Alkaline Water</span></h2>
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img01.jpg" alt="To cook lusciously glazed, delicious rice"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">To cook lusciously glazed, delicious rice</div>
                        <div class="fpx14 line14" style="margin-top:24px;">
                        Wash the rice with acidic water 1~2 times and then rinse with purified water.<br>
                        Use level-2 or level-3 alkaline water to soak the rice for about 30 minutes and then cook the create glutinous, glazed<br>
                        rice with exceptional savory taste that doesn't spoil easily. (Use a smaller quantity of water than when cooking with<br>
                        ordinary water.
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img02.jpg" alt="To parboil vegetables to bring out their vivid colors"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">To parboil vegetables to bring out their vivid colors</div>
                        <div class="fpx14 line14" style="margin-top:24px;">
                        Boiling colored vegetables with alkaline ionized waer brings out the natural vividness of the bcol-md-10 col-sm-10 green and yellow<br>
                        hues of the vegetables. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img03.jpg" alt="To enjoy barley or corn tea"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">To enjoy barley or corn tea</div>
                        <div class="fpx14 line14" style="margin-top:24px;">
                        Just place the barley (or corn) tea bag in the alkaline ionized water. Even without boiling, the water's  strong<br>
                        absorbing, dissolving effects elicits 10 times more of the tea, creating a rich brew. This save the time and energy<br>
                        usually consumed by boiling and cooling before storing the tea in the refrigerator. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img04.jpg" alt="To brew thick soups from beef or bone"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">To brew thick soups from beef or bone</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        When simmering thick soups from beef or bone, using alkaline ionized water brings out all the savory nutrients at once,<br>
                        creating a deep and rich taste. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img05.jpg" alt="To tenderize meat dishes"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">To tenderize meat dishes </div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        Marinating the meat in alkaline ionized water for 20~30 minutes enables you to enjoy a more tender dish. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img06.jpg" alt="For fried foods."></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">For fried foods.</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        When frying, using acidic water gives you a crispier and more savory result. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img07.jpg" alt="To add a deeper aroma and color to your tea"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">To add a deeper aroma and color to your tea</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        Our water removes the bitterness of coffee and the unappealing astringent taste of green tea while bringing out the tea's natural taste and aroma.
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img08.jpg" alt="To create a smooth alcoholic drink or cocktail"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">To create a smooth alcoholic drink or cocktail</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        Our water adds a mildness and smoothness to the special drink or cocktail that you create, and ice made from our alkaline ionized water will add even greater flavor.
                        Cocktails made with alkaline ionized water are relatively neutralized, and thus prevents hangovers. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img09.jpg" alt="To prepare brewed oriental medicine"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">To prepare brewed oriental medicine</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        When preparing brewed oriental medicine, try using our alkaline ionized water.
                        It deeply penetrates the medicinal ingredients to bring out their beneficial effects, giving you a result superior to re-brewing and mixing. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img10.jpg" alt="Even plants appreciate our ionized water"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">Even plants appreciate our ionized water</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        Houseplants and garden vegetables such as peppers, lettuce, and leeks grow well when watered with our alkaline ionized water.
                        Placing the seed in the water for about a day enables it to sprout and show healthy growth. Also excellent for raising soybean sprouts in your home kitchen. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img11.jpg" alt="If you're concerned about agri-chemicals and fertilizers contaminating
            your vegetables and fruits"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">If you're concerned about agri-chemicals and fertilizers contaminating<br>your vegetables and fruits</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        If you wish to directly consume various vegetables and fruits, wash them with acidic water and finally rinse with alkaline water.
                        The acidic water has strong oxidation power that can remove even bacteria, viruses, and agri-chemicals, and maintains the freshness and natural hues of the vegetables and fruits during washing. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img12.jpg" alt="When cooking noodles"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">When cooking noodles</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        When boiling noodles, use acidic water make them glutinous and chewy.
                        However, thicker noodles or pasta (such as udon noodles or spaghetti) may not be cooked easily with acidic water, so use alkaline water instead. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="fancy-title title-bottom-border">
                    <h2>Application of <span>Acidic Water</span></h2>
                </div>
                                
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img13.jpg" alt="When washing kitchenware"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">When washing kitchenware</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        Acidic ionized water has excellent cleaning and disinfecting power and is thus optimal for use in washing dishes and kitchenware to maintain their gloss and cleanliness for a longer period.
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img14.jpg" alt="When cleaning the bathroom or kitchen"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">When cleaning the bathroom or kitchen</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        Using strong acidic ionized water when cleaning the tiled floors of hallways, etc. 
                        greatly facilitates the removal of dust from every nook and cranny, and because the acidic ionized water dries quickly, it prevents stickiness. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img15.jpg" alt="Acne and Skin Problems"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">Acne and Skin Problems</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        Acid water from your water ionizer has an astringent effect and can be used with helping to treat or prevent acne, skin spots and other minor skin problems.<br>
                        Washing the face with acidic water helps to make the skin smooth, and helps to improve the overall health and sheen of your skin.  
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img16.jpg" alt="Watering Plants"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">Watering Plants</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        Use it to water house plants or outside plants to promote growth. Plants respond well to acidic water – rain water is acidic. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img17.jpg" alt="Chapped Hands"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">Chapped Hands</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        May help to prevent chapped hands as well as speed recovery from chapping.
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img18.jpg" alt="Pet Friendly"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">Pet Friendly</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        Bathe your pets with it for an overall healthier coat. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img19.jpg" alt="Cosmetics and Hair and Bath"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">Cosmetics and Hair and Bath</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        Rinse your hair with acidic water before and after shampooing to help prevent hair loss, dandruff, itchy scalp. 
                        </div>
                        
                    </div>
                    
                </div>
                
                <div class="appDiv">
                    
                    <div class="col-md-2 col-sm-2"><img src="cImages/applications/sub01_01_03_img20.jpg" alt="Insect Bites"></div>
                    <div class="col-md-10 col-sm-10" >
                        
                        <div class="fw600 fpx18 c36">Insect Bites</div>
                        <div class="fpx14 line14" style="margin-top:24px; letter-spacing:-0.2px;">
                        provides relief from the itch or sting of mosquito and bug bites.
                        </div>
                        
                    </div>
                    
                </div>
                
                
            </div>
        </div>


    </section>
    <?php include("phpIncludes/footer.php") ?>

    <!-- /footer ends -->
    <!-- Core JavaScript Files -->
    <script src="header/js/jquery.min.js"></script>
    <script src="header/js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/functions.js"></script>


    <!-- Main Js -->
    <script src="header/js/main.js"></script>


    <script src="js/customScript.js"></script>

<script>$('#aboutLi').addClass('active');</script>
</body>

</html>