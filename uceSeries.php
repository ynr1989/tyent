<?php 
	$plates = "NULL";
	$plates = $p;
	$price = "NULL";
	if($plates==9){
		$price = "2,79,000";
		$plateName = "UCE 9 Plate";
	}
	if($plates==11){
		$price = "2,97,000";
		$plateName = "UCE 11 Plate";
	}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="../header/css/bootstrap.css"  type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../style.css" type="text/css" />
	<link rel="stylesheet" href="../css/dark.css" type="text/css" />
	<link rel="stylesheet" href="../css/animate.css" type="text/css" />
	<link rel="stylesheet" href="../css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="../css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="../include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="../include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="../css/xzoom.css" media="all" />

	<link rel="stylesheet" type="text/css" href="../customStyle.css">

</head>

<body id="page-top">
	<?php include("phpIncludes/header.php") ?>

	<!-- /navbar ends -->
	<section id="slider" class="headMargin slider-element swiper_wrapper uceHead">
		<div class="swiper-container swiper-parent">
			<div class="swiper-wrapper">

				<div class="swiper-slide">
					<div class="container">
						<div class="row align-items-center mt-5 poppins marginTop30px">
							<div class="col-md-12 ">
								<div class="heading-block nobottomborder mb-4">
									<h1 class="t700 ls0 nott floatRight marginRight100px"><?php echo $plateName?></h1>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	<section id="content ">
		<div class="container headMargin">
			<div class="col-md-12 paddingMobileLR5px">
				<div class="col-md-4 productImages">
					<img class="xzoom" id="main_image" src="../../cImages/prod/uceg1.png" xoriginal="cImages/prod/uceg1.png">

					<div class="xzoom-thumbs marginTop50px">
						<a href="../../cImages/prod/uceg1.png">
							<img class="xzoom-gallery" width="60" src="../../cImages/prod/uceg1.png">
						</a>
						<a href="../../cImages/prod/uceg2.png">
							<img class="xzoom-gallery" width="60" src="../../cImages/prod/uceg2.png">
						</a>

						<a href="../../cImages/prod/uceg3.png">
							<img class="xzoom-gallery" width="60" src="../../cImages/prod/uceg3.png">
						</a>

						<a href="../../cImages/prod/uceg4.png">
							<img class="xzoom-gallery" width="60" src="../../cImages/prod/uceg4.png">
						</a>

						<a href="../../cImages/prod/uceg5.png">
							<img class="xzoom-gallery" width="60" src="../../cImages/prod/uceg5.png">
						</a>



					</div>
				</div>
				<div class="col-md-8 paddingMobileLR5px">
					<div class="text- pro-infor marginTop10px">

						<div class="pro-infor-title poppins"><?php echo $plateName?></div>

						<table  class="pro-inforTable">

							<colgroup>
								<col width="135">
								<col width="">
							</colgroup>

							<tbody>
								<tr>
									<td class="bold1">Product name</td>
									<td>Water ionizer</td>
								</tr>

								<tr>
									<td colspan="2" height="13"></td>
								</tr>

								<tr>
									<td class="bold1">Dimension(mm)</td>
									<td>Water Ionizer</td>
									<td class="tablePadLeft10px">Faucet</td>
								</tr>
								<tr>
									<td></td>
									<td> 360(W) x 132(D) x 355(H)</td>
									<td class="tablePadLeft10px"> 50(W) x 343(D)</td>
								</tr>

								<tr>
									<td colspan="2" height="13"></td>
								</tr>

								<tr>
									<td class="bold1">Weight(kg)</td>
									<td>Water ionizer - 6.1kg</td>
									<td>Faucet - 415g</td>
								</tr>

								<tr>
									<td colspan="2" height="13"></td>
								</tr>
								<tr>
									<td class="bold1">Plates</td>
									<td><?php echo $plates?></td>
								</tr>

								<tr>
									<td colspan="2" height="13"></td>
								</tr>
								<tr>
									<td class="bold1">Price</td>
									<td>₹ <?php echo $price?></td>
								</tr>


							</tbody>
						</table>

					</div>

				</div>
			</div>

		</div>

		<div class="bgColorSelected">
			<div class="container">
				<div class="productDiv">
					<ul>

						<li class="productDivSec active">
							<div>Features</div>
						</li>
						<li class="productDivSec ">
							<div>Specifications</div>
						</li>
					</ul>
				</div>
				<div class="marginTop30px">
					<div class="productDivDesc table-responsive " id="specifications">
						<table class="pro-view-table mt50" style="text-align:left;">

							<colgroup>
								<col width="220">
								<col width="50">
								<col width="120">
								<col width="300">
								<col width="110">
								<col width="">
							</colgroup>

							<tbody>
								<tr>
									<td class="center title line_r">Model</td>
									<td colspan="5" class="center fw600">UCE-9000T</td>
								</tr>

								<tr>
									<td class="center title line_r">Exterior of Product</td>
									<td></td>
									<td class="fw600">Dimension</td>
									<td><span class="bold1">Water ionizer</span> &nbsp;360(W) x 132(D) x 355(H)<br><span
											class="bold1">Faucet</span> &nbsp;50(W) x 343(D)</td>
									<td class="fw600">Weight</td>
									<td><span class="bold1">Water ionizer</span> &nbsp;6.1kg<br><span
											class="bold1">Faucet</span> &nbsp;415g</td>
								</tr>

								<tr>
									<td class="center title line_r">Use Environment</td>
									<td></td>
									<td class="fw600">Water temperature</td>
									<td>4~35℃</td>
									<td class="fw600">Water pressure</td>
									<td>1~5kgf/㎠</td>
								</tr>

							</tbody>
						</table>
						<table class="pro-view-table mt30" style="text-align:left;">

							<colgroup>
								<col width="220">
								<col width="50">
								<col width="120">
								<col width="300">
								<col width="110">
								<col width="">
							</colgroup>

							<tbody>
								<tr>
									<td class="center title line_r" rowspan="2">Water outflow</td>
									<td></td>
									<td class="fw600">Outflow method</td>
									<td>Electronic outflow structure</td>
									<td class="fw600">Water outflow</td>
									<td>1.3L/min ± 0.1L</td>
								</tr>

								<tr>
									<td></td>
									<td class="fw600">pH levels</td>
									<td colspan="3">3 Alkaline, 1 Neutral, 3 Acidic, Turbo</td>
								</tr>

							</tbody>
						</table>
						<table class="pro-view-table mt30" style="text-align:left;">

							<colgroup>
								<col width="220">
								<col width="50">
								<col width="120">
								<col width="300">
								<col width="110">
								<col width="">
							</colgroup>

							<tbody>
								<tr>
									<td class="center title line_r">Controls and display</td>
									<td></td>
									<td class="fw600">Controls</td>
									<td>Touch screen</td>
									<td class="fw600">Display</td>
									<td>TFT-LCD</td>
								</tr>

								<tr>
									<td class="center title line_r">Power</td>
									<td></td>
									<td class="fw600">Power supply</td>
									<td>SMPS</td>
									<td class="fw600">Power consumption</td>
									<td>Max.220w</td>
								</tr>

								<tr>
									<td class="center title line_r">Electrolysis section</td>
									<td></td>
									<td class="fw600">Water Cell Plate Materials</td>
									<td>Platinum and Titanium</td>
									<td class="fw600">Electrolytic Cell Quantity of Electrodes</td>
									<td>9 / 11</td>
								</tr>

								<tr>
									<td class="center title line_r">Filters configuration</td>
									<td></td>
									<td class="fw600">Filter Structure</td>
									<td colspan="3">2-Filter system</td>

								</tr>

							</tbody>
						</table>
						<table class="pro-view-table mt30" style="text-align:left;">

							<colgroup>
								<col width="220">
								<col width="50">
								<col width="120">
								<col width="300">
								<col width="110">
								<col width="">
							</colgroup>

							<tbody>
								<tr>
									<td class="center title line_r" rowspan="5">Convenient function</td>
									<td></td>
									<td class="fw600">Voice announcement</td>
									<td><img src="../../cImages/001-check.png"></td>
									<td class="fw600">Voice ON/OFF</td>
									<td><img src="../../cImages/001-check.png"></td>
								</tr>

								<tr>
									<td></td>
									<td class="fw600">Automatic Cleaning</td>
									<td><img src="../../cImages/001-check.png"></td>
									<td class="fw600">Manual Cleaning</td>
									<td><img src="../../cImages/001-check.png"></td>
								</tr>

								<tr>
									<td></td>
									<td class="fw600">Backlight color</td>
									<td><img src="../../cImages/001-check.png"></td>
									<td class="fw600">Automatic Stop Function</td>
									<td><img src="../../cImages/001-check.png"></td>
								</tr>

								<tr>
									<td></td>
									<td class="fw600">Filter usage indication</td>
									<td><img src="../../cImages/001-check.png"></td>
									<td class="fw600">Error indication</td>
									<td><img src="../../cImages/001-check.png"></td>
								</tr>

								<tr>
									<td></td>
									<td class="fw600">Turbo function</td>
									<td><img src="../../cImages/001-check.png"></td>
								</tr>

							</tbody>
						</table>
						<table class="pro-view-table mt30" style="text-align:left;">

							<colgroup>
								<col width="220">
								<col width="50">
								<col width="120">
								<col width="300">
								<col width="110">
								<col width="">
							</colgroup>

							<tbody>
								<tr>
									<td class="center title line_r" rowspan="2">Safety function</td>
									<td></td>
									<td class="fw600">Water temperature sensor</td>
									<td><img src="../../cImages/001-check.png"></td>
									<td class="fw600">Automatic Stop Function</td>
									<td><img src="../../cImages/001-check.png"></td>
								</tr>

								<tr>
									<td></td>
									<td class="fw600">Supplying water sensor</td>
									<td><img src="../../cImages/001-check.png"></td>
									<td class="fw600">Overheating protection</td>
									<td><img src="../../cImages/001-check.png"></td>
								</tr>

							</tbody>
						</table>
						<table class="pro-view-table mt30" style="text-align:left;">

							<colgroup>
								<col width="220">
								<col width="50">
								<col width="120">
								<col width="300">
								<col width="110">
								<col width="">
							</colgroup>

							<tbody>
								<tr>
									<td class="center title line_r" rowspan="2">Setting function</td>
									<td></td>
									<td class="fw600">Adjustable pH</td>
									<td><img src="../../cImages/001-check.png"></td>
									<td class="fw600">Voice volume control</td>
									<td><img src="../../cImages/001-check.png"></td>
								</tr>

								<tr>
									<td></td>
									<td class="fw600">Filter reset</td>
									<td><img src="../../cImages/001-check.png"></td>
									<td class="fw600">Language setting</td>
									<td><img src="../../cImages/001-check.png"></td>
								</tr>

							</tbody>
						</table>
					</div>
					<div class="productDivDesc" id="features">
						<div class="container clearfix paddingBottom">

							<div id="side-navigation" class="ui-tabs ui-corner-all ui-widget ui-widget-content ">

								<div class="col_one_third nobottommargin sidebar">

									<ul class="sidenav ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header sidebar__inner"
										role="tablist">
										<button class="button button-3d button-rounded button-teal mobileCloseButton"><i
												class="icon-remove"></i></button>

										<li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" role="tab"
											tabindex="-1" aria-controls="snav-content1" aria-labelledby="ui-id-1"
											aria-selected="false" aria-expanded="false"><a href="#snav-content1"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1"><i
													class="icon-screen"></i>About alkaline water</a></li>
										<li role="tab" tabindex="-1"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content2" aria-labelledby="ui-id-2"
											aria-selected="false" aria-expanded="false"><a href="#snav-content2"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2"><i
													class="icon-magic"></i>Features<i class="icon-chevron-"></i></a>
										</li>
										<li role="tab" tabindex="-1"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content3" aria-labelledby="ui-id-3"
											aria-selected="false" aria-expanded="false"><a href="#snav-content3"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-3"><i
													class="icon-star3"></i>Functions<i class="icon-chevron-"></i></a>
										</li>
										<li role="tab" tabindex="0"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content4" aria-labelledby="ui-id-4" aria-selected="true"
											aria-expanded="true"><a href="#snav-content4" role="presentation"
												tabindex="-1" class="ui-tabs-anchor" id="ui-id-4"><i
													class="icon-gift"></i>TYENT's
												Technological Process<i class="icon-chevron-"></i></a></li>
										<li role="tab" tabindex="-1"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content5" aria-labelledby="ui-id-5"
											aria-selected="false" aria-expanded="false"><a href="#snav-content5"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-5"><i
													class="icon-adjust"></i>FILTER
												SYSTEM<i class="icon-chevron-"></i></a></li>
									</ul>
								</div>

								<div class="col_two_third col_last nobottommargin">

									<div id="snav-content1" aria-labelledby="ui-id-1" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div class="tab-con-box">

											<div class="tab-box-title">
												<div class="tab-title">About alkaline water</div>
											</div>

											<div class="pageTitle" style="margin-top:60px"><img
													src="../../cImages/sub_dot.gif" alt="">Alkaline water</div>

											<div class="fpx14 line14">
												Water that has been purified through the filter next undergoes
												electrolysis
												in the electrolyzer, and acidic anion components such as chlorine,
												sulfate,
												nitrate ions, etc. are collected in the positive pole to form acidic
												ionized
												water, creating oxygen in the process. Meanwhile, in the negative pole,
												alkaline
												positive ion components such as calcium, magnesium, sodium ions, etc.
												gather
												to form alkaline ionized water and generates hydrogen.
											</div>

											<div class="imgWrap" style="margin:50px 0px;"><img
													src="../../cImages/pro11_1_1_img01.gif" alt="Alkaline water"></div>

											<table style="width:100%">

												<colgroup>
													<col width="70">
													<col width="450">
													<col width="70">
													<col width="">
												</colgroup>

												<tbody>
													<tr valign="top">

														<td><img src="../../cImages/pro11_1_1_n1.gif" alt=""></td>

														<td>
															<table>
																<tbody>
																	<tr>
																		<td class="fpx18 fw600">Water with rich <span
																				class="fblue2">minerals</span></td>
																	</tr>
																	<tr>
																		<td height="16"></td>
																	</tr>
																	<tr>
																		<td class="fpx14 line14">Alkaline water contains
																			rich minerals such as calcium,potassium and
																			magnesium.</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr>
														<td><img src="../../cImages/pro11_1_1_n2.gif" alt=""></td>

														<td>
															<table>
																<tbody>
																	<tr>
																		<td class="fpx18 fw600"><span
																				class="fblue2">Smaller</span> clusters
																		</td>
																	</tr>
																	<tr>
																		<td height="16"></td>
																	</tr>
																	<tr>
																		<td class="fpx14 line14">
																			The particles of this water is about 1/3
																			smaller
																			than ordinary
																			water and are thus able to permeate deep
																			into
																			the human
																			body, quickly removing waste matter out of
																			the
																			body.
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>

													</tr>

													<tr>
														<td height="20" colspan="4"></td>
													</tr>

													<tr valign="top">

														<td><img src="../../cImages/pro11_1_1_n3.gif" alt=""></td>

														<td>
															<table>
																<tbody>
																	<tr>
																		<td class="fpx18 fw600">Water which has high
																			reductive powers</td>
																	</tr>
																	<tr>
																		<td height="16"></td>
																	</tr>
																	<tr>
																		<td class="fpx14 line14">
																			Alkaline ionized water has high reductive
																			powers, and has
																			anti-oxidant power because it contains
																			abundant
																			amounts
																			of active hydrogen which removes active
																			oxygen,
																			which is
																			the source of an array of diseases.
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr>

														<td><img src="../../cImages/pro11_1_1_n4.gif" alt=""></td>

														<td>
															<table>
																<tbody>
																	<tr>
																		<td class="fpx18 fw600"><span
																				class="fblue2">Alkaline</span> water
																		</td>
																	</tr>
																	<tr>
																		<td height="16"></td>
																	</tr>
																	<tr>
																		<td class="fpx14 line14">Alkaline water is
																			alkaline
																			water from pH 8.5 to 10.</td>
																	</tr>
																</tbody>
															</table>
														</td>

													</tr>

												</tbody>
											</table>

											<div class="sub_space"></div>

											<div class="fpx18 fw600">Improved effect of 4 kinds of gastroenteric
												troubles
											</div>

											<div class="imgWrap mt30"><img src="../../cImages/pro11_3_1_img01.gif"
													alt="Improved effect of 4 kinds of gastroenteric troubles"></div>

										</div>
									</div>

									<div id="snav-content2" aria-labelledby="ui-id-2" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div class="tab-con-box">
											<div class="col-md-12">
												<div class="tab-box-title">
													<div class="tab-title">Features</div>
												</div>
												<div class="imgWrap mt50 marginTop50px"><img
														src="../../cImages/pro11_7_1_img02.jpg" alt="Features"></div>

											</div>

											<div class="marginTop30px uceTable1">
												<div class="fpx24 fblue2 center fw600 mt40">CAREFUL CONSIDERATION FOR
													USER</div>
												<div class="marginTop50px featuresUce">
													<div class="col-md-6 col-xs-12">
														<div class="col-md-4"><img src="../../cImages/pro11_7_1_img03.gif"
															alt="Excellent MP3 Sound Quality with Guided Support">
														</div>
														<div class="col-md-8"><div class="fblue fpx16 fw600">Excellent MP3 Sound
															Quality with Guided<br>Support</div>
														<div class="fpx14 line14" style="margin-top:17px;">
															Functions under operation can be further identified
															by<br>the top quality voice guided system.
														</div></div>
													</div>
													<div class="col-md-6 col-xs-12">
														<div class="col-md-4">
															<img src="../../cImages/pro11_7_1_img04.gif"
																alt="Multiple Languages Supported">
														</div>
														<div class="col-md-8">
															<div class="fblue fpx16 fw600">Multiple Languages
																Supported</div>
															<div class="fpx14 line14" style="margin-top:17px;">
																Diversified language guide support is available
																by<br>
																selecting 1 of 6 languages featured from across
																the<br>
																world.
															</div>
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="col-md-6 col-xs-12">
														<div class="col-md-4">
															<img src="../../cImages/pro11_7_1_img06.gif"
															alt="Automatic Water Discharge Interruption Function">
														</div>
														<div class="col-md-8">
															<div class="fblue fpx16 fw600">Automatic Water Discharge
																Interruption<br>Function</div>
															<div class="fpx14 line14" style="margin-top:17px;">
																In order to prevent the overflow of water due to
																user<br>
																negligence, the water discharge is automatically<br>
																stopped after 15 minutes of use.
															</div>
														</div>
													</div>
													<div class="col-md-6 col-xs-12">
														<div class="col-md-4">
															<img src="../../cImages/pro11_7_1_img07.gif"
															alt="Automatic Electrolysis Tank Cleaning Function">
														
														</div>
														<div class="col-md-8">
															<div class="fblue fpx16 fw600">Automatic Electrolysis
																Tank Cleaning<br>Function</div>
															<div class="fpx14 line14" style="margin-top:17px;">
																The interior of the Electrolysis Tank is
																automatically<br>
																cleaned on a regular basis, ensuring stable
																electrolysis<br>
																operating conditions.
															</div>
														</div>
													</div>
													<div class="clearfix"></div>

													<div class="col-md-6 col-xs-12">
														<div class="col-md-4">
															<img src="../../cImages/pro11_7_1_img05.gif"
															alt="Temperature Sensor Function for Safe Operation">
														
														</div>
														<div class="col-md-8">
															<div class="fblue fpx16 fw600">Temperature Sensor
																Function for Safe<br>Operation</div>
															<div class="fpx14 line14" style="margin-top:17px;">
																If the temperature of the source water exceeds
																95℉<br>
																(35℃) then water will stop flowing and a warning<br>
																message will appear on the screen.
															</div>
														</div>
													</div>
													<div class="col-md-6 col-xs-12">
														<div class="col-md-4">
															<img src="../../cImages/pro11_7_1_img08.gif"
																	alt="Automatic Memory Function">
														
														</div>
														<div class="col-md-8">
															<div class="fblue fpx16 fw600">Automatic Memory Function
															</div>
															<div class="fpx14 line14" style="margin-top:17px;">
																The last function used by the user is
																automatically<br>
																memorized and displayed on the Standby Screen
																for<br>
																fast and convenient operation.
															</div>
														</div>
													</div>
													<div class="clearfix"></div>
										
													<div class="col-md-6 col-xs-12">
														<div class="col-md-4">
															<img src="../../cImages/pro11_7_1_img09.gif"
															alt="Water Discharge Nozzle with Directional Control">														
														</div>
														<div class="col-md-8">
															<div class="fblue fpx16 fw600">Water Discharge Nozzle
																with Directional<br>Control</div>
															<div class="fpx14 line14" style="margin-top:17px;">
																The water discharge direction may be adjusted to<br>
																many diverse angles based on user preference.
															</div>
														</div>
													</div>
													<div class="col-md-6 col-xs-12">
														<div class="col-md-4">
															<img src="../../cImages/pro11_7_1_img15.gif"
															alt="Upgraded Power Turbo System">
														</div>
														<div class="col-md-8">
															<div class="fblue fpx16 fw600">Upgraded Power Turbo
																System</div>
															<div class="fpx14 line14" style="margin-top:17px;">
																Turbo upgrades to the SMPS Power Supply system<br>
																produce both the acid turbo water and strong
																alkali<br>
																water you need.
															</div>
															
														</div>
													</div>
												</div>
											</div>



											<div class="marginTop30px">
												<div class="pageTitle">
													<img src="../../cImages/sub_dot.gif" alt="">SMPS power supply
												</div>
												<div class="col-md-6  imgWrap"><img src="../../cImages/pro11_1_1_img18.jpg"
														alt="SMPS power supply"></div>
												<div class="col-md-6 ">
													<div class="fw600 fpx16 fblue2">Stables SMPS type power
														supply<br>(Registered Patent No. 10-0714055)</div>
													<div class="fpx14 mt30 line15">
														Equipped with an SMPS direct current power supply device that is
														used in<br>
														cutting-edge products, thereby heightening the stability of the
														power supply<br>
														and offering an internal preventive mechanism against
														overheating.<br><br>

														- Stable power supply<br>
														- Electrical safety<br>
														- Changes in the output voltage in correlation to changes in the
														input power
													</div>
												</div>
											</div>
											<div class="marginTop30px">
												<div class="">
													<div class="pageTitle"><img src="../../cImages/sub_dot.gif" alt="">ROUND
														MESH electrolyzer</div>
													<div class="fpx14 line15 marginTop30px">
														Electrolyzer is most important part in water ionizer.The
														difference of the surface treatment determines
														the electrolysis efficiency.
													</div>
												</div>
												<div class=" marginTop30px imgWrap"><img
														src="../../cImages/pro11_7_1_img16.gif" alt="ROUND MESH electrolyzer">
												</div>
											</div>

											<div class="marginTop30px">

												<div class="col-md-3 imgWrap"><img src="../../cImages/pro11_4_1_img14.gif"
														alt="TURBO"></div>
												<div class="col-md-9">

													<div class="fw600 c36 fpx18">Strong TURBO functional
														water-Generation functional water for cleaning and disinfection
													</div>
													<div class="fpx14 line15" style="margin-top:22px;">
														The TURBO Function allows you to create strong alkaline and
														acidic water which are used for cleaning, sanitizing and<br>
														disinfecting purposes.
													</div>

												</div>

											</div>


											<table class="table pH_table fpx14 center mt50" style="width:100%">


												<tbody>
													<tr>
														<td class="pH_title pH_line_r" style="background:#fdfdfd;">Turbo
														</td>
														<td class="pH_title pH_line_r" style="background:#fdfdfd;">pH
														</td>
														<td class="pH_title" style="background:#fdfdfd;">Purpose of use
														</td>
													</tr>

													<tr>
														<td class="pH_line_r">TURBO Strong Alkaline Water</td>
														<td class="pH_line_r">pH 10.0 ~ 12.0</td>
														<td>Cleaning, disinfection</td>
													</tr>

													<tr>
														<td class="pH_line_r">TURBO Strong Acidic Water</td>
														<td class="pH_line_r">pH 4.0 ~ 2.5</td>
														<td>Disinfection</td>
													</tr>

												</tbody>
											</table>

											<div class="clearFix mt20 line14">
												<div class="itallic">
													* The waters produced in TURBO mode are not intended for
													drinking.<br>
													DO NOT DRINK these types of waters.
												</div>
											</div>


										</div>

									</div>

									<div id="snav-content3" aria-labelledby="ui-id-3" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div class="tab-con-box">

											<div class="col-dm-12 ">
												<div class="tab-box-title">
													<div class="tab-title">Functions</div>
												</div>
												<div class="center fblue fw600 fpx24 mt50">SMART WATER IONIZER</div>
												<div class="center fw600 fpx20" style="margin-top:5px;">UNDER COUNTER
													EXTREME TURBO</div>
												<div class="imgWrap mt50"><img src="../../cImages/pro11_7_1_img10.jpg"
														alt="SMART WATER IONIZER"></div>
												<div class="fw600 fpx16 fblue mt40">A DESIGN MASTERPIECE</div>
												<div class="fpx14 line14" style="margin-top:17px;">
													Offering both Kitchen Space and Ultimate Beauty.<br>
													The RETTIN Under Counter Extreme Water Ionizer not only attracts
													person’s gaze with its well-differentiated appearance,<br>
													but also helps to upgrade your kitchen’s interior decoration with a
													neat and luxurious expression.
												</div>
											</div>


											<div class="marginTop50px">
												<div class="col-md-6">
													<div class="pageTitle"><img src="../../cImages/sub_dot.gif" alt="">Full
														Touch Panel</div>
													<div class="fblue fw600 fpx16">TFT LCD display</div>
													<div class="fpx14 line14 mt20">A 262,000 Color TFT LCD Display
														provides diverse information in true<br>vivid colors for user
														convenience.</div>
													<div class="fblue fw600 fpx16 mt40">Full touch panel</div>
													<div class="fpx14 line14 mt20">
														The Full Touch Screen Panel System automatically activates the
														unit’s<br>
														operations through a simple touch to the desired icon displayed
														on<br>
														the screen.
													</div>
													<div class="fblue fw600 fpx16 mt40">GUI(Graphical User Interface)
													</div>
													<div class="fpx14 line14 mt20">
														A Graphical User Interface is designed specifically for ease of
														use and<br>
														operation. Communication between the user and ionizer has
														never<br>
														been this simple.
													</div>
												</div>
												<div class="col-md-6 imgWrap"><img src="../../cImages/pro11_7_1_img11.jpg"
														alt="Full Touch Panel"></div>
											</div>

											<div class="marginTop50px">
												<div class="col-md-6 imgWrap"><img src="../../cImages/pro11_7_1_img12.jpg"
														alt="Jog Dial &amp; Touch with 2 way Operation Compatibility">
												</div>
												<div class="col-md-6">
													<div class="fpx16 fw600 fblue">Jog Dial &amp; Touch with 2 way
														Operation Compatibility</div>
													<div class="fpx14 line14 mt20">
														Choose from either the Jog Dial or the Touch Screen Panel
														system–both are<br>
														available with 2 way operation compatibility and are easy to use
														at your<br>
														convenience.
													</div>
												</div>

											</div>
											<div class="col-md-12">
												<div class="fpx16 fw600 fblue mt50">Light and Screen Indicators for
													Water Discharge Detection</div>
												<div class="fpx14" style="margin-top:18px;">Changes to the LCD screen
													and Jog Dial light can be easily identified to check each selected
													phase.</div>
												<div class="imgWrap mt40"><img src="../../cImages/pro11_7_1_img13.jpg"
														alt="Light and Screen Indicators for Water Discharge Detection">
												</div>

											</div>
										</div>
									</div>

									<div id="snav-content4" aria-labelledby="ui-id-4" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="false">
										<div class="tab-con-box" style="padding-bottom:10px;">

											<div class="tab-box-title">
												<div class="tab-title">
													TYENT's Technological Prowess
												</div>
											</div>
											<div class="col-md-12">
												<div class="pageTitle"><img src="../../cImages/sub_dot.gif" alt="">
													Equipped with the most powerful turbo engine SMPS(10A)
												</div>
												<div class="">

													<div class=" fpx16 fwhite mt60">
														* Increased Electrolytic efficiency &amp; safety!
													</div>
													<div class=" fpx16 fwhite mt20">
														* EU CE Safety Management System CLASS2
													</div>
													<div class=" fpx16 fwhite mt20">
														* Strengthening of double insulation system electric safety
														standard
													</div>

												</div>
												<div class="pageTitle marginTop30px"><img src="../../cImages/sub_dot.gif"
														alt="">Generating of Alkaline water
													starts with a stable power supply</div>
												<div class="fpx14 line15 marginTop30px">
													By applying the Switching Mode Power Supply (SMPS)method, the AC
													Line frequency.
													(50Hz~60Hz) is converted into DC, changing it into a high
													frequency for use.
												</div>

												<div class="fw600 fpx16 fblue2 marginTop30px">Changes in the output
													voltage in
													correlation to changes in the input power</div>
												<div class="fpx14 mt20 line15">
													Because the input exchange voltage is changed into a high dc voltage
													and controlled at a high-
													frequency, no changes occur in the output voltage.
												</div>

												<div class="fw600 fpx16 fblue2 mt30 marginTop30px">Stable power supply
												</div>
												<div class="fpx14 mt20 line15">
													The supply of electrical power remains consistent even during
													continual use, thus maintaining
													optimal performance.
												</div>

												<div class="fw600 fpx16 fblue2 mt30 marginTop30px">Electrical safety
												</div>
												<div class="fpx14 mt20 line15">
													In case of over current or over voltage, it automatically cuts off the
													supply of electricity
													to the inside of the product.
												</div>
												<div class="text-center imgWrap marginTop30px"><img
														src="../../cImages/pro11_11_1_img22.gif" alt="certificate"></div>
											</div>

											<div class="col-md-12">
												<div class="pageTitle marginTop30px"><img src="../../cImages/sub_dot.gif"
														alt="">ROUND MESH
													Electrolyzer</div>
												<div class="fpx14">
													Electrolyzer is most important part in water ionizer. The difference
													of the
													surface treatment determines the electrolysis efficiency.
												</div>
												<div class="marginTop30px">
													<div class="imgWrap mt20"><img src="../../cImages/pro11_11_1_img23.gif"
															alt="ROUND MESH Electrolyzer"></div>
													<div class="fw600 fpx16 fblue2 marginTop30px">01. A platinum coated
														titanium mesh covers</div>
													<div class="fpx14 mt10 line15 ">
														Round-edge type for considering the characteristics of the
														electricity.
													</div>

													<div class="fw600 fpx16 fblue2 mt30 marginTop30px">02. Dualization
														of water inlet in
														electrolyzer</div>
													<div class="fpx14 mt10 line15">
														Prevention of internal pressure in electrolyzer and improvement
														of
														electrolysis effectiveness.
														Stable water outflow for alkaline water and acid water.
													</div>

													<div class="fw600 fpx16 fblue2 mt30 marginTop30px">03. Thread
														Mounted Types</div>
													<div class="fpx14 mt10 line15">
														Improvement of the precision between electrode and membrane
														structure.
													</div>
												</div>

											</div>




											<div class="col-md-12">
												<div class="pageTitle "><img src="../../cImages/sub_dot.gif"
														alt="">Self-diagnosis
													safety functions</div>
												<table class="table">
													<colgroup>
														<col width="123">
														<col width="397">
														<col width="123">
														<col width="">
													</colgroup>
													<tbody>
														<tr valign="top">
															<td><img src="../../cImages/pro11_1_1_img20.gif"
																	alt="Automatic Stop Function"></td>
															<td>
																<div class="fpx16 fblue2 fw600" style="margin-top:2px;">
																	Automatic Stop Function</div>
																<div class="fpx14 line14" style="margin-top:10px;">
																	Water outflow will stop to prevent overflow or
																	flooding. there is no need to wait if the unit
																	automatically shuts off. You can resume its use
																	immediately.
																</div>
															</td>
														</tr>
														</tr>
														<td><img src="../../cImages/pro11_1_1_img21.gif"
																alt="Temperature sensor">
														</td>
														<td>
															<div class="fpx16 fblue2 fw600" style="margin-top:2px;">
																Temperature sensor</div>
															<div class="fpx14 line14" style="margin-top:10px;">This unit
																is
																equipped with a temperature sensor which prevents the
																accidental inflow of hot water.</div>
														</td>
														</tr>

														<tr>
															<td colspan="4" height="40"></td>
														</tr>

														<tr valign="top">
															<td><img src="../../cImages/pro11_1_1_img22.gif"
																	alt="Sensing function of supplied raw water"></td>
															<td>
																<div class="fpx16 fblue2 fw600" style="margin-top:2px;">
																	Sensing
																	function of supplied raw water</div>
																<div class="fpx14 line14" style="margin-top:10px;">
																	The outflow of functional water stops automatically
																	when raw water is not supplied, preventing the
																	electrolytic cell and power supply unit from being
																	damaged due to current overflow, water from
																	being wasted and accidental water leaks.
																</div>
															</td>
														</tr>
														</tr>
														<td><img src="../../cImages/pro11_11_1_img24.gif"
																alt="Detects if filter door is open"></td>
														<td>
															<div class="fpx16 fblue2 fw600" style="margin-top:2px;">
																Detects
																if filter door is open</div>
															<div class="fpx14 line14" style="margin-top:10px;">
																When door is open, the water supply will be suspended
																for
																your safety.
															</div>
														</td>
														</tr>

													</tbody>
												</table>

											</div>



										</div>
									</div>

									<div id="snav-content5" aria-labelledby="ui-id-5" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div class="tab-con-box">
											<div class="col-md-12">
												<div class="tab-box-title">
													<div class="tab-title">Filter system</div>
												</div>
												<div class="center fpx24 fw600 mt50"><span class="fblue">2-filter
														system</span> makes clean water with abundant minerals<br>and
													removes harmful substances</div>

											</div>



											<div class="mt50">

												<div class=" imgWrap"><img src="../../cImages/pro11_7_1_img17.gif"
														alt="Filter system"></div>

												<div>

													<div class="pageTitle"><img src="../../cImages/sub_dot.gif"
															alt="">Membrane</div>
													<div class="fpx14 line15" style="margin-top:22px;">
														Pretreatment filtration function for filtering various
														precipitates contained in raw water.
													</div>

													<div class="pageTitle" style="margin-bottom:0px;"><img
															src="../../cImages/sub_dot.gif" alt="">Carbon Block</div>
													<div class="fpx14 line15" style="margin-top:22px;">
														Activated carbon was made into a block form has a larger surface
														area than particle-type activated carbon. It has a very high
														adsorption rate.
														It works to filter residual chlorine, organic compounds and
														heavy metals by activated adsorption method.
													</div>

													<div class="pageTitle" style="margin-bottom:0px;"><img
															src="../../cImages/sub_dot.gif" alt="">Calcium sulfite
													</div>
													<div class="fpx14 line15" style="margin-top:22px;">
														Removes residual chlorine completely by oxidation-reduction
														reaction.
													</div>

												</div>

												<div class="pageTitle" style="margin-bottom:0px;"><img
														src="../../cImages/sub_dot.gif" alt="">TM Ceramics</div>
												<div class="fpx14 mt20">The ceramics were made by mixing the
													high-emissivity far-infrared minerals containing potassium (k),
													sodium (Na) and calcium (Ca) with TM and TM-X stock solutions.
													These multifunctional ceramics have antioxidant, antimicrobial and
													far infrared radiation.</div>

												<div class="pageTitle" style="margin-bottom:0px;"><img
														src="../../cImages/sub_dot.gif" alt="">UF</div>
												<div class="fpx14 mt20">Many fine pores on the filter surface remove
													contaminants from the water.
													It removes bad elements such as bacteria viruses and particulates,
													but does not remove substances that are beneficial to our body, such
													as minerals.
												</div>
											</div>




										</div>
									</div>

								</div>

							</div>


						</div>
					</div>
				</div>

			</div>
		</div>


		<?php include('form.php')?>

	</section>
	<?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="../header/js/jquery.min.js"></script>
	<script src="../header/js/bootstrap.min.js"></script>
	<script src="../js/plugins.js"></script>
	<script src="../js/functions.js"></script>


	<!-- Main Js -->
	<script src="../header/js/main.js"></script>

	<script type="text/javascript" src="../js/xzoom.min.js"></script>
	<script type="text/javascript" src="../js/sticky-sidebar.js"></script>

	<script src="../js/customScript.js"></script>
	<script>$('#uceSeriesMenu').parent().addClass('active');</script>

	<script>
		$(function () {
			$("#specifications").hide();
			$("#side-navigation").tabs({
				show: {
					effect: "fade",
					duration: 400
				}
			});
		});
		$(".xzoom, .xzoom-gallery").xzoom({
			tint: '#333',
			Xoffset: 15,
			defaultScale: -0.3
		});
		var a = new StickySidebar('.sidebar', {
			topSpacing: 110,
			bottomSpacing: 20,
			containerSelector: '#features',
			innerWrapperSelector: '.sidebar__inner'
		});
		$('#productLi').addClass('active');
		$(".mobileCloseButton").click(function () {
			$('.ui-tabs-tab').toggle();
			$(".mobileCloseButton").html("<i class='icon-line-rewind'></i>");
			$(".mobileCloseButton").closest("ul.sidenav").toggleClass("transparentBG");
		});
	</script>
</body>

</html>