<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Page title -->
    <?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
    <!-- Bootstrap Core CSS -->
    <link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
        rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" href="css/dark.css" type="text/css" />
    <link rel="stylesheet" href="css/animate.css" type="text/css" />
    <link rel="stylesheet" href="css/responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/font-icons.css" type="text/css" />

    <!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
    <link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="customStyle.css">
    <style>
        .heading{
        font-family:Poppins;
        font-weight: normal;
        margin-bottom:8px;
        margin-top: -2px;
        font-size: 15px;
        color: #6ec1e4; 
        }
        .checkbox-inline{
            font-style: normal;
            font-family:Poppins;
            font-size: 15px;
            margin-bottom:6px;
            margin-top:0px;
        }
        .label{
            font-size: 23px;
            /* color:blue;*/
        }
    </style>

</head>

<body id="page-top">
<?php include("phpIncludes/header.php") ?>
<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
        $to_email = "edp@melu.in,sravan@medilightindia.com";
        $subject = 'Tyent Demo Feedback | From '.$_POST['fname1'];
        $message = "Hello Admin,\n\n Please find the below feedback details\n
Name : ".$_POST['name']."\nMobile Number : ".$_POST['mobile1']."\nEmail Adress : ".$_POST['email1']."
City : ".$_POST['city']."\nAdditional Information : ".$_POST['grade']."\n\n Regards,\nServer Admin";

$message = '<table style="background-color: #f2f8f9; max-width:670px; margin:0 auto;" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody><tr>
                        <td style="padding-top: 0px;">&nbsp;</td>
                    </tr>
                    <!-- Logo -->
                    <tr>
                        <td style="text-align:center;">
                            <a href="#" title="Demo Feedback Form"><img width="50" height="50" src="https://medilighthealthcare.com/wp-content/uploads/2019/10/logo-medd-500_500px.png" class="attachment-large size-large lazyloaded" style="padding-top:0px;" alt="" sizes="(max-width: 500px) 100vw, 500px" srcset="https://medilighthealthcare.com/wp-content/uploads/2019/10/logo-medd-500_500px.png 500w, https://medilighthealthcare.com/wp-content/uploads/2019/10/logo-medd-500_500px-150x150.png 150w, https://medilighthealthcare.com/wp-content/uploads/2019/10/logo-medd-500_500px-300x300.png 300w" data-ll-status="loaded" title="Demo Feedback Form" alt="Demo Feedback Form" ></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height:40px;">&nbsp;</td>
                    </tr>
                    <!-- Email Content -->
                    <tr>
                        <td>
                            <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" style="max-width:670px; background:#fff; border-radius:10px; -webkit-box-shadow:0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 1px 3px 0 rgba(0, 0, 0, 0.12);-moz-box-shadow:0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 1px 3px 0 rgba(0, 0, 0, 0.12);box-shadow:0 1px 3px 0 rgba(0, 0, 0, 0.16), 0 1px 3px 0 rgba(0, 0, 0, 0.12); padding:0 40px;">
                                <tbody><tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                                <!-- Title -->
                                <tr>
                                    <td style="padding:0 15px; text-align:center;">
                                        <h1 style="color:#3075BA; font-weight:400; margin:0;font-size:32px;">Demo Feedback Form</h1>
                                        <span style="display:inline-block; vertical-align:middle; margin:29px 0 26px; border-bottom:1px solid #cecece; 
                                        width:100px;"></span>
                                    </td>
                                </tr>
                                <!-- Details Table -->
                                <tr>
                                    <td>
                                        <table cellpadding="0" cellspacing="0" style="width: 100%; border: 1px solid #ededed">
                                            <tbody>
                                                <tr>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; border-right: 1px solid #ededed; width: 35%; font-weight:500; color:#171f23de">Name:</td>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; color: rgba(23,31,35,.87);">'.$_POST['fname1'].'</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed;border-right: 1px solid #ededed; width: 35%; font-weight:500; color:#171f23de">Contact Number:</td>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; color: rgba(23,31,35,.87);">'.$_POST['mobile1'].'</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 10px;  border-bottom: 1px solid #ededed; border-right: 1px solid #ededed; width: 35%;font-weight:500; color:#171f23de">Email:</td>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; color: rgba(23,31,35,.87);">'.$_POST['email1'].'</td>
                                                </tr>
                                                <tr>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; border-right: 1px solid #ededed; width: 35%;font-weight:500; color:#171f23de">Location:</td>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; color: rgba(23,31,35,.87); ">'.$_POST['location'].'</td>
                                                </tr>
                                                 <tr>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; border-right: 1px solid #ededed; width: 35%;font-weight:500; color:#171f23de">Demonstration Grade:</td>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; color: rgba(23,31,35,.87); ">'.$_POST['grade'].'</td>
                                                </tr>
                                                 <tr>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; border-right: 1px solid #ededed; width: 35%;font-weight:500; color:#171f23de">Test During Demo:</td>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; color: rgba(23,31,35,.87); ">'.implode(", ",$_POST['test']).'</td>
                                                </tr>
                                                 <tr>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; border-right: 1px solid #ededed; width: 35%;font-weight:500; color:#171f23de">Explained points:</td>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; color: rgba(23,31,35,.87); ">'.implode(", ",$_POST['explanation']).'</td>
                                                </tr>
                                                 <tr>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; border-right: 1px solid #ededed; width: 35%;font-weight:500; color:#171f23de">Model Intrested In:</td>
                                                    <td style="padding: 10px; border-bottom: 1px solid #ededed; color: rgba(23,31,35,.87); ">'.$_POST['model'].'</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height:40px;">&nbsp;</td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr> 
                    <tr>
                        <td style="height:20px;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">
                             <p style="font-size:14px; color:#455056bd; line-height:18px; margin:0 0 0;">Powered by <strong>Medilight Pvt Ltd</strong>.</p>
                        </td>
                    </tr>
                </tbody></table>';


        $headers = 'From: info@tyent.co.in';
        $headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
        if (mail($to_email, $subject, $message, $headers)) { 
            http_response_code(200); 
            ?>
            
            
<?PHP
          
        } else {
            //http_response_code(501);
            //echo  "Error sending mail! Contact administrator.";
        }
    } else {
        //echo "GET NOT SUPPORTED";
       // http_response_code(501);
    }
?>

    
    <section id="content ">
        <div class="page headMargin poppins" id="top">
            
            </div>
           
          <!--  <?php
           if ($_SERVER["REQUEST_METHOD"] == "POST") { ?>
            <div class="alert alert-success">
  <strong>We got your Request! We will get back to you with in 24 hours or even early!
</div>


           <?php }
           ?> -->
          

            <section class="contactBgGen paddingTopBottom dark" style="padding-top: 16px;">
    <div class="container relative">
        
        <div class="row">
            <div class="col-md-8 col-md-offset-2 align-center">
                
                <!-- Section Titles -->
                <h1 class="section-title large poppins" style="padding-bottom: 16px;">Demo Feedback !</h1>
                <!-- <h2 class="section-heading mb-40 poppins">Just fill the form we will get in touch</h2> -->
                <!-- End Section Titles -->
                 <?php
           if ($_SERVER["REQUEST_METHOD"] == "POST") { ?>
            <div class="alert alert-success">
  <strong><!-- We got your Request! We will get back to you with in 24 hours or even early! -->We got your Feedback.Thanks for your response.
</div>


           <?php }
           ?>
                
            </div>
        </div>                    
        
        <!-- Contact Form -->                            
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                
                <form class="form contact-form" method="post" id="" autocomplete="off">
                    <div class="clearfix">
                        
                        <div class="col-md-6">
                            
                            <!-- Name -->
                            <div class="form-group">
                                <input type="text" name="fname1" id="name" class="input-lg form-control" placeholder="Name" required="">
                            </div>
                            
                            <!-- Email -->
                            <div class="form-group">
                                <input type="email" name="email1" id="email" class="input-lg form-control" placeholder="Email" required="">
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                            
                            <!-- Name -->
                            <div class="form-group">
                                <input type="text" name="mobile1" id="number" class="input-lg form-control" placeholder="Mobile Number" required="">
                            </div>
                            
                            <!-- Email -->
                            <div class="form-group">
                                <input type="text" list="cars" name="location" id="city" class="input-lg form-control" placeholder="Location" required="">
                                
                            </div>
                            
                        </div>
                        <div class="col-md-6">
                             <div class="form-group">
                                <input type="text"  name="model" id="model" class="input-lg form-control" placeholder=" Model No" required="">
                                
                            </div>
                        </div>

                        <div class="col-md-6">
                           
                                  <h5 class="heading" style=" color: #6ec1e4;  ">1. How was the demonstration given by marketing manager.</h5>
                              
                              
                       <h5 class="checkbox-inline"><input type="radio" name="grade" value="excellent"> Excellent</h5>
                   
                   
                    <h5 class="checkbox-inline"><input type="radio" name="grade" value="average"> Average</h5>
                
                
                    <h5 class="checkbox-inline"><input type="radio" name="grade" value="uptomark"> Not up-to the mark</h5>
                
                        </div>
                    </div>
                        
                        <div class="col-md-6 ">
                            <table>
                            <tr>
                            <td colspan="4" class="heading">2. Did the marketing manager performed below tests during demo ?</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="test[]" value="ph"> pH test</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="test[]" value="orp"> ORP test</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="test[]" value="phlevel">Demonstration of different water pH levels.</td>
                        </tr>
                        <tr>

                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="test[]" value="penetration">Penetration test with jeera/Turmaric powder.</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="test[]" value="blow">Blow test.</td>
                        </tr>
                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="test[]" value="cleaning"> Vegetable cleaning test.</td>
                        </table>
                        </div>

                       
                        <div class="col-md-6">
                            <table>
                            <tr>
                            <td colspan="4" class="heading">3. Did the marketing manager explained about below points</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="explanation[]" value="ionized"> RO vs Ionized water</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="explanation[]" value="orp"> ORP</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="explanation[]" value="pH"> pH</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="explanation[]" value="alkaline">Ionized hydrogen rich alkaline water benifits.</td>
                        </tr>
                        <tr>
                            <td colspan="4" class="checkbox-inline"><input type="checkbox" name="explanation[]" value="helath">Health benifits.</td>
                        </tr>
                    </table>
                            

                        </div>

                    </div>
                   <!--  <div class="col-md-6">
                            <h5 class="label">Thank You !</h5>
                        </div> -->
                        <!-- <div class="col-md-6">
                             <div class="form-group">
                                <input type="text"  name="model" id="model" class="input-lg form-control" placeholder=" Model No" required="">
                                
                            </div>
                        </div>
 -->
                        
                        <!-- <div class="col-md-12">
                            <div class="form-group">                                            
                                <textarea name="message" id="message" class="input-lg form-control" style="height: 120px;" placeholder="Message"></textarea>
                            </div>
                            
                        </div> -->
                        
                    </div>
                    
                    <div class="clearfix">
                        
                        <div class="col-md-6">                                      
                            <div class="form-tip pt-5 poppins">
                               <!--  <span>*</span></i> All the fields are required -->
                            </div>
                            
                        </div> 
                        
                        <div class="col-md-6">
                            
                            <!-- Send Button -->
                            <div class="align-right floatRight pt-5 poppins dark">
                            <button type="submit" class="button button-3d button-large button-rounded button-green"><i class="icon-angle-right"></i><span>Submit</span></button>
                            </div>
                            
                        </div>
                        
                    </div>
                    
                    
                </form>
                <div id="dynaMessage" ></div>

            </div>
        </div>
        <!-- End Contact Form -->
        
    </div>
</section>




        </div>

    </section>
    <?php include("phpIncludes/footer.php") ?>

    <!-- /footer ends -->
    <!-- Core JavaScript Files -->
    <script src="header/js/jquery.min.js"></script>
    <script src="header/js/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/functions.js"></script>

  
    <!-- Main Js -->
    <script src="header/js/main.js"></script>

    
    <script src="js/customScript.js"></script>


</body>

</html>