<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="customStyle.css">

</head>

<body id="page-top">
<?php include("phpIncludes/header.php") ?>

	<!-- /navbar ends -->

	
	<section id="content">
        <div class="aboutUsHead headMargin">
            <div class="container-fluid">
                <div class="col-md-6"></div>
                <div class="emphasis-title col-md-6 text-right dark">
                    <h2 class="uppercase poppins">About Tyent</h2>
                    <p class="lead topmargin-sm poppins">Tyent has been the leader in manufacturing water- ionization systems that transform regular tap water into pure, healthy, electrolytically-reduced and hydrogen-rich drinking water.
                    </p><p  class="lead topmargin-sm poppins">TAEYOUNG E&T CO. LTD is a Korean based water ionizer manufacturing company that sells its products in 32 countries across the world.</p>
                </div>
            </div>
        </div>
        <div class="container paddingTopBottom">
            <div class="emphasis-title col-md-12 text-center">
                <h2 class="uppercase poppins">About Tyent India</h2>
                <p class="lead topmargin-sm poppins">
                    <span>Tyent India</span> is managed by <span>Medilight private Limited</span> who is the leader in Hydrogen rich alkaline water ionizers space.
                    </p>
                    <p  class="lead topmargin-sm poppins">
                    Medilight private Limited is a core group imbibed with a commitment to excel in health & medical equipment space having a gritty passion that believes in fostering the business through Innovation and Technology. Based in Hyderabad and spinning more than 25+ delightful center's across India, Backed up by several years of industry expertise par excellence, our network of teams is comprised of a privileged set of highly motivated specialists whose relentless strive in this domain has made Medilight stand out as a leader in Hydrogen rich alkaline water ionizers space with a motivation to provide enhanced and customized products to prosper Relationships having a sense of Profitability.
                    </p>
            </div>
        </div>
        <div class="container paddingBottom">
            <div class="heading-block center">
                <h3 class="poppins letterSpacing2px fontWeight100px">Our Locations</h3>
            </div>
            <div class="col-md-6">
                <div id="map"></div>
            </div>
            <div class="col-md-6">
                <div class="emphasis-title col-md-12 text-left">
                    <ul>
                        <p class="poppins fontSize20px bounceInLeft animated" data-animate="bounceInLeft">
                            Tyent ionizers, the world's most advanced water ionizers with all certifications like GMP, KFDA & ISO 13485 which are available in more than 32 counties.
                        </p>
                        <p class="poppins fontSize20px bounceInRight animated delay-1s" data-animate="bounceInRight">
                        We are present in all major states within India.
                        </p>
                        <p class="poppins  fontSize20px bounceInLeft animated delay-2s" data-animate="bounceInLeft">
                        Currently we are serving with 25+ delightful center's across India to deliver the Tyent world class ionizer's sales & after sales service to customer doorstep.
                        </p>
                        <p class="poppins  fontSize20px bounceInRight animated delay-3s" data-animate="bounceInRight">
                            We are proud to mention that we are the only company in Ionizer industry to provide after sales service at customer’s doorstep.
                        </p>
                        <p class="poppins  fontSize20px bounceInRight animated delay-4s" data-animate="bounceInRight">
                        We believe in <span>"Every company’s greatest assets are its customers because without customers there is no company"</span> -  Michael LeBoeuf.
                        </p>
                    </ul>
                </div>
            </div>
            
        </div>

        <section class=" team pb-0">
            <div class="team__bg"><img class="section--bg t50 r0" src="cImages/testimonials-bg.png" alt="img"></div>
            <div class="container">
                <div class="heading-block center">
                    <h3 class="poppins letterSpacing2px fontWeight100px">Our Team</h3>
                </div>
                <div class="row align-items-center offset-50">
                    <div class="col-md-4 col-sm-4">
                        <div class="team-item">
                            <div class="team-item__img"><img class="img--bg" src="images/team/3.jpg" alt="team"></div>
                            <div class="team-item__description">
                                <h6 class="team-item__name">DR.SRINIVASA YADAV KANDULA</h6><span class="team-item__position">MD</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="team-item">
                            <div class="team-item__img"><img class="img--bg" src="images/team/2.jpg" alt="team"></div>
                            <div class="team-item__description">
                                <h6 class="team-item__name">DR. SREE SUMA GOPIPARTHY</h6><span class="team-item__position">Chairman</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="team-item">
                            <div class="team-item__img"><img class="img--bg" src="images/team/8.jpg" alt="team"></div>
                            <div class="team-item__description">
                                <h6 class="team-item__name">MR. SANTOSH KUMAR ANUVALSETTY</h6><span class="team-item__position">Executive Finance Director</span>
                            </div>
                        </div>
                    </div>
                 
                </div>
            </div>
        </section>

	</section>
    <?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="header/js/jquery.min.js"></script>
	<script src="header/js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/functions.js"></script>

  
	<!-- Main Js -->
    <script src="header/js/main.js"></script>
    <script src="js/mapdata.js"></script>
    <script src="js/countrymap.js"></script>
	
	<script src="js/customScript.js"></script>
<script>
    		$('#aboutLi').addClass('active');

</script>

</body>

</html>