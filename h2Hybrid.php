<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('../seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="../header/css/bootstrap.css"  type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../style.css" type="text/css" />
	<link rel="stylesheet" href="../css/dark.css" type="text/css" />
	<link rel="stylesheet" href="../css/animate.css" type="text/css" />
	<link rel="stylesheet" href="../css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="../css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="../include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="../include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="../css/xzoom.css" media="all" />

	<link rel="stylesheet" type="text/css" href="../customStyle.css">
<style>
.pd-t-20{
	padding-top:20px
}
</style>
</head>

<body id="page-top">
	<?php include("../phpIncludes/header.php") ?>

	<!-- /navbar ends -->
	<section id="content">

<div class="container headMargin pd-t-20">
	<div class="col-md-12 paddingMobileLR5px">
		<div class="col-md-6 productImages">
			<img class="xzoom" id="main_image" src="../images/combo_01.jpg" xoriginal="../images/combo_01.jpg">

			<div class="xzoom-thumbs marginTop50px">
				<a href="../images/combo_01.jpg">
					<img class="xzoom-gallery" width="60" src="../images/combo_01.jpg">
				</a>
				<a href="../images/combo_01.jpg">
					<img class="xzoom-gallery" width="60" src="../images/combo_02.jpg">
				</a>

				<a href="../images/hybrid-front.png">
					<img class="xzoom-gallery" width="60" src="../images/hybrid-front.png">
				</a>

				<a href="../images/hybrid_with_glass_backsplash-v2.jpg">
					<img class="xzoom-gallery" width="60" src="../images/hybrid_with_glass_backsplash-v2.jpg">
				</a>
			</div>
		</div>
		<div class="col-md-6 paddingMobileLR5px">
			<div class="text- pro-infor marginTop10px">

				<div class="pl-md-5 pt-md-5 pb-md-5 p-3"
					style="background: transparent linear-gradient(180deg, #E8F7FF 0%, #FFFFFF 100%) 0% 0% no-repeat padding-box;">
					<h1 class="text-primary display-4" style="font-weight: 900; font-family: Montserrat;">H2
						Hybrid <img style="max-width: 130px;" src="../images/on-sale.png" alt="On Sale"></h1>
					<p class="">
						<span class="text-primary h2 font-weight-bold">₹3,15,000/-</span>
					</p>
					<h2 class="">2021 Model Above-Counter Hybrid Ionizer</h2>

					<div id="short_description">
						NEW - Alkaline H2 Hybrid Ionizer
					</div>
					<div id="more_info_link">
						<a href="#" id="specificLink" data-id="long_description"><i class="fa fa-angle-double-down"></i> Product detail and
							specifications</a>
					</div>

				   

				</div>
			</div>

		</div>
	</div>

</div>

<div class="container">

	<div class="row pb-5">
		<div class="col-xl-10 offset-xl-1">

		<div class="row pt-md-5 pb-5 marginTop50px" style="margin-bottom:20px;font-size:15px">
                        <div class="col-md-4 text-center text-md-right">
                            <img src="../images/trial75.png" alt="75-Day Trial">
                            <div><b>World's 1st Alkaline Hydrogen Hybrid Water Ionizer</b></div>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="../images/lifetimewarranty.png" alt="Lifetime Warranty">
                            <div><b>3 Years warranty on Entire Ionizer & 15 Years on ionization chamber</b></div>

                        </div>
                        <div class="col-md-4 text-center text-md-left " style="margin-top:15px">
                            <img  src="../images/satisfactionguarantee.png" alt="100% Satisfaction Guaranteed!">
                            <div ><b>100% Satisfaction Guaranteed!</b></div>
                            
                        </div>
                    </div>

			<div class="row">
				<div class="col-12">

					<script src="https://fast.wistia.com/embed/medias/8neo5wrrap.jsonp" async></script>
					<script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
					<div class="wistia_responsive_padding" style="padding:56.25% 0 0 0;position:relative;">
						<div class="wistia_responsive_wrapper"
							style="height:100%;left:0;position:absolute;top:0;width:100%;">
							<div class="wistia_embed wistia_async_8neo5wrrap videoFoam=true"
								style="height:100%;position:relative;width:100%">
								<div class="wistia_swatch"
									style="height:100%;left:0;opacity:0;overflow:hidden;position:absolute;top:0;transition:opacity 200ms;width:100%;">
									<img src="https://fast.wistia.com/embed/medias/8neo5wrrap/swatch"
										style="filter:blur(5px);height:100%;object-fit:contain;width:100%;"
										alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" />
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<img src="../images/hybrid-1.png" alt="First Ever Alkaline Hydrogen Hybrid Water Ionizer!">
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<img src="../images/hybrid-2.png" alt="LCD Display and Touch Screen">
				</div>
			</div>

			<div class="row d-none d-md-flex py-5" style="">
				<div class="col-md-4 offset-md-2 text-center">

				</div>
				<div class="col-6 text-left">
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<img src="../images/hybrid-3.png"
						alt="Alkaline Hydrogen Hybrid Water Ionizer with Unrivaled Technology!">
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<img src="../images/hybrid-4.png" alt="Convenient Use with ONE TOUCH Technology">
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<img src="../images/hybrid-5.png" alt="JOG DIAL for Hydrogen water">
				</div>
			</div>

			<div class="row d-none d-md-flex py-5" style="">
				<div class="col-md-4 offset-md-2 text-center">

				</div>
				<div class="col-6 text-left">
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<img src="../images/hybrid-6.png" alt="Fixed Quantity Selection Function!">
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<img src="../images/hybrid-7.png" alt="Powerful Dual Filtration System!">
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<img src="../images/hybrid-8.png" alt="membrane multi-layer filtration">
				</div>
			</div>

			<div class="row d-none d-md-flex py-5" style="">
				<div class="col-md-4 offset-md-2 text-center">

				</div>
				<div class="col-6 text-left">
				</div>
			</div>

			<div class="row">
				<div class="col-12">
					<img src="../images/hybrid-9.png" alt="Front-Load Filter Replacements">
				</div>
			</div>

			<div class="row" id="long_description">
				<div class="col-12">
					<img src="../images/hybrid-10.png" alt="H2 Hybrid Specifications">
				</div>
			</div>
			<div class="row d-none d-md-flex py-5" style="">
				<div class="col-md-4 offset-md-2 text-center">
					<a href="/easy-pay-monthly-payment-plan-application/">
					</a>
				</div>
				<div class="col-6 text-left">
				</div>
			</div>
		</div>
	</div>
</div>



<?php include('../form.php')?>

</section>
	<?php include("../phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="../header/js/jquery.min.js"></script>
	<script src="../header/js/bootstrap.min.js"></script>
	<script src="../js/plugins.js"></script>
	<script src="../js/functions.js"></script>


	<!-- Main Js -->
	<script src="../header/js/main.js"></script>

	<script type="text/javascript" src="../js/xzoom.min.js"></script>
	<script type="text/javascript" src="../js/sticky-sidebar.js"></script>

	<script src="../js/customScript.js"></script>
	<script>$('#uceSeriesMenu').parent().addClass('active');</script>

	<script>
		$(function () {
			$("#specifications").hide();
			$("#side-navigation").tabs({
				show: {
					effect: "fade",
					duration: 400
				}
			});
		});
		$(".xzoom, .xzoom-gallery").xzoom({
			tint: '#333',
			Xoffset: 15,
			defaultScale: -0.3
		});
		$('#productLi').addClass('active');
		$(".mobileCloseButton").click(function () {
			$('.ui-tabs-tab').toggle();
			$(".mobileCloseButton").html("<i class='icon-line-rewind'></i>");
			$(".mobileCloseButton").closest("ul.sidenav").toggleClass("transparentBG");
		});
		$('#specificLink').click(function(e){
			console.log('dd');
			e.preventDefault();
			$('html, body').animate({scrollTop: $('#long_description').offset().top -100 }, 500);
		});
	</script>
</body>

</html>