<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link href="header/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="stylesheet" href="css/dark.css" type="text/css" />
	<link rel="stylesheet" href="css/animate.css" type="text/css" />
	<link rel="stylesheet" href="css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="customStyle.css">

</head>

<body id="page-top">
	
	<!-- /navbar ends -->
	<?php include("phpIncludes/header.php") ?>

	
	<section class=" headMargin techBGHead">
		<div class="relative container align-left">
			
			<div class="row">
				
			
			</div>
			
		</div>
	</section>
	<section id="content " >
		
        <div class="container paddingBottom ">
            <div class="SubContentsArea">
                <div class="subBox">	    
					<div class="pageTitle" style="margin-bottom:30px;"><img src="cImages/sub_dot.gif" alt="">SMPS Power Supply Device (Patent no. 10-0714055)</div>	
                    <div class="imgWrap"><img src="cImages/sub01_01_02_img01.gif" alt="SMPS Power Supply Device"></div>
                    
                    <div class="clearFix " style="margin-top:55px;">
                        
                        <div class="">
                            
                            <div class="fpx16 fblue fw600" style="letter-spacing:0px;">
                            By applying the Switching Mode Power Supply (SMPS)method,
                            the AC Line frequency.(50Hz~60Hz) is converted into DC,
                            changing it into a high frequency for use. 
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <ul class="imgWrap" style="margin-top:30px;">
                                
                                        <li class="clearFix dFlex">
                                            <div class="text-left fpx16 marginLeft5px">
                                                <div class="fw600" style="margin-bottom:6px;line-height:1.1;"><img src="cImages/sub01_01_02_no01.gif" alt="01">Changes in the output voltage in correlation to changes in the input power</div>
                                                <span class="fpx14 l-s05 line14">Because the input exchange voltage is changed into a high dc voltage and controlled at a high-frequency,no changes occur in the output voltage.</span>
                                            </div>
                                        </li>
                                        
                                        <li class="clearFix mt15 dFlex">
											<div class="text-left fpx16 marginLeft5px">
                                                <div class="fw600" style="margin-bottom:6px;line-height:1.1;"><img src="cImages/sub01_01_02_no02.gif" alt="01">Stable power supply</div>
                                                <span class="fpx14 l-s05 line14">The supply of electrical power remains consistent even during continual use, thus maintaining optimal performance.</span>
                                            </div>
										</li>
										<li class="clearFix mt15 dFlex">
                                            <div class="text-left fpx16 marginLeft5px">
                                                <div class="fw600" style="margin-bottom:6px;line-height:1.1;"><img src="cImages/sub01_01_02_no03.gif" alt="01">Electrical safety</div>
                                                <span class="fpx14 l-s05 line14">in the event that the product malfunctions due to leakage or short-circuiting that could potentially
													lead to the flow of over-current or over-voltage, the SMPS to prevent the flow currents or voltage
													above a certain determined level automatically blocks the supply of electricity from reaching the
													internals of the product..</span>
                                            </div>
                                        </li>                                    
                                    </ul>	
                                </div>
                                <div class="col-md-4">
                                    <div class="imgWrap" ><img src="cImages/sub01_01_02_img02.gif" alt="certificate"></div>

                                </div>
                            </div>
                           		
                            
                        </div>
                        
                    
                    </div>
                    
                    <div class="sub_space"></div>
                    
                    <div class="pageTitle" style="margin-bottom:50px;"><img src="cImages/sub_dot.gif" alt="">Electrolysis tank</div>
                    <div class="imgWrap"><img src="cImages/sub01_01_02_img03.gif" alt="Electrolysis tank"></div>
                    
                    <div class="fpx16 fblue fw600 mt50" style="margin-top: 10px;">Electrolyzer is most important part in water ionizer. The difference of the surface treatment determines the electrolysis efficiency. </div>	
                    <ul class="imgWrap" style="margin-top:30px;">
                
                        <li class="clearFix">
                            <div class="text-left fpx15">
                                <div class="fw600"><img src="cImages/sub01_01_02_no01.gif" alt="01">A platinum coated titanium mesh covers</div>
                                
                                <div class="clearFix mt20">
                                    <div class="text-left">
                                        <div class="fw600">Round-edge type for considering the characteristics of the electricity</div>
                                        <div class="mt7">Prevention of gas layer adhesion due to high-efficiency electrolysis performance to the entire electrode plate</div>
                                    </div>
                                </div>
                                
                                <div class="clearFix mt20">
                                    <div class="text-left">
                                        <div class="fw600">Broad surface area</div>
                                        <div class="mt7">Coated products have inconsistent surfaces, making its actual surface area broader than that of electroplated products.</div>
                                    </div>
                                </div>
                                
                                <div class="clearFix mt20">
                                    <div class="text-left">
                                        <div class="fw600">High-efficiency electrolysis</div>
                                        <div class="mt7">The use of low voltage enables high-efficiency electrolysis performance.</div>
                                    </div>
                                </div>
                                
                            </div>
                        </li>
                
                        <li class="clearFix mt40">
                            <div class="text-left fpx15">
                                <div class="fw600" style="margin-bottom:6px;"><img src="cImages/sub01_01_02_no02.gif" alt="01">Dualization of water inlet in electrolyzer</div>
                                <span class="fpx14 mt7 line14">Prevention of internal pressure in electrolyzer and improvement of electrolysis effectiveness.Stable water outflow for alkaline water and acid water. </span>
                                </div>
                        </li>
                
                        <li class="clearFix mt40">
                            <div class="text-left fpx15">
								<div class="fw600" style="margin-bottom:6px;"><img src="cImages/sub01_01_02_no03.gif" alt="01">Thread Mounted Types</div><span class="fpx14">Improvement of the precision between electrode and membrane structure.</span></div>
                        </li>		
                
                    </ul>
                    
                    <div class="sub_space"></div>
                    
                    <div class="pageTitle"><img src="cImages/sub_dot.gif">FILTER SYSTEM</div>
                    <div class="clearFix mt50">
                    
                        <div class="row">
                            <div class="col-md-6">
                                <div class="imgWrap text-left width100p"><img src="cImages/sub01_01_02_img06.gif" alt="FILTER SYSTEM"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="textMobRight" >
                                    <div class="pageTitle" style="margin:0px;margin-bottom:22px;"><img src="cImages/sub_dot.gif">Membrane</div>
                                    <div class="fpx14 line15">
                                    Pretreatment filtration function for filtering various precipitates contained in raw water.
                                    </div>
                                    <div class="pageTitle" style="margin:0px;margin-bottom:22px;margin-top:40px;"><img src="cImages/sub_dot.gif">Carbon Block</div>
                                    <div class="fpx14 line15">
                                    Activated carbon was made into a block form has a larger surface area than particle-type activated carbon. It has a very high adsorption rate.
                                    It works to filter residual chlorine, organic compounds and heavy metals by activated adsorption method
                        
                                    </div>
                                    <div class="pageTitle" style="margin:0px;margin-bottom:22px;margin-top:40px;"><img src="cImages/sub_dot.gif">Calcium sulfite</div>
                                    <div class="fpx14 line15">
                                    Removes residual chlorine completely by oxidation-reduction reaction
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    
                    </div>
                    
                    <div class="pageTitle" style="margin:0px;margin-top:30px;"><img src="cImages/sub_dot.gif">TM Ceramics</div>
                    <div class="fpx14" style="margin-top:25px;">The ceramics were made by mixing the high-emissivity far-infrared minerals containing potassium (k), sodium (Na) and calcium (Ca) with TM and TM-X stock solutions.
                These multifunctional ceramics have antioxidant, antimicrobial and far infrared radiation.</div>
                    <div class="pageTitle" style="margin-bottom:0px;"><img src="cImages/sub_dot.gif">UF</div>
                    <div class="fpx14" style="margin-top:25px;">Many fine pores on the filter surface remove contaminants from the water.
                It removes bad elements such as bacteria viruses and particulates, but does not remove substances that are beneficial to our body, such as minerals.</div>
                    
                    
                    <div class="sub_space"></div>
                    
                    <div class="pageTitle" style="margin-bottom:26px;"><img src="cImages/sub_dot.gif">Super Simple Filter Replacement System</div>
                    <div class="fpx14">
                        <div class="fpx16 fblue fw600" style="margin-bottom:5px;">Open and close type door &amp; simple filter replacement system (Patent no. 10-0831027)</div>
                        A simple filter replacement system along with on-touch Filter Usage Initialization Setting System makes filter management easy for any one.
                    </div>
                    
                    <div class="imgWrap mt50"><img src="cImages/sub01_01_02_img05.gif" alt="Super Simple Filter Replacement System"></div>
                    
                </div></div>
        </div>

    </section>
    <?php include('form.php')?>

	<?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="header/js/jquery.min.js"></script>
	<script src="header/js/bootstrap.min.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/functions.js"></script>

  
	<!-- Main Js -->
    <script src="header/js/main.js"></script>
	
	<script src="js/customScript.js"></script>


</body>

</html>