<?php 
	$plates = "NULL";
	$plates = $p;
	
	$price = "NULL";
	$imgs = "NULL";
	$img=1;
	$plateDesc = "NULL";
	if($plates==5){
		$plateDesc = '<ul class="swiperList">
		<li><span>Electrode Plates:</span> 5</li>
		<li><span>Generates:</span> 7 types of water</li>
		<li><span>pH Level:</span> 11.5 to 2.5<i class="redStar">*</i></li>
		<li><span>ORP Level:</span> -500 to -700<i class="redStar">*</i> </li>
	</ul>';
		$price = "1,53,000";
		$plateName = "NMP 5 Plate";
		$imgs = '<a href="../../cImages/prod/nmpg1.png">
					<img class="xzoom-gallery" width="60" src="../cImages/prod/nmpg1.png">
				</a>
				<a href="../../cImages/prod/nmpg2.png">
					<img class="xzoom-gallery" width="60" src="../cImages/prod/nmpg2.png">
				</a>';
	}
	if($plates==7){
		$price = "1,85,000";
		$plateName = "NMP 7 Plate";
		$imgs = '<a href="../../cImages/prod/nmpg1.png">
					<img class="xzoom-gallery" width="60" src="../cImages/prod/nmpg1.png">
				</a>
				<a href="../../cImages/prod/nmpg2.png">
					<img class="xzoom-gallery" width="60" src="../cImages/prod/nmpg2.png">
				</a>';
		$plateDesc = '<ul class="swiperList">
		<li><span>Electrode Plates:</span> 7</li>
		<li><span>Generates:</span> 7 types of water</li>
		<li><span>pH Level:</span> 11.5 to 2.5<i class="redStar">*</i></li>
		<li><span>ORP Level:</span> -500 to -800<i class="redStar">*</i> </li>
			</ul>';
	}
	if($plates==9){
		$img=3;
		$price = "2,17,000";
		$plateName = "NMP 9 Plate";
		$imgs = '<a href="../../cImages/prod/nmpg3.png">
					<img class="xzoom-gallery" width="60" src="../cImages/prod/nmpg3.png">
				</a>

				<a href="../../cImages/prod/nmpg4.png">
					<img class="xzoom-gallery" width="60" src="../cImages/prod/nmpg4.png">
				</a>';
		$plateDesc = '<ul class="swiperList">
				<li><span>Electrode Plates:</span> 9</li>
				<li><span>Generates:</span> 7 types of water</li>
				<li><span>pH Level:</span> 11.5 to 2.5<i class="redStar">*</i></li>
				<li><span>ORP Level:</span> -500 to -1000<i class="redStar">*</i> </li>
			</ul>';
	}
	if($plates==11){
		$img=3;
		$price = "2,48,000";
		$plateName = "NMP 11 Plate";
		$imgs = '<a href="../../cImages/prod/nmpg3.png">
					<img class="xzoom-gallery" width="60" src="../cImages/prod/nmpg3.png">
				</a>

				<a href="../../cImages/prod/nmpg4.png">
					<img class="xzoom-gallery" width="60" src="../cImages/prod/nmpg4.png">
				</a>';
		$plateDesc = '<ul class="swiperList">
				<li><span>Electrode Plates:</span> 11</li>
				<li><span>Generates:</span> 7 types of water</li>
				<li><span>pH Level:</span> 11.5 to 2.5<i class="redStar">*</i></li>
				<li><span>ORP Level:</span> -500 to -1200<i class="redStar">*</i> </li>
			</ul>';
	}
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Page title -->
	<?php include('seoTags.php');echo ${basename(__FILE__, '.php')};?><link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--[if lt IE 9]>
      <script src="../js/respond.js"></script>
      <![endif]-->
	<!-- Bootstrap Core CSS -->
	<link rel="stylesheet" href="../header/css/bootstrap.css"  type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800%7COpen+Sans:400,700,800"
		rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../style.css" type="text/css" />
	<link rel="stylesheet" href="../css/dark.css" type="text/css" />
	<link rel="stylesheet" href="../css/animate.css" type="text/css" />
	<link rel="stylesheet" href="../css/responsive.css" type="text/css" />
	<link rel="stylesheet" href="../css/font-icons.css" type="text/css" />

	<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="../include/rs-plugin/css/settings.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../include/rs-plugin/css/layers.css">
	<link rel="stylesheet" type="text/css" href="../include/rs-plugin/css/navigation.css">
	<link rel="stylesheet" type="text/css" href="../css/xzoom.css" media="all" />

	<link rel="stylesheet" type="text/css" href="../customStyle.css">
</head>

<body id="page-top">
	<?php include("phpIncludes/header.php") ?>

	<!-- /navbar ends -->
	<section id="slider" class="headMargin slider-element swiper_wrapper nmpHead" style="<?php if($p==9 || $p==11)echo 'background-image: url(../../cImages/prod/nmpg5.png);';else echo 'background-image: url(../../cImages/prod/nmpg6.png);'; ?>">
			<div class="swiper-container swiper-parent">
				<div class="swiper-wrapper">

					<div class="swiper-slide">
						<div class="container">
							<div class="row align-items-center mt-5 poppins marginTop50px">
								<div class="col-xl-4 pr-0 pr-xl-5 col-md-4 col-sm-11 col-12 mb-5 mb-lg-0 ">
									<div class="heading-block nobottomborder mb-4">
										<h2 class="t700 ls0 nott"><?php echo $plateName?></h2>
									</div>
									<?php echo $plateDesc;?>
									<p class="swiperConditions"><i class="redStar">*</i> Depends upon Input water quality water and input water pressure.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</section>
	<section id="content ">
		<div class="container headMargin">
			<div class="col-md-12 paddingMobileLR5px">
				<div class="col-md-4 productImages">
					<img class="xzoom" id="main_image" src="../cImages/prod/nmpg<?php echo $img;?>.png" xoriginal="cImages/prod/nmpg1.png">

					<div class="xzoom-thumbs marginTop50px">
						<?php echo $imgs;?>
					</div>
				</div>
				<div class="col-md-6 paddingMobileLR5px">
					<div class="text- pro-infor marginTop10px">

						<div class="pro-infor-title poppins"><?php echo $plateName?></div>

						<table class="pro-inforTable">

							<colgroup>
								<col width="135">
								<col width="">
							</colgroup>

							<tbody>
								<tr>
									<td class="bold1">Product name</td>
									<td>Water ionizer</td>
								</tr>

								<tr>
									<td colspan="2" height="13"></td>
								</tr>

								<tr>
									<td class="bold1">Dimension(mm)</td>
									<td>300(W) x 135(D) x 355(H)</td>
								</tr>

								<tr>
									<td colspan="2" height="13"></td>
								</tr>

								<tr>
									<td class="bold1">Weight(kg)</td>
									<td>5.2</td>
								</tr>

								<tr>
									<td colspan="2" height="13"></td>
								</tr>
								<tr>
									<td class="bold1">Plates</td>
									<td><?php echo $plates?></td>
								</tr>

								<tr>
									<td colspan="2" height="13"></td>
								</tr>
								<tr>
									<td class="bold1">Price</td>
									<td>₹ <?php echo $price?></td>
								</tr>


							</tbody>
						</table>

					</div>

				</div>
			</div>

		</div>

		<div class="bgColorSelected">
			<div class="container">
				<div class="productDiv">
					<ul>

						<li class="productDivSec active">
							<div>Features</div>
						</li>
						<li class="productDivSec ">
							<div>Specifications</div>
						</li>
					</ul>
				</div>
				<div class="col-md-12 marginTop30px paddingMobileLR5px">
					<div class="productDivDesc " id="specifications">
						<div class="table-responsive">
							<table class="pro-view-table mt50">

								<colgroup>
									<col width="220">
									<col width="50">
									<col width="120">
									<col width="300">
									<col width="110">
									<col width="">
								</colgroup>

								<tbody>
									<tr>
										<td class="center title line_r">Model</td>
										<td colspan="5" class="center fw600"><?php echo $plateName?></td>
									</tr>

									<tr>
										<td class="center title line_r">Exterior of Product</td>
										<td></td>
										<td class="fw600">Dimension</td>
										<td>300(W) x 135(D) x 355(H) mm</td>
										<td class="fw600">Weight</td>
										<td>5.2kg</td>
									</tr>

									<tr>
										<td class="center title line_r">Use Environment</td>
										<td></td>
										<td class="fw600">Water temperature</td>
										<td>4~35℃</td>
										<td class="fw600">Water pressure</td>
										<td>1~5kgf/㎠</td>
									</tr>

								</tbody>
							</table>
							<table class="pro-view-table mt30">

								<colgroup>
									<col width="220">
									<col width="50">
									<col width="120">
									<col width="300">
									<col width="110">
									<col width="">
								</colgroup>

								<tbody>
									<tr>
										<td class="center title line_r" rowspan="2">Water outflow</td>
										<td></td>
										<td class="fw600">Outflow method</td>
										<td>Electronic outflow structure</td>
										<td class="fw600">Water outflow</td>
										<td>Below 2L / min</td>
									</tr>

									<tr>
										<td></td>
										<td class="fw600">pH levels</td>
										<td colspan="3">3 Alkaline, 1 Neutral, 2 Acidic, Turbo</td>
									</tr>

								</tbody>
							</table>
							<table class="pro-view-table mt30">

								<colgroup>
									<col width="220">
									<col width="50">
									<col width="120">
									<col width="300">
									<col width="110">
									<col width="">
								</colgroup>

								<tbody>
									<tr>
										<td class="center title line_r">Controls and display</td>
										<td></td>
										<td class="fw600">Controls</td>
										<td>Touch screen</td>
										<td class="fw600">Display</td>
										<td>CUSTOM LCD</td>
									</tr>

									<tr>
										<td class="center title line_r">Power</td>
										<td></td>
										<td class="fw600">Power supply</td>
										<td>SMPS</td>
										<td class="fw600">Power consumption</td>
										<td>Max.220w</td>
									</tr>

									<tr>
										<td class="center title line_r">Electrolysis section</td>
										<td></td>
										<td class="fw600">Water Cell Plate Materials</td>
										<td>Platinum and Titanium</td>
										<td class="fw600">Electrolytic Cell Quantity of Electrodes</td>
										<td><?php echo $plates?></td>
									</tr>

									<tr>
										<td class="center title line_r">Filters configuration</td>
										<td></td>
										<td class="fw600">Filter Structure</td>
										<td colspan="3">2-Filter system</td>

									</tr>

								</tbody>
							</table>
							<table class="pro-view-table mt30">

								<colgroup>
									<col width="220">
									<col width="50">
									<col width="120">
									<col width="300">
									<col width="110">
									<col width="">
								</colgroup>

								<tbody>
									<tr>
										<td class="center title line_r" rowspan="5">Convenient function</td>
										<td></td>
										<td class="fw600">quantity discharge</td>
										<td><img src="../cImages/001-check.png"></td>
										<td class="fw600">Backlight color</td>
										<td><img src="../cImages/001-check.png"></td>
									</tr>

									<tr>
										<td></td>
										<td class="fw600">ECO mode</td>
										<td><img src="../cImages/001-check.png"></td>
										<td class="fw600">Time mode</td>
										<td><img src="../cImages/001-check.png"></td>
									</tr>

									<tr>
										<td></td>
										<td class="fw600">Automatic Cleaning</td>
										<td><img src="../cImages/001-check.png"></td>
										<td class="fw600">Manual Cleaning</td>
										<td><img src="../cImages/001-check.png"></td>
									</tr>

									<tr>
										<td></td>
										<td class="fw600">Filter usage indication</td>
										<td><img src="../cImages/001-check.png"></td>
										<td class="fw600">Automatic Stop Function</td>
										<td><img src="../cImages/001-check.png"></td>
									</tr>

									<tr>
										<td></td>
										<td class="fw600">Voice announcement</td>
										<td><img src="../cImages/001-check.png"></td>
										<td class="fw600">Voice setting</td>
										<td><img src="../cImages/001-check.png"></td>
									</tr>

								</tbody>
							</table>
							<table class="pro-view-table mt30">

								<colgroup>
									<col width="220">
									<col width="50">
									<col width="120">
									<col width="300">
									<col width="110">
									<col width="">
								</colgroup>

								<tbody>
									<tr>
										<td class="center title line_r" rowspan="2">Safety function</td>
										<td></td>
										<td class="fw600">Water temperature sensor</td>
										<td><img src="../cImages/001-check.png"></td>
										<td class="fw600">Overheating protection</td>
										<td><img src="../cImages/001-check.png"></td>
									</tr>

									<tr>
										<td></td>
										<td class="fw600">Supplying water sensor</td>
										<td><img src="../cImages/001-check.png"></td>
										<td class="fw600">Error indication</td>
										<td><img src="../cImages/001-check.png"></td>
									</tr>

								</tbody>
							</table>
							<table class="pro-view-table mt30">

								<colgroup>
									<col width="220">
									<col width="50">
									<col width="120">
									<col width="300">
									<col width="110">
									<col width="">
								</colgroup>

								<tbody>
									<tr>
										<td class="center title line_r" rowspan="3">Setting function</td>
										<td></td>
										<td class="fw600">Adjustable Alkaline pH</td>
										<td><img src="../cImages/001-check.png"></td>
										<td class="fw600">Adjustable Acidic pH</td>
										<td><img src="../cImages/001-check.png"></td>
									</tr>

									<tr>
										<td></td>
										<td class="fw600">Language setting</td>
										<td><img src="../cImages/001-check.png"></td>
										<td class="fw600">Time setting</td>
										<td><img src="../cImages/001-check.png"></td>
									</tr>

									<tr>
										<td></td>
										<td class="fw600">Volume control</td>
										<td><img src="../cImages/001-check.png"></td>
										<td class="fw600">Filter reset</td>
										<td><img src="../cImages/001-check.png"></td>
									</tr>

								</tbody>
							</table>
						</div>

					</div>
					<div class="productDivDesc" id="features">
						<div class="container clearfix paddingBottom paddingMobileLR5px">

							<div id="side-navigation" class="ui-tabs ui-corner-all ui-widget ui-widget-content ">

								<div class="col_one_third nobottommargin sidebar">
									<ul class="sidenav ui-tabs-nav ui-corner-all ui-helper-reset ui-helper-clearfix ui-widget-header sidebar__inner"
										role="tablist">
										<button class="button button-3d button-rounded button-teal mobileCloseButton"><i
												class="icon-remove"></i></button>

										<li class="ui-tabs-tab ui-corner-top ui-state-default ui-tab" role="tab"
											tabindex="-1" aria-controls="snav-content1" aria-labelledby="ui-id-1"
											aria-selected="false" aria-expanded="false"><a href="#snav-content1"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-1"><i
													class="icon-screen"></i>About alkaline water</a></li>
										<li role="tab" tabindex="-1"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content2" aria-labelledby="ui-id-2"
											aria-selected="false" aria-expanded="false"><a href="#snav-content2"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-2"><i
													class="icon-magic"></i>Features<i class="icon-chevron-"></i></a>
										</li>
										<li role="tab" tabindex="-1"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content3" aria-labelledby="ui-id-3"
											aria-selected="false" aria-expanded="false"><a href="#snav-content3"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-3"><i
													class="icon-star3"></i>Functions<i class="icon-chevron-"></i></a>
										</li>
										<li role="tab" tabindex="0"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content4" aria-labelledby="ui-id-4" aria-selected="true"
											aria-expanded="true"><a href="#snav-content4" role="presentation"
												tabindex="-1" class="ui-tabs-anchor" id="ui-id-4"><i
													class="icon-gift"></i>TYENT's
												Technological Process<i class="icon-chevron-"></i></a></li>
										<li role="tab" tabindex="-1"
											class="ui-tabs-tab ui-corner-top ui-state-default ui-tab"
											aria-controls="snav-content5" aria-labelledby="ui-id-5"
											aria-selected="false" aria-expanded="false"><a href="#snav-content5"
												role="presentation" tabindex="-1" class="ui-tabs-anchor" id="ui-id-5"><i
													class="icon-adjust"></i>FILTER
												SYSTEM<i class="icon-chevron-"></i></a></li>
									</ul>

								</div>

								<div class="col_two_third col_last nobottommargin">

									<div id="snav-content1" aria-labelledby="ui-id-1" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div class="tab-con-box">

											<div class="tab-box-title">
												<div class="tab-title">About alkaline water</div>
											</div>

											<div class="pageTitle" style="margin-top:60px"><img
													src="../cImages/sub_dot.gif" alt="">Alkaline water</div>

											<div class="fpx14 line14">
												Water that has been purified through the filter next undergoes
												electrolysis
												in the electrolyzer, and acidic anion components such as chlorine,
												sulfate,
												nitrate ions, etc. are collected in the positive pole to form acidic
												ionized
												water, creating oxygen in the process. Meanwhile, in the negative pole,
												alkaline
												positive ion components such as calcium, magnesium, sodium ions, etc.
												gather
												to form alkaline ionized water and generates hydrogen.
											</div>

											<div class="imgWrap" style="margin:50px 0px;"><img
													src="../cImages/pro11_1_1_img01.gif" alt="Alkaline water"></div>

											<table style="width:100%">

												<colgroup>
													<col width="70">
													<col width="450">
													<col width="70">
													<col width="">
												</colgroup>

												<tbody>
													<tr valign="top">

														<td><img src="../cImages/pro11_1_1_n1.gif" alt=""></td>

														<td>
															<table>
																<tbody>
																	<tr>
																		<td class="fpx18 fw600">Water with rich <span
																				class="fblue2">minerals</span></td>
																	</tr>
																	<tr>
																		<td height="16"></td>
																	</tr>
																	<tr>
																		<td class="fpx14 line14">Alkaline water contains
																			rich minerals such as calcium,potassium and
																			magnesium.</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr>
														<td><img src="../cImages/pro11_1_1_n2.gif" alt=""></td>

														<td>
															<table>
																<tbody>
																	<tr>
																		<td class="fpx18 fw600"><span
																				class="fblue2">Smaller</span> clusters
																		</td>
																	</tr>
																	<tr>
																		<td height="16"></td>
																	</tr>
																	<tr>
																		<td class="fpx14 line14">
																			The particles of this water is about 1/3
																			smaller
																			than ordinary
																			water and are thus able to permeate deep
																			into
																			the human
																			body, quickly removing waste matter out of
																			the
																			body.
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>

													</tr>

													<tr>
														<td height="20" colspan="4"></td>
													</tr>

													<tr valign="top">

														<td><img src="../cImages/pro11_1_1_n3.gif" alt=""></td>

														<td>
															<table>
																<tbody>
																	<tr>
																		<td class="fpx18 fw600">Water which has high
																			reductive powers</td>
																	</tr>
																	<tr>
																		<td height="16"></td>
																	</tr>
																	<tr>
																		<td class="fpx14 line14">
																			Alkaline ionized water has high reductive
																			powers, and has
																			anti-oxidant power because it contains
																			abundant
																			amounts
																			of active hydrogen which removes active
																			oxygen,
																			which is
																			the source of an array of diseases.
																		</td>
																	</tr>
																</tbody>
															</table>
														</td>
													</tr>
													<tr>

														<td><img src="../cImages/pro11_1_1_n4.gif" alt=""></td>

														<td>
															<table>
																<tbody>
																	<tr>
																		<td class="fpx18 fw600"><span
																				class="fblue2">Alkaline</span> water
																		</td>
																	</tr>
																	<tr>
																		<td height="16"></td>
																	</tr>
																	<tr>
																		<td class="fpx14 line14">Alkaline water is
																			alkaline
																			water from pH 8.5 to 10.</td>
																	</tr>
																</tbody>
															</table>
														</td>

													</tr>

												</tbody>
											</table>

											<div class="sub_space"></div>

											<div class="fpx18 fw600">Improved effect of 4 kinds of gastroenteric
												troubles
											</div>

											<div class="imgWrap mt30"><img src="../cImages/pro11_3_1_img01.gif"
													alt="Improved effect of 4 kinds of gastroenteric troubles"></div>

										</div>
									</div>

									<div id="snav-content2" aria-labelledby="ui-id-2" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div class="tab-con-box nmpFeatures">

											<div class="tab-box-title">
												<div class="tab-title">Features</div>
											</div>
											<div class="col-md-12 marginTop30px">
												<div class="col-md-3">
													<img src="../cImages/pro11_11_1_img03.png" alt="ONE-TOUCH system">
												</div>
												<div class="col-md-9">
													<div class="c36 fpx18 fw600">ONE-TOUCH system</div>
													<div class="fpx14 line14" style="margin-top:25px;">
														Convenient ONE-TOUCH electronic-type water discharging
														structure.
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="col-md-3">
													<img src="../cImages/pro11_11_1_img04.png" alt="ONE-TOUCH system">
												</div>
												<div class="col-md-9">
													<div class="c36 fpx18 fw600" style="margin-top:16px">Chrome
														decorative pointModern and stylish design</div>
													<div class="fpx14 line14" style="margin-top:25px;">Stylish
														design with chrome decoration and soft round square
														shapes.</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="col-md-3">
													<img src="../cImages/pro11_11_1_img05.png" alt="ONE-TOUCH system">
												</div>
												<div class="col-md-9">
													<div class="c36 fpx18 fw600" style="margin-top:16px">46cm
														flexible water outlet tube</div>
													<div class="fpx14 line14" style="margin-top:25px;">Water can be
														generated to the desired angle and direction.</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class="col-md-3">
													<img src="../cImages/pro11_11_1_img06.png" alt="ONE-TOUCH system">
												</div>
												<div class="col-md-3">
													<div class="c36 fpx18 fw600" style="margin-top:2px">Fixed
														quantity discharge function</div>
													<div class="fpx14 line14" style="margin-top:25px;">It generates
														desired amount of water by convince operation.</div>
												</div>
											</div>
											<?php
												if($plates==11){
													?>
														<div class="col-md-12">
												<div class="col-md-3">
													<img src="../cImages/pro11_11_1_img07.png" alt="ONE-TOUCH system">
												</div>
												<div class="col-md-9">
													<div class="c36 fpx18 fw600" style="margin-top:25px">2way
														control system</div>
													<div class="fpx14 line14" style="margin-top:25px;">According to
														user’s convenience, can use JOG DIAL or Touch</div>
												</div>
											</div>
													<?php
												}
											?>

										

											<div class="col-md-12">
												<div class="col-md-3">
													<img src="../cImages/pro11_11_1_img08.png" alt="ONE-TOUCH system">
												</div>
												<div class="col-md-9">
													<div class="c36 fpx18 fw600" style="margin-top:10px">Simple
														Filter Replacement</div>
													<div class="fpx14 line14" style="margin-top:25px;">Anyone can
														replace the filter easily.</div>
												</div>
											</div>

											<div class="col-md-12">
												<div class="col-md-3">
													<img src="../cImages/pro11_11_1_img09.png" alt="ONE-TOUCH system">
												</div>
												<div class="col-md-9">
													<div class="c36 fpx18 fw600">A convenient open &amp;close
														method</div>
													<div class="fpx14 line14" style="margin-top:25px;">
														Our sliding door opening system
														allows anyone to operate the
														machine with ease.
													</div>
												</div>
											</div>

											<div class="col-md-12">
												<div class="col-md-3">
													<img src="../cImages/pro11_11_1_img10.png" alt="ONE-TOUCH system">
												</div>
												<div class="col-md-9">
													<div class="c36 fpx18 fw600" style="margin-top:3px">Wall-mounted
														type</div>
													<div class="fpx14 line14" style="margin-top:25px;">Wall-mounted
														type of for making better use of a space.</div>
												</div>
											</div>
										</div>

									</div>

									<div id="snav-content3" aria-labelledby="ui-id-3" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div class="tab-con-box nmpFunctions">

											<div class="tab-box-title">
												<div class="tab-title">Functions</div>
											</div>


											<div class="col-md-12 marginTop30px">
												<div class="pageTitle" style="margin-top:50px">
													<img src="../cImages/sub_dot.gif" alt="">Control panel designed for
													convenient use
												</div>
												<div class="col-md-6">
													<img src="../cImages/pro11_11_1_img11.jpg"
														alt="Control panel designed for convenient use">
												</div>
												<div class="col-md-6">
													<div class="fpx16 fw600 fblue2">ONE TOUCH system</div>
													<div class="fpx14 line14" style="margin-top:15px;">Each function
														selection is ionized, and you can outflow and stop the desired
														water with one touch button.</div>
													<div class="fpx16 fw600 fblue2" style="margin-top:10px;">Color
														backlight
														function</div>
													<div class="fpx14 line14"
														style="margin-top:15px; letter-spacing:-0.2px;">
														It distinguishes alkali water and acid water with the color of
														backlight.
													</div>
													<div class="fpx16 fw600 fblue2" style="margin-top:10px;">2way
														Control
														method</div>
													<?php
														if($plates==11 || $plates==9){
													?>
														<div class="fpx14 line14" style="margin-top:15px;">
															It can be operated by JOG DIAL or TOUCH SCREEN according to
															user's convenience.
														</div>
													<?php } ?>
												</div>
											</div>

											<div class="col-md-12 marginTop30px">
												<div class="pageTitle"><img src="../cImages/sub_dot.gif" alt="">DISPLAY
													COLORS</div>
												<div class="col-md-12">
													<img src="../cImages/pro11_11_1_img13.png" alt="DISPLAY COLORS">
												</div>

											</div>
											<div class="col-md-12">
												<div class="pageTitle" style="margin-top:60px;"><img
														src="../cImages/sub_dot.gif" alt="">Various convenience functions
												</div>
												<div class="fw600 fblue2 fpx16">Fixed quantity discharge function</div>
												<div class="fpx14 line14" style="margin-top:17px;">
													You can drink the fixed amount of water which uses a
													lot in our everyday lives such as 0.5L, 1.0L and 1.5L
													via Fixed quantity discharge function.

												</div>
											</div>
											<div class="col-md-12 marginTop30px">
												<div class="col-md-6">
													<img src="../cImages/pro11_11_1_img15.png"
														alt="Real time Filter Usage Indication">
												</div>
												<div class="col-md-6">
													<div class="fw600 fblue2 fpx16 mt60">Real time Filter Usage
														Indication
													</div>
													<div class="fpx14 line14">
														Level of filter usage can be identified through the
														LCD Screen on a real time basis. The
														recommended replacement time is automatically displayed.
													</div>
												</div>
											</div>
											<div class="col-md-12 marginTop30px">
												<div class="col-md-6">
													<div class="fw600 fblue2 fpx16">Upgraded Power
														Turbo System</div>
													<div class="fpx14 line14">
														Turbo upgrades to the SMPS power supply system
														produce both the acid turbo water and strong alkaline
														water you need!
													</div>
												</div>
												<div class="col-md-6">
													<img src="../cImages/pro11_11_1_img17.png"
														alt="Upgraded Power Turbo System">
												</div>
											</div>

											<div class="col-md-12 marginTop30px">
												<div class="col-md-6">
													<img src="../cImages/pro11_11_1_img18.png" alt="ECO Mode">
												</div>
												<div class="col-md-6">
													<div class="fw600 fblue2 fpx16">ECO Mode</div>
													<div class="fpx14 line14" style="margin-top:15px;">
														When setting the eco-mode, the LCD backlight will be
														turned off automatically during periods of non-use and
														the Time function is displayed
													</div>
												</div>
											</div>
											<?php
												if($plates==11 || $plates==9){
													?>
												<div class="col-md-12 marginTop30px">
													<div class="pageTitle"><img src="../cImages/sub_dot.gif" alt="">Special
														operation, JOG DIAL
													</div>
													<div class="col-md-6">
														<img src="../cImages/pro11_11_1_img19.png"
															alt="Special operation, JOG DIAL">
													</div>
													<div class="col-md-6">
														<div class="fpx14 line14">
															It is easy to operate with simple rotation and click action,
															and the soft LED lamp can change color according to the outflow
															stage,
															helping to use safer product with aesthetic effect.
														</div>
													</div>
												</div>
												<?php } ?>
											<div class="col-md-12 marginTop30px">
												<div class="col-md-6">
													<img src="../cImages/pro11_11_1_img20.png" alt="Drain Faucet">
												</div>
												<div class="col-md-6">
													<div class="pageTitle" style="margin-top:60px;">

														Drain Faucet
													</div>
													<div class="fpx14 line14" style="margin-top:15px;">
														It comes with a Drain faucet that can be easily
														installed to anywhere on the sink.
													</div>
												</div>
											</div>

											<div class="col-md-12">
												<div class="pageTitle" style="margin-bottom:30px;"><img
														src="../cImages/sub_dot.gif" alt="">LCD Display and Touch Pad
													Overview
												</div>
												<div class="imgWrap mt40"><img src="../cImages/pro11_11_1_img21.png"
														alt="LCD Display and Touch Pad Overview" class="auto"></div>
											</div>



											<div class="">
												<div class="col-md-4">
												<table class="buttonTable marginTop30px" style="width:100%;">
													<tbody>
														<tr>
															<td class="bg_sky">No</td>
															<td class="bg_gray"></td>
															<td class="bg_sky">Name</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">01</td>
															<td class="bg_gray"></td>
															<td>Turbo</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">02</td>
															<td class="bg_gray"></td>
															<td>Acidic Level 2</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">03</td>
															<td class="bg_gray"></td>
															<td>Acidic Level 1</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">04</td>
															<td class="bg_gray"></td>
															<td>Purified water</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">05</td>
															<td class="bg_gray"></td>
															<td>Alkaline water 1</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">06</td>
															<td class="bg_gray"></td>
															<td>Alkaline water 2</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">07</td>
															<td class="bg_gray"></td>
															<td>Alkaline water 3</td>
															<td class="bg_gray"></td>
														</tr>

													</tbody>
												</table>
												</div>
												<div class="col-md-4">
												<table class="buttonTable marginTop30px" style="width:100%;">
													<tbody>
														<tr>
															<td class="bg_sky">No</td>
															<td class="bg_gray"></td>
															<td class="bg_sky">Name</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">08</td>
															<td class="bg_gray"></td>
															<td>Time</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">09</td>
															<td class="bg_gray"></td>
															<td>pH</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">10</td>
															<td class="bg_gray"></td>
															<td>Water flow</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">11</td>
															<td class="bg_gray"></td>
															<td>Voltage/Flow/Time</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">12</td>
															<td class="bg_gray"></td>
															<td>Door open</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">13</td>
															<td class="bg_gray"></td>
															<td>Temperature</td>
															<td class="bg_gray"></td>
														</tr>

														<tr>
															<td class="bold">14</td>
															<td class="bg_gray"></td>
															<td>Remaining time</td>
															<td class="bg_gray"></td>
														</tr>

													</tbody>
												</table>
												</div>
												<div class="col-md-4">
												<table class="buttonTable marginTop30px" style="width:100%;">
													<tbody>
														<tr>
															<td class="bg_sky">No</td>
															<td class="bg_gray"></td>
															<td class="bg_sky">Name</td>
														</tr>

														<tr>
															<td class="bold">15</td>
															<td class="bg_gray"></td>
															<td>Fixed quantity discharge</td>
														</tr>

														<tr>
															<td class="bold">16</td>
															<td class="bg_gray"></td>
															<td>System set up</td>
														</tr>

														<tr>
															<td class="bold">17</td>
															<td class="bg_gray"></td>
															<td>Cleaning</td>
														</tr>

														<tr>
															<td class="bold">18</td>
															<td class="bg_gray"></td>
															<td>Volume On / Off</td>
														</tr>

														<tr>
															<td class="bold">19</td>
															<td class="bg_gray"></td>
															<td>1st Filter reset</td>
														</tr>

														<tr>
															<td class="bold">20</td>
															<td class="bg_gray"></td>
															<td>2nd Filter reset</td>
														</tr>
													</tbody>
												</table>
												</div>
												
											</div>
										</div>
									</div>

									<div id="snav-content4" aria-labelledby="ui-id-4" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="false">
										<div class="tab-con-box" style="padding-bottom:10px;">

											<div class="tab-box-title">
												<div class="tab-title">
													TYENT's Technological Prowess
												</div>
											</div>
											<div class="col-md-12">
												<div class="pageTitle"><img src="../cImages/sub_dot.gif" alt="">
													Equipped with the most powerful turbo engine SMPS(10A)
												</div>
												<div class="">

													<div class=" fpx16 fwhite mt60">
														* Increased Electrolytic efficiency &amp; safety!
													</div>
													<div class=" fpx16 fwhite mt20">
														* EU CE Safety Management System CLASS2
													</div>
													<div class=" fpx16 fwhite mt20">
														* Strengthening of double insulation system electric safety
														standard
													</div>

												</div>
												<div class="pageTitle marginTop30px"><img src="../cImages/sub_dot.gif"
														alt="">Generating of Alkaline water
													starts with a stable power supply</div>
												<div class="fpx14 line15 marginTop30px">
													By applying the Switching Mode Power Supply (SMPS)method, the AC
													Line frequency.
													(50Hz~60Hz) is converted into DC, changing it into a high
													frequency for use.
												</div>

												<div class="fw600 fpx16 fblue2 marginTop30px">Changes in the output
													voltage in
													correlation to changes in the input power</div>
												<div class="fpx14 mt20 line15">
													Because the input exchange voltage is changed into a high dc voltage
													and controlled at a high-
													frequency, no changes occur in the output voltage.
												</div>

												<div class="fw600 fpx16 fblue2 mt30 marginTop30px">Stable power supply
												</div>
												<div class="fpx14 mt20 line15">
													The supply of electrical power remains consistent even during
													continual use, thus maintaining
													optimal performance.
												</div>

												<div class="fw600 fpx16 fblue2 mt30 marginTop30px">Electrical safety
												</div>
												<div class="fpx14 mt20 line15">
													In case of over current or over voltage, it automatically cuts off the
													supply of electricity
													to the inside of the product.
												</div>
												<div class="text-center imgWrap marginTop30px"><img
														src="../cImages/pro11_11_1_img22.gif" alt="certificate"></div>
											</div>

											<div class="col-md-12">
												<div class="pageTitle marginTop30px"><img src="../cImages/sub_dot.gif"
														alt="">ROUND MESH
													Electrolyzer</div>
												<div class="fpx14">
													Electrolyzer is most important part in water ionizer. The difference
													of the
													surface treatment determines the electrolysis efficiency.
												</div>
												<div class="col-md-12 marginTop30px">
													<div class="imgWrap mt20"><img src="../cImages/pro11_11_1_img23.gif"
															alt="ROUND MESH Electrolyzer"></div>
													<div class="fw600 fpx16 fblue2 marginTop30px">01. A platinum coated
														titanium mesh covers</div>
													<div class="fpx14 mt10 line15 ">
														Round-edge type for considering the characteristics of the
														electricity.
													</div>

													<div class="fw600 fpx16 fblue2 mt30 marginTop30px">02. Dualization
														of water inlet in
														electrolyzer</div>
													<div class="fpx14 mt10 line15">
														Prevention of internal pressure in electrolyzer and improvement
														of
														electrolysis effectiveness.
														Stable water outflow for alkaline water and acid water.
													</div>

													<div class="fw600 fpx16 fblue2 mt30 marginTop30px">03. Thread
														Mounted Types</div>
													<div class="fpx14 mt10 line15">
														Improvement of the precision between electrode and membrane
														structure.
													</div>
												</div>

											</div>




											<div class="col-md-12">
												<div class="pageTitle "><img src="../cImages/sub_dot.gif"
														alt="">Self-diagnosis
													safety functions</div>
												<table class="table">
													<colgroup>
														<col width="123">
														<col width="397">
														<col width="123">
														<col width="">
													</colgroup>
													<tbody>
														<tr valign="top">
															<td><img src="../cImages/pro11_1_1_img20.gif"
																	alt="Automatic Stop Function"></td>
															<td>
																<div class="fpx16 fblue2 fw600" style="margin-top:2px;">
																	Automatic Stop Function</div>
																<div class="fpx14 line14" style="margin-top:10px;">
																	Water outflow will stop to prevent overflow or
																	flooding. there is no need to wait if the unit
																	automatically shuts off. You can resume its use
																	immediately.
																</div>
															</td>
														</tr>
														</tr>
														<td><img src="../cImages/pro11_1_1_img21.gif"
																alt="Temperature sensor">
														</td>
														<td>
															<div class="fpx16 fblue2 fw600" style="margin-top:2px;">
																Temperature sensor</div>
															<div class="fpx14 line14" style="margin-top:10px;">This unit
																is
																equipped with a temperature sensor which prevents the
																accidental inflow of hot water.</div>
														</td>
														</tr>

														<tr>
															<td colspan="4" height="40"></td>
														</tr>

														<tr valign="top">
															<td><img src="../cImages/pro11_1_1_img22.gif"
																	alt="Sensing function of supplied raw water"></td>
															<td>
																<div class="fpx16 fblue2 fw600" style="margin-top:2px;">
																	Sensing
																	function of supplied raw water</div>
																<div class="fpx14 line14" style="margin-top:10px;">
																	The outflow of functional water stops automatically
																	when raw water is not supplied, preventing the
																	electrolytic cell and power supply unit from being
																	damaged due to current overflow, water from
																	being wasted and accidental water leaks.
																</div>
															</td>
														</tr>
														</tr>
														<td><img src="../cImages/pro11_11_1_img24.gif"
																alt="Detects if filter door is open"></td>
														<td>
															<div class="fpx16 fblue2 fw600" style="margin-top:2px;">
																Detects
																if filter door is open</div>
															<div class="fpx14 line14" style="margin-top:10px;">
																When door is open, the water supply will be suspended
																for
																your safety.
															</div>
														</td>
														</tr>

													</tbody>
												</table>

											</div>



										</div>
									</div>

									<div id="snav-content5" aria-labelledby="ui-id-5" role="tabpanel"
										class="ui-tabs-panel ui-corner-bottom ui-widget-content" aria-hidden="true"
										style="display: none;">
										<div class="tab-con-box" style="padding-bottom:10px;">

											<div class="tab-box-title">
												<div class="tab-title">FILTER SYSTEM</div>
											</div>

											<div class="pageTitle" style="margin-top:60px;margin-bottom:25px;"><img
													src="../cImages/sub_dot.gif" alt="">FILTER SYSTEM</div>
											<div class="fpx14">2-filter system makes clean water with abundant minerals
												and
												removes harmful substances.</div>

											<div class="clearFix mt50">

												<div class=" imgWrap" style="margin-top:4px;"><img
														src="../cImages/pro11_2_1_img22.gif" alt="FILTER SYSTEM"></div>

												<div >

													<div class="pageTitle" style="margin:0px;"><img
															src="../cImages/sub_dot.gif" alt="">Membrane</div>
													<div class="fpx14 line15" style="margin-top:22px;">
														Pretreatment filtration function for filtering various
														precipitates
														contained in raw water.
													</div>

													<div class="pageTitle" style="margin-bottom:0px;"><img
															src="../cImages/sub_dot.gif" alt="">Carbon Block</div>
													<div class="fpx14 line15" style="margin-top:22px;">
														Activated carbon was made into a block form has a larger surface
														area than particle-type activated carbon. It has a very high
														adsorption rate.
														It works to filter residual chlorine, organic compounds and
														heavy
														metals by activated adsorption method.

													</div>

													<div class="pageTitle" style="margin-bottom:0px;"><img
															src="../cImages/sub_dot.gif" alt="">Calcium sulfite</div>
													<div class="fpx14 line15" style="margin-top:22px;">
														Removes residual chlorine completely by oxidation-reduction
														reaction.
													</div>

												</div>

											</div>

											<div class="pageTitle" style="margin-bottom:0px;"><img
													src="../cImages/sub_dot.gif" alt="">TM Ceramics</div>
											<div class="fpx14 mt20">The ceramics were made by mixing the high-emissivity
												far-infrared minerals containing potassium (k), sodium (Na) and calcium
												(Ca)
												with TM and TM-X stock solutions.
												These multifunctional ceramics have antioxidant, antimicrobial and far
												infrared radiation.
											</div>

											<div class="pageTitle" style="margin-bottom:0px;"><img
													src="../cImages/sub_dot.gif" alt="">UF</div>
											<div class="fpx14 mt20">Many fine pores on the filter surface remove
												contaminants from the water.
												It removes bad elements such as bacteria viruses and particulates, but
												does
												not remove substances that are beneficial to our body, such as minerals.
											</div>



											<div class="sub_space"></div>

											<div class="pageTitle"><img src="../cImages/sub_dot.gif" alt="">Open and close
												type
												door &amp; simple filter replacement system</div>
											<div class="fpx14">A simple filter replacement system along with one-touch
												Filter Usage Initialization Setting System makes filter management easy
												for
												any one.</div>
											<div class="imgWrap mt40"><img src="../cImages/pro11_11_1_img25.gif"
													alt="Open and close type door &amp; simple filter replacement system">
											</div>

										</div>
									</div>

								</div>

							</div>


						</div>
					</div>
				</div>

			</div>
		</div>
		<?php include('form.php')?>



	</section>
	<?php include("phpIncludes/footer.php") ?>

	<!-- /footer ends -->
	<!-- Core JavaScript Files -->
	<script src="../header/js/jquery.min.js"></script>
	<script src="../header/js/bootstrap.min.js"></script>
	<script src="../js/plugins.js"></script>
	<script src="../js/functions.js"></script>


	<!-- Main Js -->
	<script src="../header/js/main.js"></script>

	<script type="text/javascript" src="../js/xzoom.min.js"></script>
	<script type="text/javascript" src="../js/sticky-sidebar.js"></script>

	<script src="../js/customScript.js"></script>
	<script>$('#nmpSeriesMenu').parent().addClass('active');</script>
	<script>
		
		jQuery(window).on( 'load', function(){
		    var swiper = new Swiper('.swiper-scroller', {
		    	slidesPerView: 'auto',
		    	spaceBetween: 50,
				freeMode: true,
				grabCursor: true,
				navigation: {
				    nextEl: '.slider-arrow-right-1',
				    prevEl: '.slider-arrow-left-1',
				},
				scrollbar: {
				el: '.swiper-scrollbar',
				},
				mousewheel: true,
				breakpoints: {
			        768: {
			          spaceBetween: 20,
			        },
			        576: {
			          spaceBetween: 15,
			        }
			      }
		    });
		});

		$(function () {
			$("#specifications").hide();
			$("#side-navigation").tabs({
				show: {
					effect: "fade",
					duration: 400
				}
			});
		});
		$(".xzoom, .xzoom-gallery").xzoom({
			tint: '#333',
			Xoffset: 15,
			defaultScale: -0.3
		});
		var a = new StickySidebar('.sidebar', {
			topSpacing: 110,
			bottomSpacing: 20,
			containerSelector: '#features',
			innerWrapperSelector: '.sidebar__inner'
		});
		$('#productLi').addClass('active');
		$(".mobileCloseButton").click(function () {
			$('.ui-tabs-tab').toggle();
			$(".mobileCloseButton").html("<i class='icon-line-rewind'></i>");
			$(".mobileCloseButton").closest("ul.sidenav").toggleClass("transparentBG");
		});
	</script>
</body>

</html>